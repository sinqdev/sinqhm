\documentclass[12pt,a4paper]{article}
\usepackage[dvips]{graphicx}
\usepackage{epsf}
\setlength{\textheight}{24cm}
\setlength{\textwidth}{16cm}
\setlength{\headheight}{0cm}
\setlength{\headsep}{0cm}
\setlength{\topmargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\hoffset}{0cm}
\setlength{\marginparwidth}{0cm}

\begin{document}

\title{The Linux-RTAI based Histogram Memory Software for SINQ}
\author{Gerd Theidel, Mark K\"onnecke\\Laboratory for Development and Methods\\Paul Scherrer Institute\\
CH-5232 Villigen--PSI\\Switzerland}
\date{March, 2006}
\maketitle

\section{Introduction}
The system described in this paper offers a solution for the problem
of histogramming neutron events detected in a position sensitive
neutron detector. Single neutron events are only mildly interesting to
scientists.  Scientists prefer their neutron events to be summed
during the measurement time into some meaningful representation called
histograms.  This is exactly what this system does.

This new system is the successor of an earlier vxWorks based system
developed by David Maden et al. This project was initiated by
organisational changes which would have landed our group with the
maintainance fines for the vxWorks version used to the sum of 9 KCHF/year. 

The system as described here is only in parts specific to the hardware
setup used at PSI. The system design and the server implementation can
be reused in other contexts too.

\section{System Overview}
\subsection{Hardware Overview}
\begin{figure}
\caption{Hardware Overview} \label{hw}
\includegraphics[width=15cm]{hmhw.eps}
\end{figure}
Figure \ref{hw} shows on overview of the hardware used in the
system. The system consists of the following hardware components:
\begin{itemize}
\item A position sensitive detector detects neutrons
\item The detector electronics amplifies the signal generated in the
  detector and formats the neutron detection event into a packet on
  the fibre optic link. This packet encodes some header information
  and the position where the neutron was detected. Optionally a time
  stamp is added to the packet too.
\item The packets on the fibre optic link are read by a VME single 
  board computer. 
  This histogram memory computer also performs the
  histogramming. Moreover there is some server software on this
  computer which allows to start and stop measurements, configure the
  histogramming process and read the histogrammed data. The histogram memory
  computer is connected to the TCP/IP network. 
\item The instrument is controlled through an instrument computer
  which communicates via the network with the histogram memory computer.   
\end{itemize}
Currently MEN A--12 PowerPC VME onboard computers are used as
histogram memory computers. One of the advantages of the new system is
that it will also run on i386 based hardware.  

\subsection{Software Overview}
\begin{figure}
\caption{Software Overview} \label{sw}
\includegraphics[width=15cm]{hmsw.eps}
\end{figure}
Figure \ref{sw} shows the software setup on the histogram memory
computer. Essentially there are two cooperating processes, the filler
and the server which communicate via shared memory. In Linux--RTAI
there is a distinction between user space processes and realtime
processes.  The idea is to keep the realtime part minimal and do most
of the work in user space. 

The filler process is responsible for reading data from the fibre
optic link and for histogramming the data. The FIFO on the fibre
optic receiver is to small to hold all the data coming in during the
latency  period of a plain linux system. The fibre optic receiver
cannot do interrupts either. This is why the filler process has to be
a realtime process in order to ensure that all incoming neutron events
are processed.  The filler process implements most of the hardware
dependent part of the system. Different detectors and data aquisition
methods require different histogramming algorithms.

The server process is responsible for:
\begin{itemize}
\item Communication with the outside world over the TCP/IP network.
\item Starting and stopping data aquisition.
\item Configuration of the histogramming memory.
\item Retrieval of the histogrammed data. 
\item Debugging of histogramming and LWL data.
\end{itemize}
The server is implemented as a http server. Http is very popular and
libraries for http client communication exits for most programming
environments. Moreover, some status and debug information can be
obtained from the histogram memory by pointing an normal WWW-browser
to it. The histogram memory
configuration is uploaded as an XML file via a http--POST request to the server. All
other requests are normal http-GET requests. 

The two processes, the http-server and the filler process, communicate
with each other through three shared memory regions:
\begin{description}
\item[Control SHM] This is small control shared memory area which is
  used for flags and global counters. 
\item[Data SHM] is a large shared memory area which contains the
  layout of the histogram memory and the actual histogram memory
  data. 
\item[Debug SHM] is a shared memory area which serves as a ring buffer
  for debugging and error messages.
\end{description}


\section{SinqHM HTTP Requests}
The WWW browser interface on the histogram memory computer is self
explanatory or is described on the relevant WWW-pages on the
server. 


This section describes the usage of the SinqHM histogram memory
through the http protocoll by clients such as instrument control
software and status displays.  Basically specific functions are mapped to
a predefined paths in the WWW--server. Parameters are appended to the
base path of a function according to the CGI specification.  Most
SinqHM operations require authentication: HTTP base authentication is
used for this. All requests return some data. In case of errors the
returned request body will be an error message starting with the
string: ERROR followed by further information about the error. 


In the following sections only the path strings within the SinqHM
server are shown. In order to get a complete request URL
{\it http://your-hm-computername} must be prepended. 


\subsection{Starting and Stopping DAQ}
For staring and stopping data aquisition the following paths are to be
used:
\begin{description}
\item[/admin/startdaq.egi] initializes the histogram memory data and
  eventual counters to zero and starts data aquisition.
\item[/admin/stopdaq.egi] stops data aquisition.
\item[/admin/pausedaq.egi] pauses data aquisition. The data in the
  histogram memory is not modified.
\item[sinqhm/continuedaq.egi] continues a paused data aquisition.
\end{description}

\subsection{Status Request}
\begin{description}
\item[/admin/textstatus.egi] perfoms a status request on the
  histogram memory. The returned request body contains several lines
  of name value pairs separated by :. For an example see below
\end{description}
\begin{verbatim}
HM-Host: hm03
DAQ: 1
LWL-Status: 1
LWL-Overflow-Count: 0
\end{verbatim}
The content is pretty self explanatory. More fields and counters will
be added to this message later. 

\subsection{Retrieving Data}
The following request can be used to download histogram memory data:\\
\centerline{/admin/readhmdata.egi?bank=val\&start=val\&end=val}\\
val is a placeoholder for an integer parameter. The parameter mean:
\begin{description}
\item[bank] The number of the bank to retrieve data from. The SinqHM
  supports multiple banks.
\item[start] start downloading at the given index in the histogram
  memory data area.
\item[end] download data only until the given index in the data area.
\end{description}
The start and end parameter together allow for downloading subsets of
the histogram memory data. For index calculation, C language storage
order is assumed. 

All parameters  can be omitted: then the full histogram memory data for bank
0 is downloaded.

The downloaded histogram memory data has the content type
application/x-sinqhm. The body contains the histogrammed data in C
storage order and in network byte order. The reply to this request may
also be of the content type text/plain. In this case an error ocurred
and the request body contains an error message. 


For online status displays it is sometimes advisable to download only
a reduced data representation. To this purpose the call:\\
\centerline{{\em /admin/processdata.egi?bank=bankno\&command=commandtext}}\\
can be used. The bank parameter has the same meaning as above. The
command parameter is a string which describes the
 processing to perform.  The command string consists of a semicolon
 (;) separated list of subcommands which are performed in succession
 on the result of the preceding command or on the histogram memory data in
 case of the first subcommand. The following subcommands are supported:
\begin{description}
\item[sample:x1:x2:y2:y2..] for extracting a subset of the data. For
  each dimension of the data the start and end values for the
  subsampling need to be given. An example: consider a histogram
  memory with the dimensions 256,256,1024. This may be an area
  detector or 256x256 with time 1024 channels. The
  sample:0:256:0:256:510:511 would subsample the 510th frame out of the
  histogram memory. The result is a 2D dataset of size 256x256.
\item[sum:dimNo:start:end] will sum the data along the dimension dimNo
  from start to stop. Consider again the example histogram memory
  given above: then sum:2:0:1024 would sum all the time
  channels. Again the result is a 2D dataset of size 256x256. 
\end{description} 
Subcommands can be combined: using the sample example situation,
256x256x1024 the command string sample:0:256:0:256:100:200;sum:2:0:100
would return  a 256x256 dataset with the time channels 100 to 200
summed. 

The data downloaded through processdata.egi is in the same format as
described above, for readhmdata.egi.  

This process data scheme should cover most online status display
demands. For further instrument specific calculations, the server
would need to be extended.     

\subsection{Miscellaneous Requests}
There is a: \\
\centerline{{\em /admin/presethm.egi?value=val}}\\
function which presets the histogram memory to tha value val. This is
useful for debugging purposes. 

\section{Configuring the SinqHM}
The Sinq histogram memory server is configured via an XML file
containing configuration instructions.
This configuration file  can come from two sources: At startup of the
histogram memory server the file /etc/sinqhm.xml is read. This path
can be overriden by setting the environment variable
SINQHMCONFIG. This file is meant to contain a reasonable default
configuration for the histogram memory. In  many cases this is all
that is required. 

In many cases, however, the histogram memory has to be configured
dynamically from the instrument control computer. To this purpose a
http--POST request has to be sent to:\\
\centerline{{\em /admin/configure.egi}}\\
with the content of the new configuration as XML in the body. From the
command line this can for example be done with:
\begin{verbatim}
wget --post-file=newconfig.xml --http-user=XXX \\ \\
  --http-password=YYYY http://hm03.psi.ch/admin/configure.egi
\end{verbatim}
This is for linux and a recent version of wget. The format and content
of the XML file is documented in the following sections. 

\subsection{Guided Tour to SinqHM Configuration XML Files}
Below there is the simplest possible SinqHM XML configuration file:
\begin{verbatim}
<?xml version="1.0" encoding="UTF-8"?>
<sinqhm filler="dig" hdr_daq_mask=''0x000000'' hdr_daq_active=''0x000000''>
  <bank rank="1">
     <axis length="400" mapping="direct"/>
  </bank>
</sinqhm>
\end{verbatim}
The data for the sinqhm configuration is enclosed between the $<$sinqhm$>$
and $<$/sinqhm$>$ tags. Within the sinqhm tags there can be one to many
bank descriptions enclosed between $<$bank$>$ and $<$/bank$>$. Each bank must
have a  rank attribute giving the dimensions of the bank. Within the
bank tags there have to be rank axis descriptions. The example shown
is a linear PSD with 400 detectors and a direct mapping, i.e. detector
number in the fibre optic link packet directly map to histogram memory
detector number. 

As another example, consider a time axis to be added:
\begin{verbatim}
<?xml version="1.0" encoding="UTF-8"?>
<sinqhm filler="tof" hdr\_daq\_mask=''0x000000'' hdr\_daq\_active=''0x000000''>
<bank rank="2">
  <axis length="400" mapping="direct"/>
  <axis length="1000" mapping="boundary" array="tof"/>
</bank>
<tof rank="1" dim="1001">
         1000         2000         3000         4000         5000
         6000         7000         8000         9000        10000
        11000        12000        13000        14000        15000
        16000        17000        18000        19000        20000
        21000        22000        23000        24000        25000
        ............. many numbers cut here ....................
        26000        27000        28000        29000        30000
       986000       987000       988000       989000       990000
       991000       992000       993000       994000       995000
       996000       997000       998000       999000      1000000
</tof>
</sinqhm>
\end{verbatim}
In this case we have another filler and a second axis with the mapping
boundary which means bin boundary. The actual bin boundaries are
stored in an array with the name tof. This array is specified outside
of all bank tags.  

\subsection{SinqHM Configuration File Reference}
The examples above should have given a good example of the structure
of a SinqHM XML configuration file. This section details the possible
tags and their attributes.

\subsubsection{The Sinqhm Tag}
The sinqhm tag can have various attributes:
\begin{description}
\item[filler] This attribute selects which filler algorithm is used
  to do the actual histogramming. Currently implemented are:
\begin{description}
\item[dig] Simple linear histogramming
\item[tof] Time of flight processing
\item[tofmap] For the FOCUS 2D detector: a 2D detector  built from 65
  position sensitive tubes. 
\item[psd] The EMBL area detectors with PSI electronics.  
\item[hrpt] A special filler for HRPT which fixes electronics problems
  aquired commercially. 
\end{description}
\item[hdr\_daq\_mask] is a mask with which the headers of the data
  packets on the fibre optic link are anded. 
\item[hdr\_daq\_active]The value which the headers of the data packest
  anded with hdr\_daq\_mask must have in order to be considered a valid
  packet suitable for histogramming. These two attributes allow to
  histogram only when all veto flags are OK. 
\item[bankmaparray] with the value an array name. This is rarely
  used. It is only needed when multiple banks are present and the
  mapping which detector number belongs to which bank is unclear. Then
  this array describes which detector maps to which bank.  
\end{description}

\subsubsection{The Bank Tag}
The bank tag has only one attribute:
\begin{description}
\item[rank] The number of dimensions of the bank.
\end{description}

\subsubsection{The Axis Tag}
This tags describes an axis of the histogram memory. The axis tag has
the following attributes:
\begin{description}
\item[length] The length of the axis.
\item[mapping] The mapping type of the axis. Currently supported
  mapping types are:
\begin{description}
\item[direct] Detector number in packet is detector number in
  histogram.
\item[calculate] The detector number in the histogram memory must be
  calculated from the data in the packet. If calculate has been
  selected, the following additional attribute to axis are evaluated:
  \begin{description}
    \item[multiplier] A multiplier
    \item[divisor] A divisor
    \item[preoffset] An offset to apply befor division or
    multiplication.
    \item[postoffset] An offset to apply after division or multiplication.
  \end{description}
  The formula to arrive at the histogram memory detector number d from
  the value in the packet p is:
\begin{math}
  d = postoffset + (preoffset + p*multiplier)/divisor
\end{math} 
 If any of the parameters are not given or 0, then the operation is
 not performed: i.e. without a divisor no division. The application of
 this is a charge readout detector which in our case gives a number
 between -4000 and 4000 for the position. From this a physically
 meaningful detector position can be calculated from the formula.
\item[boundary] The histogram memory detector position is deduced
  through lookup in an array of bin boundaries. These boundaries must
  not necessarily be linear. This mode is commonly applied to time of
  flight data. This array should have length+1 bin boundary entries.
\item[lookuptable] This is for an area detector where the lookup for
  y is not independent of x. A 2D lookup array is required. 
\end{description}
\item[array] Some axis mappings, tof, lookup, need arrays for their
  data. This attribute gives the name of the array to use. 
\end{description}

\subsubsection{Arrays}
Arrays for tof and lookup axis types are storded as separate entities
within the XML file. The tag names must be those specified in the
array attribute of the axis tag. The array tags itself has the
following attributes:
\begin{description}
\item[rank] The number of dimensions of the array.
\item[dim] A comma separated list of values describing the length of
  each dimension. 
\end{description}
Between the closing and ending array tag is the array data. This is just
numbers. C storage order is assumed. 

A special case is the array for the lookup axis type. The application
is an area detector built from 65 linear position sensitive tubes. The
mapping of position on the linear tube to the desired position in the
histogram memory is different for each tube. Moreover it can happen
that the contribution from one position on the tube has to be split
between adjacent histogram memory positions.  In order to account for
that a three dimensional array has to be specified with the dimensions
nDetector x resolution-along-tube x 4. For each position there are
four values: tube-number position-on-tube target-position-in-hm
weight. The weight is 100 for a full contribution, less for a split
contribution. In the case of a split contribution, the remainder is
put into the next adjacent bin. In the end this gives histogram memory
data where each value is to high by 100. Two orders of magnitude in
intensity gained by pure magic!  

\section{Implementation Notes}
\begin{figure}
\caption{Hardware Overview} \label{hw}
\includegraphics[width=15cm]{dataSHM2.eps}
\end{figure}
\subsection{Common Files}
Both the server process and the filler process must access the shared
memory regions. There are access functions for each of the three
shared memory regions:
\begin{description}
\item[debugshm.c,h] The debugging shared memory
\item[controlshm.c,h] The control shared memory
\item[datashm.c,h] The data shared memory.
\end{description}
The layout of the data shared memory is shown in figure \ref{datashm}.
 It starts
with a structure of type hist\_descr\_type. This is followed optionally
by the bank mapping array. Then follow bank descriptions of type
bank\_descr\_type. Each bank description is followed by rank axis
decriptions of type axis\_descr\_type. Some axis types require additional
data: this is appended after the axis descriptions. If the data is an
array, then it is headed by an array descriptor of type
array\_descr\_type. Then follows the actual histogram data area for the
bank. All data types are documented in datashm.h.  Usually there is no
need to worry about the data layout; there are access functions in
datashm.h which take care of that. The use of those functions is
highly recommended.  


\subsection{The HTTP Server} 
Rather then implementing an own http server, a free embeddable WWW--server from appWeb
(http://www.appwebserver.org) was choosen. The appWeb server has a so
called EGI interface which allows to call ANSI--C functions for
requests to specific path strings. This allowed for the efficient
implementation of the histogram memories server functionality. For
dealing with the XML configuration files Michael Sweets mini--XML library
was used (http://www.easysw.com/~mike/mxml/). Besides that the HTTP
server is implemented in the following files:
\begin{description}
\item[sinqhmsrv.c,h] Functions which are common to histogram memory
  operations and independent of the choosen interface to the http
  server. This file also contains the configuration file processing.
\item[nxdataset.c,h] helper functions for data management.
\item[egiadapter.c,h]The adapter functions to the EGI interface of
  the appWeb WWW--server
\item[sinqhmegi.c] The main program which initializes the appWeb
  WWW--server, the EGI interface to the SinqHM functionality and runs
  the WWW--server. Most of this is boilerplate appWeb code. 
\end{description}
The appWeb WWW--server has its own configuration file,
sinqhmegi.conf. The format and content of this file is fully
documented at the appWeb WWW--site. But some of the variables are
special in that they might cause surprising results:
\begin{description}
\item[LimitRequestBody] is the maximum size of POST data. If there are
  very large configuration files, this may need to be increased. 
\item[KeepAliveTimeout, MaxKeepAliveRequests] control keepalive of
  connections. Originally the http protocoll required to close the
  connection after each request. In the context of a constantly
  monitored histogram memory server this would soon cause telephone
  calls from the network people because of excessive load on the name
  server. Http 1.1 then introduced keeaplive, i.e, the connection is
  kept alive for a certain period of time and for a certain number of
  requests. Keep these values high.
\end{description} 


\subsection{The Filler Program}
The filler program is a RTAI module. This module runs a periodic task
which checks the fibre optic link (LWL in the code) for incoming data
and if there is such data processes it. There is the problem that
depending on the filler type quite different processing must occur. In
order to cope with this, each filler has to implement four functions:  
\begin{description}
\item[extern int  (*process\_packet\_fcn)(packet\_type*)] for processing packets.
\item[extern int  (*process\_construct\_fcn)(void)] called when
  configuring the histogram memory. Has to check if the histogram
  memory configuration is compatible with the filler program.
\item[extern void (*process\_destruct\_fcn)(void)] clears up the configuration 
\item[extern void (*process\_init\_daq\_fcn)(void)] a function which is
  called each time data aquisition is started.
\end{description}
These filler specific functions are assigned to global function
pointers at configuration time and can thus be accessed efficiently
ever after. 

Resizing shared memory regions at runtime did not work reliably with
the Linux--RTAI version choosen. Therefore the size of the shared
memory segments are module parameters to the real time module.  The
shared memory is initialized by the realtime process from these
parameters. This also means that the realtime module has to be loaded
before the http--server can run.

All in all the filler process is implemented in the following files:
\begin{description}
\item[filler\_main.c] RTAI module initialization and task.
\item[lwlpmc.c] The fibre optic link access module.
\item[process\_common.c,h] Common filler functions.
\item[process\_dig, process\_tof, process\_tofmap, process\_psd,
  process\_hrpt] specific filler implementations.  
\item[rawdata.c] Store raw data from LWL. 
\end{description}


\subsection{Communication between Filler and Server Process}
In normal operation the filler process and the http--server can run
independently of each other. However when starting a measurement or
during configuration the two processes need to agree on some
protocoll. This is currently implemented via flags in the control
shared memory.  

When the http--server wishes to configure the histogram memory its sets
the flag CFG\_UA\_DO\_CFG\_CMD  to 0. The filler acknowlegdes this by
setting CFG\_RT\_DO\_CFG\_ACK to 0, too. Then the configuration can
proceed. When the http--server is finished configuring it sets 
CFG\_UA\_DO\_CFG\_CMD to 1. This is acknowledged by the filler through
setting CFG\_RT\_DO\_CFG\_ACK to 1, too. The filler also sets
CFG\_RT\_CFG\_STATUS to CFG\_RT\_CFG\_VALID when the configuration is
compatible with the filler. If not CFG\_RT\_CFG\_STATUS is set to a
negative error code. Configuration is not allowed during data
aquisition. 

When starting data aquistion the http--server sets CFG\_UA\_DO\_DAQ\_CMD to
1. This is acknowledged by the filler setting CFG\_RT\_DO\_DAQ\_ACK to 1,
too.  Stopping follows the same procedure, just replace the 1 with a
0. Pausing data aquisition can be accomplished by setting
CFG\_UA\_DAQ\_PAUSE\_CMD to 1. Continuing a paused  data aquisition can be
done by setting CFG\_UA\_DAQ\_PAUSE\_CMD to 0. 

This communication scheme works fine as long as the http--server is
single threaded. If it would be multi threaded it is theoreticaly
possible that multiple clients interfer with each other  when they try
to configure or start or stop DAQ at the same time. Even single
threaded the appWeb server can handle multiple connections. As a
histogram mmeory server should only be accessed from the instrument
control computer and may be a handful status clients this limitation
should not cause trouble. 



\subsection{Porting SinqHM to other Hardware Environments}
Other hardware environments most likely will differ in the way how the
data is read from the DAQ electronics. In such case a new filler
algorithm must be written. Most of the server side software can be left
intact excpet minor changes to account for the new filler type.  


\section{Installing and Running SinqHM}
For test and debugging purposes the sinqhm-filler program and the 
 http server can be executed on any standard vanilla linux system. In this
 case neutron events can be fed into the filler program by writing to a named 
 pipe. The default name of this pipe is /tmp/lwlfifo. The data to write are 
 hexadecimal values representing the SINQ LWL packet format which is
 documented elsewhere. 

\subsection{Running the Debug Version on Vanilla Linux}
\begin{enumerate}
\item Build the software by running ./build\_user.c86.sh
\item Start the filler by typing distrib/server/sinqhm\_filler
\item In another window start the server: distrib/server/sinqhmegi
\end{enumerate}
The arguments for the filler program are the same as the ones used for
the kernel module. There is one extra parameter, though, which allows
to configure the name of the named pipe for writing LWL data to. This
parameter is:
\begin{description}
\item[lwlfifo=/x/y] named pipe for lwl data, default: /tmp/lwlfifo
\end{description}
An example:
\begin{verbatim}
mkfifo /tmp/lwlfifo
./sinqhm_filler&
./sinqhmegi&
cat lwl_data > /tmp/lwlfifo
\end{verbatim}


\subsection{Running under RTAI}
RTAI must be properly installed and the rtai modules must be loaded.

The filler realtime module must be installed before the http server is started.
Histo and Dbg SHM Memory sizes can be set via kernel parameters 
and are fixed after start-up. The CFG SHM size is always fixed.

The server configures the histo memory only if no valid config in 
SHM found. This allows the server app to  be restarted (eg. after
a crash - what of course never happens -:) without disturbing an
active data acquisition.

\subsubsection{Loading the Fille Module}
The filler module must be loaded into the RTAI-linux kernel by calling:
\begin{verbatim}
insmod sinqhm_filler.o 
\end{verbatim}

The filler module can hvae the following parameters. 
(NOTE: No spaces before or after '=' allowed)
\begin{description}
\item[hst\_size=xxxx]   Histo SHM size in byte, default: 20971520 byte
\item[dbg\_size=xxxx]   Debug SHM size in byte, default: 1048576 byte
\item[task\_period=xx]  Realtime task period in ns, default  1000000 ns
\item[rt\_duty\_cycle=xx] Duty cycle of realtime task in percent, 
  default 80 percent. This is the maximum amount of CPU time the
  filler task may use.
\end{description}

An example: install the realtime filler module with 400 MB data space:
\begin{verbatim}
insmod sinqhm_filler.o  hst\_size=419430400
\end{verbatim}

\subsubsection{Removing the Filler Module}
The filler module can be removed from the kernel using the command:
\begin{verbatim}
rmmod sinqhm_filler
\end{verbatim}
Experience has shown that RTAI does not like this very much and
becomes unstable. A better solution is to reboot the computer.



\section{Support Programs}
\subsection{vmetgen}
This program reads data from stdio or a named pipe and sends it to the
lwl testgenerator. This is a special VME module which is able to write
data to the LWL. The
following parameters are understood:
\begin{description}
\item[base=xxxx] base address of vme lwl testgenerator in hex
\item[lwlfifo] read data from named pipe /tmp/lwlfifo instead of stdio
\item[lwlfifo=xxx] read data from named pipe xxx instead of stdio
\end{description}

A usage example: 
\begin{verbatim}
echo 0x052a36be 0x000c | ./vmetgen base=1800
\end{verbatim}
or
\begin{verbatim}
mkfifo /tmp/my_tmp_lwl_fifo
./vmetgen lwlfifo=/tmp/my_tmp_lwl_fifo &
cat testdata > /tmp/my_tmp_lwl_fifo
\end{verbatim}

\end{document}
