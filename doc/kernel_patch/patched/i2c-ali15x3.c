/*
    ali15x3.c - Part of lm_sensors, Linux kernel modules for hardware
              monitoring
    Copyright (c) 1999  Frodo Looijaard <frodol@dds.nl> and
    Philip Edelbrock <phil@netroedge.com> and
    Mark D. Studebaker <mdsxyz123@yahoo.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

/*
    This is the driver for the SMB Host controller on
    Acer Labs Inc. (ALI) M1541 and M1543C South Bridges.

    The M1543C is a South bridge for desktop systems.
    The M1541 is a South bridge for portable systems.
    They are part of the following ALI chipsets:
       "Aladdin Pro 2": Includes the M1621 Slot 1 North bridge
       with AGP and 100MHz CPU Front Side bus
       "Aladdin V": Includes the M1541 Socket 7 North bridge
       with AGP and 100MHz CPU Front Side bus
       "Aladdin IV": Includes the M1541 Socket 7 North bridge
       with host bus up to 83.3 MHz.
    For an overview of these chips see http://www.acerlabs.com

    The M1533/M1543C devices appear as FOUR separate devices
    on the PCI bus. An output of lspci will show something similar
    to the following:

  00:02.0 USB Controller: Acer Laboratories Inc. M5237
  00:03.0 Bridge: Acer Laboratories Inc. M7101
  00:07.0 ISA bridge: Acer Laboratories Inc. M1533
  00:0f.0 IDE interface: Acer Laboratories Inc. M5229

    The SMB controller is part of the 7101 device, which is an
    ACPI-compliant Power Management Unit (PMU).

    The whole 7101 device has to be enabled for the SMB to work.
    You can't just enable the SMB alone.
    The SMB and the ACPI have separate I/O spaces.
    We make sure that the SMB is enabled. We leave the ACPI alone.

    This driver controls the SMB Host only.
    The SMB Slave controller on the M15X3 is not enabled.

    This driver does not use interrupts.
*/

/* Note: we assume there can only be one ALI15X3, with one SMBus interface */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <linux/kernel.h>
#include <linux/stddef.h>
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/i2c.h>

#include <asm/pci-bridge.h>



#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,1,54))
#include <linux/bios32.h>
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,1,53)
#include <linux/init.h>
#else
#define __init 
#define __initdata 
#endif

#undef FORCE_ALI15X3_ENABLE

/* ALI15X3 SMBus address offsets */
#define SMBHSTSTS (0 + ali15x3_smba)
#define SMBHSTCNT (1 + ali15x3_smba)
#define SMBHSTSTART (2 + ali15x3_smba)
#define SMBHSTCMD (7 + ali15x3_smba)
#define SMBHSTADD (3 + ali15x3_smba)
#define SMBHSTDAT0 (4 + ali15x3_smba)
#define SMBHSTDAT1 (5 + ali15x3_smba)
#define SMBBLKDAT (6 + ali15x3_smba)

/* PCI Address Constants */
#define SMBCOM    0x004 
#define SMBBA     0x014
#define SMBATPC   0x05B   /* used to unlock xxxBA registers */
#define SMBHSTCFG 0x0E0
#define SMBSLVC   0x0E1
#define SMBCLK    0x0E2
#define SMBREV    0x008

/* Other settings */
#define MAX_TIMEOUT 50   /* times 1/100 sec */
#define ALI15X3_SMB_IOSIZE 32

/* this is what the Award 1004 BIOS sets them to on a ASUS P5A MB.
   We don't use these here. If the bases aren't set to some value we
   tell user to upgrade BIOS and we fail.
*/
#define ALI15X3_SMB_DEFAULTBASE 0xE800

/* ALI15X3 address lock bits */
#define ALI15X3_LOCK  0x06

/* ALI15X3 command constants */
#define ALI15X3_ABORT      0x04
#define ALI15X3_T_OUT      0x08
#define ALI15X3_QUICK      0x00
#define ALI15X3_BYTE       0x10
#define ALI15X3_BYTE_DATA  0x20
#define ALI15X3_WORD_DATA  0x30
#define ALI15X3_BLOCK_DATA 0x40
#define ALI15X3_BLOCK_CLR  0x80

/* ALI15X3 status register bits */
#define ALI15X3_STS_IDLE  0x04          
#define ALI15X3_STS_BUSY  0x08          
#define ALI15X3_STS_DONE  0x10          
#define ALI15X3_STS_DEV   0x20  /* device error */
#define ALI15X3_STS_COLL  0x40  /* collision or no response */
#define ALI15X3_STS_TERM  0x80  /* terminated by abort */
#define ALI15X3_STS_ERR   0xE0  /* all the bad error bits */


/* If force_addr is set to anything different from 0, we forcibly enable
   the device at the given address. */

#if defined(CONFIG_MENA15) || defined (CONFIG_MENA12)
/* workaround to get RTC to work on the a15/a12. The smbus is forced back t
o the memory location where MenMon sets it up*/
static u16 force_addr = 0x1800;
#else
static u16 force_addr = 0;
#endif


#ifdef MODULE
static
#else
extern
#endif
       int __init i2c_ali15x3_init(void);
static int __init ali15x3_cleanup(void);
static int ali15x3_setup(void);
static s32 ali15x3_access(struct i2c_adapter *adap, u16 addr, 
                          unsigned short flags,char read_write,
                          u8 command, int size, union i2c_smbus_data * data);
static int ali15x3_transaction(void);
static void ali15x3_inc(struct i2c_adapter *adapter);
static void ali15x3_dec(struct i2c_adapter *adapter);
static u32 ali15x3_func(struct i2c_adapter *adapter);

#ifdef MODULE
extern int init_module(void);
extern int cleanup_module(void);
#endif /* MODULE */

static struct i2c_algorithm smbus_algorithm = {
  /* name */    "Non-I2C SMBus adapter",
  /* id */    I2C_ALGO_SMBUS,
  /* master_xfer */ NULL,
  /* smbus_access */    ali15x3_access,
  /* slave_send */  NULL,
  /* slave_rcv */ NULL,
  /* algo_control */  NULL,
  /* functionality */   ali15x3_func,
};

static struct i2c_adapter ali15x3_adapter = {
  "unset",
  I2C_ALGO_SMBUS | I2C_HW_SMBUS_ALI15X3,
  &smbus_algorithm,
  NULL,
  ali15x3_inc,
  ali15x3_dec,
  NULL,
  NULL,
};

static int __initdata ali15x3_initialized;
static int __initdata ali15x3_setup_before;
static unsigned short ali15x3_smba = 0;
int ali15x3_early_transaction = 1;


#define tic_read(var) __asm volatile("sync" "\n\t" "mftb %0" "\n\t" "sync" : "=r" (var) )

void i2c_udelay(unsigned int usec)
{
  unsigned int t1, t2;
  // tic duration is 40 ns for MEN A012c
  tic_read(t1);
  do {
    tic_read(t2);
  } while ( (t2-t1) < (usec*25) );
}





/* Detect whether a ALI15X3 can be found, and initialize it, where necessary.
   Note the differences between kernels with the old PCI BIOS interface and
   newer kernels with the real PCI interface. In compat.h some things are
   defined to make the transition easier. */

int ali15x3_setup_early(struct  pci_controller* hose, int devnb)
{
  int error_return=0;
  unsigned char temp;

  unsigned char ALI15X3_bus = 0, ALI15X3_devfn = PCI_DEVFN(devnb, 0);  

  if(ali15x3_setup_before)
    return -EBUSY;

  ali15x3_setup_before = 0;


/* Unlock the register.
   The data sheet says that the address registers are read-only
   if the lock bits are 1, but in fact the address registers
   are zero unless you clear the lock bits.
*/
  early_read_config_byte(hose, ALI15X3_bus, ALI15X3_devfn, SMBATPC, &temp);
  if(temp & ALI15X3_LOCK)
  {   
    temp &= ~ALI15X3_LOCK;
    early_write_config_byte(hose, ALI15X3_bus, ALI15X3_devfn, SMBATPC, temp);
  }

  if (force_addr)
  {
    early_write_config_word(hose, ALI15X3_bus, ALI15X3_devfn, SMBBA, force_addr );
    printk("i2c-ali15x3.o: forcing ali15x3_smba to 0x%04x \n", force_addr);
  }

  /* Determine the address of the SMBus area */
  early_read_config_word(hose, ALI15X3_bus, ALI15X3_devfn, SMBBA, &ali15x3_smba);
  ali15x3_smba &= (0xffff & ~ (ALI15X3_SMB_IOSIZE - 1));

  if (force_addr && (force_addr != ali15x3_smba))
  {
    printk("i2c-ali15x3.o: error forcing to 0x%04x, ali15x3_smba = 0x%04x \n", force_addr, ali15x3_smba);
    error_return=-ENODEV;
  }
  else if(ali15x3_smba == 0) {
    printk("i2c-ali15x3.o: ALI15X3_smb region uninitialized - upgrade BIOS?\n");
    error_return=-ENODEV;
  }

  if(error_return == -ENODEV)
    goto END;


/* check if whole device is enabled */
  early_read_config_byte(hose, ALI15X3_bus, ALI15X3_devfn, SMBCOM, &temp);
  if ((temp & 1) == 0) {
    printk("SMBUS: Error: SMB device not enabled - upgrade BIOS?\n");     
    error_return=-ENODEV;
    goto END;
  }


#if defined(CONFIG_MENA15) || defined (CONFIG_MENA12)
  /* set SMB clock to 37KHz */  // 0x50 to increase idle delay
  early_write_config_byte(hose, ALI15X3_bus ,ALI15X3_devfn, SMBCLK, 0x50);
#else
  /* set SMB clock to 74KHz as recommended in data sheet */
  early_write_config_byte(hose, ALI15X3_bus ,ALI15X3_devfn, SMBCLK, 0x20);
#endif

//  early_read_config_byte(hose, ALI15X3_bus, ALI15X3_devfn, SMBCLK, &temp);
//  printk("i2c-ali15x3.o: smb clock setting: 0x%02x \n", temp);


  /* enable smb*/
  early_write_config_byte(hose, ALI15X3_bus, ALI15X3_devfn, SMBHSTCFG, 0x01);


END:
  ali15x3_setup_before = 1;
  return error_return;
}


int ali15x3_setup(void)
{
  int error_return=0;
  unsigned char temp;
  struct pci_dev *ALI15X3_dev;

  if(ali15x3_setup_before)
    goto END;

  /* First check whether we can access PCI at all */  
  if (pci_present() == 0) {
    printk("i2c-ali15x3.o: Error: No PCI-bus found!\n");
    error_return=-ENODEV;
    goto END;
  }

  /* Look for the ALI15X3, M7101 device */
  ALI15X3_dev = NULL;
  ALI15X3_dev = pci_find_device(PCI_VENDOR_ID_AL, 
                                PCI_DEVICE_ID_AL_M7101, ALI15X3_dev);
  if(ALI15X3_dev == NULL) {
    printk("i2c-ali15x3.o: Error: Can't detect ali15x3!\n");
    error_return=-ENODEV;
    goto END;
  } 

/* Check the following things:
  - SMB I/O address is initialized
  - Device is enabled
  - We can use the addresses
*/

/* Unlock the register.
   The data sheet says that the address registers are read-only
   if the lock bits are 1, but in fact the address registers
   are zero unless you clear the lock bits.
*/
  pci_read_config_byte(ALI15X3_dev, SMBATPC, &temp);
  if(temp & ALI15X3_LOCK)
  {   
    temp &= ~ALI15X3_LOCK;
    pci_write_config_byte(ALI15X3_dev, SMBATPC, temp);
  }

/* Determine the address of the SMBus area */
  pci_read_config_word(ALI15X3_dev, SMBBA,&ali15x3_smba);
  ali15x3_smba &= (0xffff & ~ (ALI15X3_SMB_IOSIZE - 1));
  if(ali15x3_smba == 0) {
    printk("i2c-ali15x3.o: ALI15X3_smb region uninitialized - upgrade BIOS?\n");
    error_return=-ENODEV;
  }

  if(error_return == -ENODEV)
    goto END;

  if (check_region(ali15x3_smba, ALI15X3_SMB_IOSIZE)) {
    printk("i2c-ali15x3.o: ALI15X3_smb region 0x%x already in use!\n", ali15x3_smba);
    error_return=-ENODEV;
  }

  if(error_return == -ENODEV)
    goto END;

/* check if whole device is enabled */
    pci_read_config_byte(ALI15X3_dev, SMBCOM, &temp);
  if ((temp & 1) == 0) {
    printk("SMBUS: Error: SMB device not enabled - upgrade BIOS?\n");     
    error_return=-ENODEV;
    goto END;
  }

/* Is SMB Host controller enabled? */
  pci_read_config_byte(ALI15X3_dev, SMBHSTCFG, &temp);
#ifdef FORCE_ALI15X3_ENABLE
/* This should never need to be done.
   NOTE: This assumes I/O space and other allocations WERE
   done by the Bios!  Don't complain if your hardware does weird 
   things after enabling this. :') Check for Bios updates before
   resorting to this.  */
  if ((temp & 1) == 0) {
    pci_write_config_byte(ALI15X3_dev, SMBHSTCFG, temp | 1);
    printk("i2c-ali15x3.o: WARNING: ALI15X3 SMBus interface has been FORCEFULLY "
           "ENABLED!!\n");
  }
#else /* FORCE_ALI15X3_ENABLE */
  if ((temp & 1) == 0) {
    printk("SMBUS: Error: Host SMBus controller not enabled - upgrade BIOS?\n");     
    error_return=-ENODEV;
    goto END;
  }
#endif /* FORCE_ALI15X3_ENABLE */

/* set SMB clock to 74KHz as recommended in data sheet */
  pci_write_config_byte(ALI15X3_dev, SMBCLK, 0x20);

  /* Everything is happy, let's grab the memory and set things up. */
  request_region(ali15x3_smba, ALI15X3_SMB_IOSIZE, "ali15x3-smb");       

#ifdef DEBUG
/*
  The interrupt routing for SMB is set up in register 0x77 in the
  1533 ISA Bridge device, NOT in the 7101 device.
  Don't bother with finding the 1533 device and reading the register.
  if ((....... & 0x0F) == 1)
     printk("i2c-ali15x3.o: ALI15X3 using Interrupt 9 for SMBus.\n");
*/
  pci_read_config_byte(ALI15X3_dev, SMBREV, &temp);
  printk("i2c-ali15x3.o: SMBREV = 0x%X\n",temp);
  printk("i2c-ali15x3.o: ALI15X3_smba = 0x%X\n",ali15x3_smba);
#endif /* DEBUG */

END:
  return error_return;
}

/* Another internally used function */
int ali15x3_transaction(void) 
{
  int temp;
  int result=0;
  int timeout=0;
  unsigned long _flags;

#ifdef DEBUG
  printk("i2c-ali15x3.o: Transaction (pre): STS=%02x, CNT=%02x, CMD=%02x, ADD=%02x, DAT0=%02x, "
         "DAT1=%02x\n",
         inb_p(SMBHSTSTS), inb_p(SMBHSTCNT),inb_p(SMBHSTCMD),inb_p(SMBHSTADD),inb_p(SMBHSTDAT0),
         inb_p(SMBHSTDAT1));
#endif

  /* get status */
  temp = inb_p(SMBHSTSTS);

  /* Make sure the SMBus host is ready to start transmitting */
  /* and check the error bits and the busy bit */
  if ((!(temp & ALI15X3_STS_IDLE)) || (temp & (ALI15X3_STS_ERR | ALI15X3_STS_BUSY))) 
  {
    return -1;
  }
  i2c_udelay(1000);
 #if 0
  /* start the transaction by writing anything to the start register */
  outb_p(0x10, SMBHSTSTS);
  outb_p(0xFF, SMBHSTSTART); 
#endif


#if 1
  __save_flags(_flags);
  cli();

  /* start the transaction by writing anything to the start register */
  outb_p(0xFF, SMBHSTSTART); 

  /* 
   The following line kicks off the start in case it did not start 
   due to the start command 
  */
  outb_p(0x10, SMBHSTSTS);
  __restore_flags(_flags);
#endif


  /* We will always wait for a fraction of a second! */
  timeout = 0;
  do 
  {
    i2c_udelay(1000);
    temp=inb_p(SMBHSTSTS);
  } while ((!(temp & (ALI15X3_STS_ERR | ALI15X3_STS_DONE))) && (timeout++ < MAX_TIMEOUT));

  /* If the SMBus is still busy, we give up */
  if (timeout >= MAX_TIMEOUT) {
    result = -1;
#ifdef DEBUG
    printk("i2c-ali15x3.o: SMBus Timeout!\n"); 
#endif
  }

  if (temp & ALI15X3_STS_TERM) {
    result = -1;
#ifdef DEBUG
    printk("i2c-ali15x3.o: Error: Failed bus transaction\n");
#endif
  }

/*
  Unfortunately the ALI SMB controller maps "no response" and "bus collision"
  into a single bit. No reponse is the usual case so don't
  do a printk.
  This means that bus collisions go unreported.
*/
  if (temp & ALI15X3_STS_COLL) {
    result = -1;
#ifdef DEBUG
    printk("i2c-ali15x3.o: Error: no response or bus collision ADD=%02x\n", inb_p(SMBHSTADD));
#endif
  }

/* haven't ever seen this */
  if (temp & ALI15X3_STS_DEV) {
    result = -1;
    printk("i2c-ali15x3.o: Error: device error\n");
  }

#ifdef DEBUG
  printk("i2c-ali15x3.o: Transaction (post): STS=%02x, CNT=%02x, CMD=%02x, ADD=%02x, "
         "DAT0=%02x, DAT1=%02x\n",
         inb_p(SMBHSTSTS), inb_p(SMBHSTCNT),inb_p(SMBHSTCMD),inb_p(SMBHSTADD),inb_p(SMBHSTDAT0),
         inb_p(SMBHSTDAT1));
#endif
  return result;
}

/* Return -1 on error. See smbus.h for more information */
s32 ali15x3_access_core(struct i2c_adapter *adap, u16 addr, unsigned short flags,
                   char read_write, u8 command, int size, 
                   union i2c_smbus_data * data)
{
  int i,len;
  int temp;
  int timeout;

  /* clear all the bits (clear-on-write) */
  outb_p(0xFF, SMBHSTSTS); 

  /* make sure SMBus is idle */
  timeout = 0;
  do 
  {
    i2c_udelay(1000);
    temp=inb_p(SMBHSTSTS);
  } while ( (!(temp & ALI15X3_STS_IDLE)) && (timeout++ < MAX_TIMEOUT));

  if (timeout >= MAX_TIMEOUT) {
#ifdef DEBUG
    printk("i2c-ali15x3.o: Idle wait Timeout! STS=0x%02x\n", temp);
#endif 
    return -1;
  }

  switch(size) {
    case I2C_SMBUS_PROC_CALL:
      printk("i2c-ali15x3.o: I2C_SMBUS_PROC_CALL not supported!\n");
      return -1;
    case I2C_SMBUS_QUICK:
      outb_p(((addr & 0x7f) << 1) | (read_write & 0x01), SMBHSTADD);
      size = ALI15X3_QUICK;
      break;
    case I2C_SMBUS_BYTE:
      outb_p(((addr & 0x7f) << 1) | (read_write & 0x01), SMBHSTADD);
      if (read_write == I2C_SMBUS_WRITE)
        outb_p(command, SMBHSTCMD);
      size = ALI15X3_BYTE;
      break;
    case I2C_SMBUS_BYTE_DATA:
      outb_p(((addr & 0x7f) << 1) | (read_write & 0x01), SMBHSTADD);
      outb_p(command, SMBHSTCMD);
      if (read_write == I2C_SMBUS_WRITE)
        outb_p(data->byte,SMBHSTDAT0);
      size = ALI15X3_BYTE_DATA;
      break;
    case I2C_SMBUS_WORD_DATA:
      outb_p(((addr & 0x7f) << 1) | (read_write & 0x01), SMBHSTADD);
      outb_p(command, SMBHSTCMD);
      if (read_write == I2C_SMBUS_WRITE) {
        outb_p(data->word & 0xff,SMBHSTDAT0);
        outb_p((data->word & 0xff00) >> 8,SMBHSTDAT1);
      }
      size = ALI15X3_WORD_DATA;
      break;
    case I2C_SMBUS_BLOCK_DATA:
      outb_p(((addr & 0x7f) << 1) | (read_write & 0x01), SMBHSTADD);
      outb_p(command, SMBHSTCMD);
      if (read_write == I2C_SMBUS_WRITE) {
        len = data->block[0];
        if (len < 0) {
          len = 0;
          data->block[0] = len;
        } 
        if (len > 32) {
          len = 32;
          data->block[0] = len;
        } 
        outb_p(len,SMBHSTDAT0);
        outb_p(inb_p(SMBHSTCNT) | ALI15X3_BLOCK_CLR, SMBHSTCNT); /* Reset SMBBLKDAT */
        for (i = 1; i <= len; i++)
          outb_p(data->block[i],SMBBLKDAT);
      }
      size = ALI15X3_BLOCK_DATA;
      break;
  }

  outb_p(size, SMBHSTCNT);  /* output command */

  if (ali15x3_transaction()) /* Error in transaction */ 
    return -1; 
  
  if ((read_write == I2C_SMBUS_WRITE) || (size == ALI15X3_QUICK))
    return 0;
  

  switch(size) {
    case ALI15X3_BYTE: /* Result put in SMBHSTDAT0 */
      data->byte = inb_p(SMBHSTDAT0);
      break;
    case ALI15X3_BYTE_DATA:
      data->byte = inb_p(SMBHSTDAT0);
      break;
    case ALI15X3_WORD_DATA:
      data->word = inb_p(SMBHSTDAT0) + (inb_p(SMBHSTDAT1) << 8);
      break;
    case ALI15X3_BLOCK_DATA:
      len = inb_p(SMBHSTDAT0);
      if(len > 32)  
        len = 32;
      data->block[0] = len;
      outb_p(inb_p(SMBHSTCNT) | ALI15X3_BLOCK_CLR, SMBHSTCNT); /* Reset SMBBLKDAT */
      for (i = 1; i <= data->block[0]; i++) {
        data->block[i] = inb_p(SMBBLKDAT);
#ifdef DEBUG
        printk("i2c-ali15x3.o: Blk: len=%d, i=%d, data=%02x\n", len, i, data->block[i]);
#endif /* DEBUG */
      }
      break;
  }
  return 0;
}


#define I2C_MAX_MESSAGE_COUNT 100
#define I2C_MAX_TRIES 3

static s32 ali15x3_access(struct i2c_adapter * adap, u16 addr,
       unsigned short flags, char read_write, u8 command,
       int size, union i2c_smbus_data * data)
{
  s32 status;
  u8 temp;
  int errors = 0;

  do
  {
    status = ali15x3_access_core(adap, addr, flags, read_write, command, size, data);
    if (status < 0)
    {
      errors++;
      if(errors<2) 
      {
        /* reset host controller */
        outb_p( ALI15X3_ABORT, SMBHSTCNT);
#ifdef DEBUG
        printk("I2C ERROR: Resetting host controller\n");
#endif
      }
      else
      {
        /* reset host controller and all devices on bus */
        outb_p(ALI15X3_T_OUT, SMBHSTCNT);
#ifdef DEBUG
        printk("I2C ERROR: Resetting entire Bus\n");
#endif
      }

      i2c_udelay(5000);
      /* clear bits in status register */
      outb_p(0xFF, SMBHSTSTS); 
      i2c_udelay(5000);

#ifdef DEBUG
      printk("I2C Status after reset: STS=%02x, CNT=%02x, CMD=%02x, "
      "ADD=%02x, DAT0=%02x, DAT1=%02x\n\n", inb_p(SMBHSTSTS),
      inb_p(SMBHSTCNT), inb_p(SMBHSTCMD), inb_p(SMBHSTADD),
      inb_p(SMBHSTDAT0), inb_p(SMBHSTDAT1));
#endif

      if(errors >= I2C_MAX_TRIES) 
      {
        printk("I2C ERROR: acces failed after %d tries\n",errors);
      }
    }

  } while( (status<0) && (errors < I2C_MAX_TRIES));

  return status;
}


void ali15x3_inc(struct i2c_adapter *adapter)
{
  MOD_INC_USE_COUNT;
}

void ali15x3_dec(struct i2c_adapter *adapter)
{

  MOD_DEC_USE_COUNT;
}

u32 ali15x3_func(struct i2c_adapter *adapter)
{
  return I2C_FUNC_SMBUS_QUICK | I2C_FUNC_SMBUS_BYTE |
               I2C_FUNC_SMBUS_BYTE_DATA | I2C_FUNC_SMBUS_WORD_DATA |
               I2C_FUNC_SMBUS_BLOCK_DATA;
}

int __init i2c_ali15x3_init(void)
{
  int res;
  printk("ali15x3 i2c adapter\n");
#ifdef DEBUG
/* PE- It might be good to make this a permanent part of the code! */
  if (ali15x3_initialized) {
    printk("i2c-ali15x3.o: Oops, ali15x3_init called a second time!\n");
    return -EBUSY;
  }
#endif
  ali15x3_initialized = 0;
  if ((res = ali15x3_setup())) {
    printk("i2c-ali15x3.o: ALI15X3 not detected, module not inserted.\n");
    ali15x3_cleanup();
    return res;
  }
  ali15x3_initialized ++;
  sprintf(ali15x3_adapter.name,"SMBus ALI15X3 adapter at %04x",ali15x3_smba);
  if ((res = i2c_add_adapter(&ali15x3_adapter))) {
    printk("i2c-ali15x3.o: Adapter registration failed, module not inserted.\n");
    ali15x3_cleanup();
    return res;
  }
  ali15x3_initialized++;
  printk("i2c-ali15x3.o: ALI15X3 SMBus Controller detected and initialized\n");
  return 0;
}

int __init ali15x3_cleanup(void)
{
  int res;
  if (ali15x3_initialized >= 2)
  {
    if ((res = i2c_del_adapter(&ali15x3_adapter))) {
      printk("i2c-ali15x3.o: i2c_del_adapter failed, module not removed\n");
      return res;
    } else
      ali15x3_initialized--;
  }
  if (ali15x3_initialized >= 1) {
    release_region(ali15x3_smba, ALI15X3_SMB_IOSIZE);
    ali15x3_initialized--;
  }
  return 0;
}

EXPORT_NO_SYMBOLS;

#ifdef MODULE

MODULE_AUTHOR("Frodo Looijaard <frodol@dds.nl>, Philip Edelbrock <phil@netroedge.com>, and Mark D. Studebaker <mdsxyz123@yahoo.com>");
MODULE_DESCRIPTION("ALI15X3 SMBus driver");

int init_module(void)
{
  return i2c_ali15x3_init();
}

int cleanup_module(void)
{
  return ali15x3_cleanup();
}

#endif /* MODULE */
