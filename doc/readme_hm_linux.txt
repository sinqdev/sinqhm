SINQ Histo Memory Installation Guide
====================================

1) Quick Installation (with existing linux image files)
=======================================================

The files /home/sinqhm/target_fs/sinqhm.tgz and /home/sinqhm/tftpboot/ppcimage must 
exist on lnsl15.psi.ch. and the tftp-server and ssh-demon must be running on lnsl15.
Read section "Building Linux Image Files" for building these files.


- Make sure that the MEN A12 is configured for 5V PMC I/O Voltage.
  (This is done by removing Resistor R814 (0 Ohm) and installing R815 (0 Ohm) - See schematic !)
  Install the PMC LWL-Receiver Board

- Insert a 512 MByte Compact Flash card into MEN A12 board

- Establish a serial Connection to the MEN A12 CPU board,
  if it had been previously configured, press 'ESC' during boot to stop at MenMon

- Make sure that the MEN A12 CPU board has MenMon Version 3.3 or higher installed,
  otherwise go to section "Update MenMon"
  
- Configure the A12, as shown in the example for hm04 (129.129.196.44) but use the 
  ip-addresses corresponding to the new histo memory and its correct name


    MenMon> ee-st-mode quick
    MenMon> ee-st-halt delay
    MenMon> ee-st-msg  small
    MenMon> ee-vme-a24sa ff
    MenMon> ee-vmode 3
    MenMon> ee-pci-stgath 0
    MenMon> ee-pci-specrd 0
    MenMon> rtc-set 08 01 22 18 45 00      //RTC-SET YY MM DD hh mm ss

    MenMon> ee-netip    129.129.196.44:ffffff00
    MenMon> ee-netgw    129.129.196.1
    MenMon> ee-nethost  129.129.138.22

    MenMon> ee-xkerpar  1
    MenMon> ee-kerpar   ip=129.129.196.44::129.129.196.1:255.255.255.0:hm04:eth0: rootfstype=tmpfs root=/dev/ram rw
    MenMon> ee-bootfile ppcimage
    MenMon> nboot tftp

- login as root, use root password of lnsl15

    hm04 login: root
    Password: ********


- execute 'cf_install' script file, this will partition /dev/hda and copy
  data from lnsl15:/home/sinqhm/target_fs/sinqhm.tgz
  WARNING: ALL PREVIOUS DATA FROM /dev/hda WILL BE DESTROYED !!!
  Reboot when the script has finished.

    [root@hm04 ~]# cf_install
    :
    :
    [root@hm04 ~]# reboot

- Stop at MenMon and configure for booting from compact flash, reset board when finished

    MenMon> ee-startup dboot
    MenMon> rst
    
    

1.1) Update MenMon
==================

MenMon V3.3: /afs/psi.ch/group/sinqhm/sinqhmrtai/doc/configuration/MenMon_update/menmon_A012_33.smm

Connect a terminal emulation program with the COM 1 port of your target board and set the
terminal emulation program to 9600 baud, 8 data bits, 1 stop bit, no parity, no handshaking
(if you haven't changed the target baudrate on your own).

Power on your target board, and press "ESC" immediately.

In your terminal emulation program, you should see the "MenMon>" prompt.

Enter "SERDL MENMON". You should now see a "C" character appear every 3 seconds.

In your terminal emulation program, start a "YModem" download of file "menmon_A012_33.smm"
(for example, with Windows� Hyperterm, select "Transfer|Send File" Protocol="YModem").


linux:
under minicom: 
SERDL MENMON
in shell:
sb --ymodem -b menmon_A012_33.smm < /dev/ttyS0 > /dev/ttyS0


When the download is completed, reset the target CPU board.

 
 


2) Building Linux Image Files
=============================


2.1) Elinos with Kernel patch
=============================

Elinos has to be installed in '/opt/elinos-4.0' on lnsl15.psi.ch.
For the special boot strategics for SinqHM  a patch is required
for executing the script '/linuxrc2' before the '/sbin/init' process
is started.

This patch changes '/opt/elinos-4.0/linux/linux-ppc-2.4.31/init/do_mounts.c'

diff original/do_mounts.c patched/do_mounts.c
834a835,859
> static int do_linuxrc2(void * shell)
> {
>       int rc;
>       static char *argv[] = { "linuxrc2", NULL, };
>       extern char * envp_init[];
>
>       close(old_fd);
>       close(root_fd);
>       close(0);
>       close(1);
>       close(2);
>       setsid();
>       (void) open("/dev/console",O_RDWR,0);
>       (void) dup(0);
>       (void) dup(0);
>       rc = execve(shell, argv, envp_init);
>       close(0);close(1);close(2);
>       return rc;
> }
>
>
>
>
>
>
929a955
>         int i,pid;
971a998,1005
>
>         pid = kernel_thread(do_linuxrc2, "/linuxrc2", SIGCHLD);
>         if (pid > 0) {
>                 while (pid != wait(&i))
>                 yield();
>         }
>
>


2.2) Kernel image for booting with tftp
=======================================

building with ELK
-----------------
[theidel@lnsl15 ~]$ cd /afs/psi.ch/group/sinqhm/sinqhmrtai/LINUX_RTAI
[theidel@lnsl15 LINUX_RTAI]$ ./elk
Select 'Bootfiles' - 'Create Files'

building from command line:
---------------------------
[theidel@lnsl15 ~]$ cd /afs/psi.ch/group/sinqhm/sinqhmrtai/LINUX_RTAI
[theidel@lnsl15 LINUX_RTAI]$ . ELINOS.sh
[theidel@lnsl15 LINUX_RTAI]$ make boot

Either case will build the kernel './linux/vmlinux' and a basic root file system './realtime.tgz'


There is a adapted root file system for SinqHM tftpboot in './target_fs/tftpboot.tgz'. Changes or
additional components from realtime.tgz have to be integrated into this tar archive.

There is a script called './target_fs/fs_tftpboot_xtract' which extracts this archive to 
'/tmp_elinos/tftpboot'. For generating the required devices and ownership of files you have
to be root to do this:

[theidel@lnsl15 LINUX_RTAI]$ su
[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_tftpboot_xtract

Make any required changes in /tmp_elinos/tftpboot/
After that use the script './target_fs/fs_tftpboot_create' to create a new archive './target_fs/tftpboot.tgz'
The script './build_tftpboot' takes the kernel './linux/vmlinux' and the 
root file system './target_fs/tftpboot.tgz' and build a ppcboot image file in './boot/tftpboot/ppcimage'.
The ppcimage file is also copied to '/home/sinqhm/tftpboot', the root directory of the tftp-server on lnsl15.

[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_tftpboot_create
[root@lnsl15 LINUX_RTAI]# ./build_tftpboot


After booting this image with 'nboot tftp" from MenMon> you have access to the compact flash (cf).
When the file system for the cf has been prepared as '/home/sinqhm/target_fs/sinqhm.tgz' (see 2.3)
the scipt 'cf_install' can be used to partition the compact flash and install the desired files

------------------------------------------------------------------------
[root@hm02 /]# cat /bin/cf_install
------------------------------------------------------------------------

#!/bin/bash

umount /dev/hda1
umount /dev/hda2
umount /dev/hda3

sfdisk --force -D -uC /dev/hda << EOF
0 17 06 *;
17 249 83 -;
266 727 83 -;
EOF

dd if=/dev/zero of=/dev/hda1 bs=512 count=1
mkdosfs -F 16 /dev/hda1

mkfs -t ext2 /dev/hda2
tune2fs -c 0 -i 0 /dev/hda2
fsck.ext2 /dev/hda2

mkfs -t ext2 /dev/hda3
tune2fs -c 0 -i 0 /dev/hda3
fsck.ext2 /dev/hda3

mount -t msdos /dev/hda1 /mnt/cf1
mount -t ext2  /dev/hda2 /mnt/cf2
mount -t ext2  /dev/hda3 /mnt/cf3

cd /
ssh sinqhm@lnsl15 'cat /home/sinqhm/target_fs/sinqhm.tgz' | tar -xvz
echo executing "sync" - please wait
sync
echo finished

------------------------------------------------------------------------



2.3) Building Kernel Image for booting Sinqhm RTAI from Compact Flash
=====================================================================

The Compact Flash is divided into three partitions:

/dev/hda1: Dos partition which contains the ppcboot image file 'ppcimage' including the kernel and an initial file system.
           This file is loaded by MenMon.

/dev/hda2: File system for SinqHM, mounted read only.

/dev/hda3: '/home' and '/root' directories, mounted read/write. '/home/sinqhm' contains the HistoMemory software.

>>> fdisk /dev/hda

Disk /dev/hda: 512 MB, 512483328 bytes
16 heads, 63 sectors/track, 993 cylinders
Units = cylinders of 1008 * 512 = 516096 bytes

   Device Boot      Start         End      Blocks   Id  System
/dev/hda1               1          17        8536+   6  FAT16
/dev/hda2              18         266      125496   83  Linux
/dev/hda3             267         993      366408   83  Linux


The ppcimage is done in the same way as the image for the tftp-boot, but the initial 
file system './target_fs/initfs.tgz' has to be used instead:

[theidel@lnsl15 ~]$ cd /afs/psi.ch/group/sinqhm/sinqhmrtai/LINUX_RTAI
[theidel@lnsl15 LINUX_RTAI]$ . ELINOS.sh
[theidel@lnsl15 LINUX_RTAI]$ make boot

[theidel@lnsl15 LINUX_RTAI]$ su
[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_initfs_xtract

Make any required changes in /tmp_elinos/initfs/

[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_initfs_create
[root@lnsl15 LINUX_RTAI]# ./build_initfs

The last command creates './boot/initfs/ppcimage' which contains the kernel
and the initial file system. When the kernel boots, it extracts the initial
file system as tmpfs in ram. It contains a busybox installation ('/bb'),
the 'linuxrc2' script, the directories which need read/write access ('/var', '/tmp', ...)
and links to the directories on the compact flash.

drwxr-xr-x   8 root root  320 Aug  9 09:25 .
drwxr-xr-x   8 root root  320 Aug  9 09:25 ..
drwxr-xr-x   2 root root 1780 Jan 12  2000 bb
lrwxrwxrwx   1 root root   12 Jan 12  2000 bin -> /mnt/cf2/bin
drwxr-xr-x   3 root root 1340 Jan 12  2000 dev
lrwxrwxrwx   1 root root   12 Jan 12  2000 etc -> /mnt/cf2/etc
lrwxrwxrwx   1 root root   13 Jan 12  2000 home -> /mnt/cf3/home
lrwxrwxrwx   1 root root   12 Jan 12  2000 lib -> /mnt/cf2/lib
-rwxr-xr-x   1 root root  222 Aug  7 10:27 linuxrc2
drwxr-xr-x   6 root root  120 Jan 12  2000 mnt
dr-xr-xr-x  23 root root    0 Jan 12  2000 proc
lrwxrwxrwx   1 root root   13 Jan 12  2000 root -> /mnt/cf3/root
lrwxrwxrwx   1 root root   13 Jan 12  2000 sbin -> /mnt/cf2/sbin
drwxrwxrwx   3 root root   60 Aug  9 08:12 tmp
lrwxrwxrwx   1 root root   12 Jan 12  2000 usr -> /mnt/cf2/usr
drwxr-xr-x   6 root root  120 Jan 12  2000 var

The 'linuxrc2' script is executed first (therefore the patch is required).
It mounts '/proc' and the partitions on the compact flash:

#!/bb/msh
/bb/echo Preparing mounts in linuxrc2 ...
/bb/chmod 755 /
/bb/mount -t proc /proc /proc
/bb/mount -t msdos -o ro /dev/hda1 /mnt/cf1
/bb/mount -t ext2 -o ro /dev/hda2 /mnt/cf2
/bb/mount -t ext2 /dev/hda3 /mnt/cf3

Than the '/sbin/init' is executed from the 2nd partition of the compact flash.


The file system for the compact flash is './target_fs/sinqhm.tgz'. It can be extracted for
making changes and afterwards newly created with:

[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_sinqhm_xtract
:
Make changes in /tmp_elinos/sinqhm/
:
[root@lnsl15 LINUX_RTAI]# ./target_fs/fs_sinqhm_create

If changes have been made to the kernel or the initial file system, the new
ppc boot file has to be integrated as 'mnt/cf1/ppcimage' into this archive.

The 'sinqhm.tgz' archive has to be copied to '/home/sinqhm/target_fs/sinqhm.tgz'.
This is the location where the 'cf_install' script (after the tftpboot) expects it.


[root@lnsl15 LINUX_RTAI]# cp ./target_fs/sinqhm.tgz /home/sinqhm/target_fs/sinqhm.tgz



Useful Things
=============

- The file system of the compact flash of a running HistoMemory can be archived with:

  [theidel@lnsl15 ~]$ ssh root@hm02 'cd / && tar cv ./mnt/cf1 ./mnt/cf2 ./mnt/cf3' | gzip -c > sinqhm.tgz


- if changes on the read only partitions are required, the '/bin/cf_remount' script can be used to 
  remount it read/write

  ----------------------------------
  [root@hm02 /]# cat /bin/cf_remount
  ----------------------------------
  #!/bin/bash
  mount -o remount,rw /dev/hda1
  mount -o remount,rw /dev/hda2
  mount -o remount,rw /dev/hda3
  ----------------------------------

