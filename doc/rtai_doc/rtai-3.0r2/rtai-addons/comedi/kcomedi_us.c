
/**
 * This file is the non-inline KComedilib-LXRT userspace lib.
 * It will be compiled into a libkcomedilxrt.a library with which
 * your application can link if it included the rtai_comedi.h file.
 */

#include <rtai_comedi.h>
