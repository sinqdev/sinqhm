\chapter{The RTAI Makefile System}

% ----------------------------------------------------------------------------

\section{Description of the Build System}

\subsection{Motivations}

The RTAI build system is a merge of Linux's 2.5 Kconfig with
autoconf/automake/libtool. The following requirements led to such
mixed system:

\begin{itemize}
\item GUI-based configuration.
\item Cross-compilation support.
\item Compilation of C and C++ kernel modules.
\item Full shared library support (C/C++).
\item C++ compilation of hosts programs.
\item Compilation of GUI (X-based).
\item Script-based configuration (for automatic mode).
\item Initial support for profile-based configurations.
\item Reliable (host) feature detection framework.
\end{itemize}

% ----------------------------------------------------------------------------

\subsection{Dynamics}

The dynamics of the system is as follows:

=> Kconfig/Menuconfig-based configuration (GUI-aided)

  \$ make menuconfig,xconfig,gconfig

         |
         | 1. Produces rtai-core/config/kconfig/.rtai\_config
	      (Kconfig format: CONFIG\_RTAI\_FEATURE=<value> or unset)
         | 2. Runs ./configure --with-kconfig-file=rtai-core/config/kconfig/.rtai\_config
         v

=> Script level configuration (automatic or manual). The configure
script can either be controlled by individual options passed to it, or
fed with a configuration file in Kconfig format.

  Automatic mode (spawned right after kconfig):
	./configure --with-kconfig-file=rtai-core/config/kconfig/.rtai\_config

  Manual mode (from the user CLI):
	./configure --enable-feature-x --with-param-y=n

         |
         | 1. Applies a set of reasonable defaults for unset parameters.
	 | 2. Auto-detects host features as required.
	 | 3. Produces config.h and Makefiles.
         v

=> Compilation

   \$ make

% ----------------------------------------------------------------------------

\subsection{Bootstrapping}

RTAI needs to be configured prior to being built, but the
configuration tool cannot rely on autoconf-generated Makefiles to
build itself. Therefore, the GUI-aided configuration system which
depends on dynamically built programs is bootstrapped by a simple
"makefile" (note the small caps) at the root of the source tree.

The bootstrap makefile traps the targets needed to build and run the
Kconfig tool (make menuconfig|xconfig|gconfig), and invokes the
self-contained rtai-core/config/kconfig/Makefile.kc makefile which in
turn builds and run the configuration GUI as required. Upon return of
the GUI-based configuration session, the bootstrap makefile (re)runs
autoconf's "configure" script as needed to take the configuration
changes into account.

Once the configuration script has been run successfully at least once,
there is no more need to use the bootstrap makefile located in the
source tree, since the top-level autoconf-generated Makefile will be
available in the build tree, and handle the menuconfig, xconfig,
gconfig and other targets appropriately (actually, menuconfig, xconfig
and gconfig are silently passed to the boostrap makefile by the
autoconf-generated one so that we do not duplicate rather complex
rules uselessly).

% ----------------------------------------------------------------------------

\subsection{Dependencies}

The build system is based on:

\begin{itemize}
\item Kconfig as extracted from Linux 2.5.69 development version. The
X-based configuration front-ends either needs Qt (xconfig) or GTK2
(gconfig), and the dialog-based one depends on ncurses.
\item Autoconf >= 2.57
\item Automake >= 1.7.3
\item Libtool >= 1.4.3
\end{itemize}

% ----------------------------------------------------------------------------

\subsection{Configuration Variables}

The following variables are substituted by autoconf into each
automake-controlled Makefile.

\paragraph{@RTAI\_TARGET\_ARCH@} The canonical name of the target 
architecture for which RTAI is built. Currently, "i386" is the 
only supported architecture.

\paragraph{@RTAI\_BUILTIN\_MODLIST@}
A white-space separated list of (otherwise modular) features
to be integrated into the RTAI scheduler(s). Each name found
in this list is canonicalised relatively to the rtai-core/
directory, and can be one of:

\begin{itemize}
\item trace (LTT support)
\item math (In-kernel libm support)
\item ipc/bits (event flags support)
\item ipc/fifos (FIFOS support)
\item ipc/netrpc (NETRPC support)
\item ipc/tbx (Typed mailboxes support)
\item ipc/shmem (Shared memory support)
\item rtmem (Dynamic memory management)
\item tasklets (Tasklets support)
\item usi (User-space interrupts support)
\item watchdog (Task watchdog support)
\end{itemize}

\paragraph{@RTAI\_KMOD\_CFLAGS@} Basic CFLAGS used to compile kernel modules written in C.

\paragraph{@RTAI\_KMOD\_CXXFLAGS@} Basic CFLAGS used to compile kernel modules written in C++.

\paragraph{@RTAI\_USER\_CFLAGS@} Basic CFLAGS used to compile user-space programs.

\paragraph{@RTAI\_FP\_CFLAGS@}
Additional CFLAGS used to compile objects including math
operations. This variable's value depends on
CONFIG\_RTAI\_FPU\_SUPPORT to determine whether we should ask GCC
to use hardware-assisted or software-emulated floating-point
support.

\paragraph{@RTAI\_COMPAT\_CFLAGS@}
When the compatibility mode is activated, this variable
contains additional directives that basically help finding
compatibility headers. Otherwise, this variable is empty.

\paragraph{@RTAI\_MVM\_LDADD@, @RTAI\_MVM\_CFLAGS@ @RTAI\_MVM\_CXXFLAGS@ @RTAI\_MVM\_GCC\_TARBALL@}
Specific flags to compile and link the Xenomai
simulator. These are not used outside of the virtual machine
environment.
