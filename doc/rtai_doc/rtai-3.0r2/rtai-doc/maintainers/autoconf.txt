1. Configuration variables
--------------------------

The following variables are substituted by autoconf into each
automake-controlled Makefile.

@RTAI_TARGET_ARCH@

	The canonical name of the target architecture for which RTAI
	is built. Currently, "i386" is the only supported
	architecture.

@RTAI_BUILTIN_MODLIST@

	A white-space separated list of (otherwise modular) features
	to be integrated into the RTAI scheduler(s). Each name found
	in this list is canonicalised relatively to the rtai-core/
	directory, and can be one of:

	o trace (LTT support)
	o math (In-kernel libm support)
	o ipc/bits (event flags support)
	o ipc/fifos (FIFOS support)
	o ipc/netrpc (NETRPC support)
	o ipc/tbx (Typed mailboxes support)
	o ipc/shmem (Shared memory support)
	o rtmem (Dynamic memory management)
	o tasklets (Tasklets support)
	o usi (User-space interrupts support)
	o watchdog (Task watchdog support)

@RTAI_KMOD_CFLAGS@

	Basic CFLAGS used to compile kernel modules written in C.

@RTAI_KMOD_CXXFLAGS@

	Basic CFLAGS used to compile kernel modules written in C++.

@RTAI_USER_CFLAGS@

	Basic CFLAGS used to compile user-space programs.

@RTAI_FP_CFLAGS@

	Additional CFLAGS used to compile objects including math
	operations. This variable's value depends on
	CONFIG_RTAI_FPU_SUPPORT to determine whether we should ask GCC
	to use hardware-assisted or software-emulated floating-point
	support.

@RTAI_COMPAT_CFLAGS@

	When the compatibility mode is activated, this variable
	contains additional directives that basically help finding
	compatibility headers. Otherwise, this variable is empty.

@RTAI_MVM_LDADD@
@RTAI_MVM_CFLAGS@
@RTAI_MVM_CXXFLAGS@
@RTAI_MVM_GCC_TARBALL@

	Specific flags to compile and link the Xenomai
	simulator. These are not used outside of the virtual machine
	environment.

--
April 23, 2003
Philippe Gerum
<rpm@xenomai.org>
