/** 
 * @file
 * Mailbox functions.
 * @author Paolo Mantegazza
 *
 * @note Copyright (C) 1999-2003 Paolo Mantegazza
 * <mantegazza@aero.polimi.it> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @ingroup mbx
 */

/**
 * @ingroup sched
 * @defgroup mbx Mailbox functions
 *@{*/


#include <rtai_schedcore.h>

MODULE_LICENSE("GPL");

/* +++++++++++++++++++++++++++++ MAIL BOXES ++++++++++++++++++++++++++++++++ */

static inline void mbx_signal(MBX *mbx)
{
	unsigned long flags;
	RT_TASK *task;
	int tosched;

	flags = rt_global_save_flags_and_cli();
	if ((task = mbx->waiting_task)) {
		rem_timed_task(task);
		mbx->waiting_task = NOTHING;
		task->prio_passed_to = NOTHING;
		if (task->state != RT_SCHED_READY && (task->state &= ~(RT_SCHED_MBXSUSP | RT_SCHED_DELAYED)) == RT_SCHED_READY) {
			enq_ready_task(task);
			if (mbx->sndsem.type <= 0) {
				RT_SCHEDULE(task, hard_cpu_id());
				rt_global_restore_flags(flags);
				return;
			}
			tosched = 1;
			goto res;
		}
	}
	tosched = 0;
res:	if (mbx->sndsem.type > 0) {
		DECLARE_RT_CURRENT;
		int sched;
		ASSIGN_RT_CURRENT;
		mbx->owndby = 0;
		if (rt_current->owndres & SEMHLF) {
			--rt_current->owndres;
		}
		if (!rt_current->owndres) {
			sched = renq_current(rt_current, rt_current->base_priority);
		} else if (!(rt_current->owndres & SEMHLF)) {
			int priority;
			sched = renq_current(rt_current, rt_current->base_priority > (priority = ((rt_current->msg_queue.next)->task)->priority) ? priority : rt_current->base_priority);
		} else {
			sched = 0;
		}
		if (rt_current->suspdepth) {
			if (rt_current->suspdepth > 0) {
				rt_current->state |= RT_SCHED_SUSPENDED;
				rem_ready_current(rt_current);
                        	sched = 1;
			} else {
				rt_task_delete(rt_current);
			}
		}
		if (sched) {
			if (tosched) {
				RT_SCHEDULE_BOTH(task, cpuid);
			} else {
				rt_schedule();
			}
		} else if (tosched) {
			RT_SCHEDULE(task, cpuid);
		}
	}
	rt_global_restore_flags(flags);
}

#define RT_MBX_MAGIC 0x3ad46e9b

static inline int mbx_wait(MBX *mbx, int *fravbs, RT_TASK *rt_current)
{
	unsigned long flags;

	flags = rt_global_save_flags_and_cli();
	if (!(*fravbs)) {
		unsigned long schedmap;
		if (mbx->sndsem.type > 0) {
			schedmap = pass_prio(mbx->owndby, rt_current);
		} else {
			schedmap = 0;
		}
		rt_current->state |= RT_SCHED_MBXSUSP;
		rem_ready_current(rt_current);
		mbx->waiting_task = rt_current;
		RT_SCHEDULE_MAP_BOTH(schedmap);
		if (mbx->waiting_task == rt_current || mbx->magic != RT_MBX_MAGIC) {
			rt_current->prio_passed_to = NOTHING;
			rt_global_restore_flags(flags);
			return -1;
		}
	}
	if (mbx->sndsem.type > 0) {
		(mbx->owndby = rt_current)->owndres++;
	}
	rt_global_restore_flags(flags);
	return 0;
}

static inline int mbx_wait_until(MBX *mbx, int *fravbs, RTIME time, RT_TASK *rt_current)
{
	unsigned long flags;

	flags = rt_global_save_flags_and_cli();
	if (!(*fravbs)) {
		mbx->waiting_task = rt_current;
		if ((rt_current->resume_time = time) > rt_smp_time_h[hard_cpu_id()]) {
			unsigned long schedmap;
			if (mbx->sndsem.type > 0) {
				schedmap = pass_prio(mbx->owndby, rt_current);
			} else {
				schedmap = 0;
			}
			rt_current->state |= (RT_SCHED_MBXSUSP | RT_SCHED_DELAYED);
			rem_ready_current(rt_current);
			enq_timed_task(rt_current);
			RT_SCHEDULE_MAP_BOTH(schedmap);
		}
		if (mbx->magic != RT_MBX_MAGIC) {
			rt_current->prio_passed_to = NOTHING;
			rt_global_restore_flags(flags);
			return -1;
		}
		if (mbx->waiting_task == rt_current) {
			mbx->waiting_task = NOTHING;
			rt_current->prio_passed_to = NOTHING;
			rt_global_restore_flags(flags);
			return -1;
		}
	}
	if (mbx->sndsem.type > 0) {
		(mbx->owndby = rt_current)->owndres++;
	}
	rt_global_restore_flags(flags);
	return 0;
}

#define MOD_SIZE(indx) ((indx) < mbx->size ? (indx) : (indx) - mbx->size)

static inline int mbxput(MBX *mbx, char **msg, int msg_size)
{
	unsigned long flags;
	int tocpy;

	while (msg_size > 0 && mbx->frbs) {
		if ((tocpy = mbx->size - mbx->lbyte) > msg_size) {
			tocpy = msg_size;
		}
		if (tocpy > mbx->frbs) {
			tocpy = mbx->frbs;
		}
		memcpy(mbx->bufadr + mbx->lbyte, *msg, tocpy);
		flags = rt_spin_lock_irqsave(&(mbx->lock));
		mbx->frbs -= tocpy;
		mbx->avbs += tocpy;
		rt_spin_unlock_irqrestore(flags, &(mbx->lock));
		msg_size -= tocpy;
		*msg     += tocpy;
		mbx->lbyte = MOD_SIZE(mbx->lbyte + tocpy);
	}
	return msg_size;
}

static inline int mbxovrwrput(MBX *mbx, char **msg, int msg_size)
{
	unsigned long flags;
	int tocpy,n;

	if ((n = msg_size - mbx->size) > 0) {
		*msg += n;
		msg_size -= n;
	}		
	while (msg_size > 0) {
		if (mbx->frbs) {	
			if ((tocpy = mbx->size - mbx->lbyte) > msg_size) {
				tocpy = msg_size;
			}
			if (tocpy > mbx->frbs) {
				tocpy = mbx->frbs;
			}
			memcpy(mbx->bufadr + mbx->lbyte, *msg, tocpy);
	        	flags = rt_spin_lock_irqsave(&(mbx->lock));
			mbx->frbs -= tocpy;
			mbx->avbs += tocpy;
        		rt_spin_unlock_irqrestore(flags, &(mbx->lock));
			msg_size -= tocpy;
			*msg     += tocpy;
			mbx->lbyte = MOD_SIZE(mbx->lbyte + tocpy);
		}	
		if (msg_size) {
			while ((n = msg_size - mbx->frbs) > 0) {
				if ((tocpy = mbx->size - mbx->fbyte) > n) {
					tocpy = n;
				}
				if (tocpy > mbx->avbs) {
					tocpy = mbx->avbs;
				}
	        		flags = rt_spin_lock_irqsave(&(mbx->lock));
				mbx->frbs  += tocpy;
				mbx->avbs  -= tocpy;
        			rt_spin_unlock_irqrestore(flags, &(mbx->lock));
				mbx->fbyte = MOD_SIZE(mbx->fbyte + tocpy);
			}
		}		
	}
	return 0;
}

static inline int mbxget(MBX *mbx, char **msg, int msg_size)
{
	unsigned long flags;
	int tocpy;

	while (msg_size > 0 && mbx->avbs) {
		if ((tocpy = mbx->size - mbx->fbyte) > msg_size) {
			tocpy = msg_size;
		}
		if (tocpy > mbx->avbs) {
			tocpy = mbx->avbs;
		}
		memcpy(*msg, mbx->bufadr + mbx->fbyte, tocpy);
		flags = rt_spin_lock_irqsave(&(mbx->lock));
		mbx->frbs  += tocpy;
		mbx->avbs  -= tocpy;
		rt_spin_unlock_irqrestore(flags, &(mbx->lock));
		msg_size -= tocpy;
		*msg     += tocpy;
		mbx->fbyte = MOD_SIZE(mbx->fbyte + tocpy);
	}
	return msg_size;
}

static inline int mbxevdrp(MBX *mbx, char **msg, int msg_size)
{
	int tocpy, fbyte, avbs;

	fbyte = mbx->fbyte;
	avbs  = mbx->avbs;
	while (msg_size > 0 && avbs) {
		if ((tocpy = mbx->size - fbyte) > msg_size) {
			tocpy = msg_size;
		}
		if (tocpy > avbs) {
			tocpy = avbs;
		}
		memcpy(*msg, mbx->bufadr + fbyte, tocpy);
		avbs     -= tocpy;
		msg_size -= tocpy;
		*msg     += tocpy;
		fbyte = MOD_SIZE(fbyte + tocpy);
	}
	return msg_size;
}

int rt_mbx_evdrp(MBX *mbx, void *msg, int msg_size)
{
	return mbxevdrp(mbx, (char **)(&msg), msg_size);
}

#define CHK_MBX_MAGIC { if (mbx->magic != RT_MBX_MAGIC) { return -EINVAL; } }

int rt_typed_mbx_init(MBX *mbx, int size, int type)
{
	if (!(mbx->bufadr = sched_malloc(size))) { 
		return -ENOMEM;
	}
	TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_INIT, mbx, size, 0);
	rt_typed_sem_init(&(mbx->sndsem), 1, type & 3 ? type : BIN_SEM | type);
	rt_typed_sem_init(&(mbx->rcvsem), 1, type & 3 ? type : BIN_SEM | type);
	mbx->magic = RT_MBX_MAGIC;
	mbx->size = mbx->frbs = size;
	mbx->waiting_task = mbx->owndby = 0;
	mbx->fbyte = mbx->lbyte = mbx->avbs = 0;
        spin_lock_init(&(mbx->lock));
	return 0;
}


/**
 * @brief Initializes a mailbox.
 *
 * rt_mbx_init initializes a mailbox of size @e size. @e mbx must
 * point to a user allocated MBX structure.
 * Using mailboxes is a flexible method for inter task
 * communications. Tasks are allowed to send arbitrarily sized
 * messages by using any mailbox buffer size. There is even no need to
 * use a buffer sized at least as the largest message you envisage,
 * even if efficiency is likely to suffer from such a
 * decision. However if you expect a message larger than the average
 * message size very rarely you can use a smaller buffer without much
 * loss of efficiency. In such a way you can set up your own mailbox
 * usage protocol, e.g. using fix sized messages with a buffer that is
 * an integer multiple of such a size guarantees maximum efficiency by
 * having each message sent/received atomically to/from the
 * mailbox. Multiple senders and receivers are allowed and each will
 * get the service it requires in turn, according to its priority. 
 * Thus mailboxes provide a flexible mechanism to allow you to freely
 * implement your own policy.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param size corresponds to the size of the mailbox.
 *
 * @return On success 0 is returned. On failure, a special value is
 * returned as indicated below:
 * - @b EINVAL: Space could not be allocated for the mailbox buffer.
 */
int rt_mbx_init(MBX *mbx, int size)
{
	return rt_typed_mbx_init(mbx, size, PRIO_Q);
}


/**
 *
 * @brief Deletes a mailbox.
 * 
 * rt_mbx_delete removes a mailbox previously created with rt_mbox_init().
 *
 * @param mbx Pointer to the structure used in the corresponding call
 * to rt_mbox_init.
 *
 * @return 0 is returned on success. On failure, a negative value is
 * returned as described below:
 * - @b EINVAL: @e mbx points to an invalid mailbox.
 * - @b EFAULT: mailbox data were found in an invalid state.
 */
int rt_mbx_delete(MBX *mbx)
{
	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_DELETE, mbx, 0, 0);

	mbx->magic = 0;
	if (rt_sem_delete(&mbx->sndsem) || rt_sem_delete(&mbx->rcvsem)) {
		return -EFAULT;
	}
	while (mbx->waiting_task) {
		mbx_signal(mbx);
	}
	sched_free(mbx->bufadr); 
	return 0;
}


/**
 * @brief Sends a message unconditionally.
 *
 * rt_mbx_send sends a message @e msg of @e msg_size bytes to the
 * mailbox @e mbx. The caller will be blocked until the whole message
 * is copied into the mailbox or an error occurs. Even if the message
 * can be sent in a single shot, the sending task can be blocked if
 * there is a task of higher priority waiting to receive from the
 * mailbox.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg corresponds to the message to be sent.
 *
 * @param msg_size is the size of the message.
 *
 * @return On success, the number of unsent bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: @e mbx points to not a valid mailbox.
 */
int rt_mbx_send(MBX *mbx, void *msg, int msg_size)
{
	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_SEND, mbx, msg_size, 0);

	if (rt_sem_wait(&mbx->sndsem) > 1) {
		return msg_size;
	}
	while (msg_size) {
		if (mbx_wait(mbx, &mbx->frbs, RT_CURRENT)) {
			rt_sem_signal(&mbx->sndsem);
			return msg_size;
		}
		msg_size = mbxput(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
	}
	rt_sem_signal(&mbx->sndsem);
	return 0;
}


/**
 * @brief Sends as many bytes as possible without blocking the calling task.
 *
 * rt_mbx_send_wp atomically sends as many bytes of message @e msg as
 * possible to the mailbox @e mbx then returns immediately.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg corresponds to the message to be sent.
 * 
 * @param msg_size is the size of the message.
 *
 * @return On success, the number of unsent bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: @e mbx points to an invalid mailbox.
 */
int rt_mbx_send_wp(MBX *mbx, void *msg, int msg_size)
{
	unsigned long flags;

	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_SEND_WP, mbx, msg_size, 0);

	flags = rt_global_save_flags_and_cli();
	if (mbx->sndsem.count && mbx->frbs) {
		mbx->sndsem.count = 0;
		if (mbx->sndsem.type > 0) {
			(mbx->sndsem.owndby = mbx->owndby = RT_CURRENT)->owndres += 2;
		}
		rt_global_restore_flags(flags);
		msg_size = mbxput(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
		rt_sem_signal(&mbx->sndsem);
	} else {
		rt_global_restore_flags(flags);
	}
	return msg_size;
}


/**
 * @brief Sends a message, only if the whole message can be passed
 * without blocking the calling task.
 *
 * rt_mbx_send_if tries to atomically send the message @e msg of @e
 * msg_size bytes to the mailbox @e mbx. It returns immediately and
 * the caller is never blocked.
 *
 * @return On success, the number of unsent bytes (0 or @e msg_size)
 * is returned. On failure a negative value is returned as described
 * below:
 * - @b EINVAL: @e mbx points to an invalid mailbox.
 */
int rt_mbx_send_if(MBX *mbx, void *msg, int msg_size)
{
	unsigned long flags;

	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_SEND_IF, mbx, msg_size, 0);

	flags = rt_global_save_flags_and_cli();
	if (mbx->sndsem.count && msg_size <= mbx->frbs) {
		mbx->sndsem.count = 0;
		if (mbx->sndsem.type > 0) {
			(mbx->sndsem.owndby = mbx->owndby = RT_CURRENT)->owndres += 2;
		}
		rt_global_restore_flags(flags);
		mbxput(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
		rt_sem_signal(&mbx->sndsem);
		return 0;
	}
	rt_global_restore_flags(flags);
	return msg_size;
}


/**
 * @brief Sends a message with timeout.
 *
 * rt_mbx_send_until sends a message @e msg of @e msg_size bytes to
 * the mailbox @e mbx. The caller will be blocked until all bytes of
 * message is enqueued, timeout expires or an error occurs.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg is the message to be sent.
 *
 * @param msg_size corresponds to the size of the message.
 *
 * @param time is an absolute value for the timeout.
 *
 * @return On success, the number of unsent bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 *
 * See also: notes under @ref rt_mbx_send_timed().
 */
int rt_mbx_send_until(MBX *mbx, void *msg, int msg_size, RTIME time)
{
	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_SEND_UNTIL, mbx, msg_size, time);

	if (rt_sem_wait_until(&mbx->sndsem, time) > 1) {
		return msg_size;
	}
	while (msg_size) {
		if (mbx_wait_until(mbx, &mbx->frbs, time, RT_CURRENT)) {
			rt_sem_signal(&mbx->sndsem);
			return msg_size;
		}
		msg_size = mbxput(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
	}
	rt_sem_signal(&mbx->sndsem);
	return 0;
}


/**
 * @brief Sends a message with timeout.
 *
 * rt_mbx_send_timed send a message @e msg of @e msg_size bytes to the
 * mailbox @e mbx. The caller will be blocked until all bytes of message 
 * is enqueued, timeout expires or an error occurs.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg is the message to be sent.
 *
 * @param msg_size corresponds to the size of the message.
 *
 * @param delay is the timeout value relative to the current time.
 *
 * @return On success, the number of unsent bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 *
 * See also: notes under @ref rt_mbx_send_until().
 */
int rt_mbx_send_timed(MBX *mbx, void *msg, int msg_size, RTIME delay)
{
	return rt_mbx_send_until(mbx, msg, msg_size, get_time() + delay);
}


/**
 * @brief Receives a message unconditionally.
 *
 * rt_mbx_receive receives a message of @e msg_size bytes from the
 * mailbox @e mbx. The caller will be blocked until all bytes of the
 * message arrive or an error occurs.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg points to a buffer provided by the caller.
 *
 * @param msg_size corresponds to the size of the message received.
 *
 * @return On success, the number of received bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 */
int rt_mbx_receive(MBX *mbx, void *msg, int msg_size)
{
	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_RECV, mbx, msg_size, 0);

	if (rt_sem_wait(&mbx->rcvsem) > 1) {
		return msg_size;
	}
	while (msg_size) {
		if (mbx_wait(mbx, &mbx->avbs, RT_CURRENT)) {
			rt_sem_signal(&mbx->rcvsem);
			return msg_size;
		}
		msg_size = mbxget(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
	}
	rt_sem_signal(&mbx->rcvsem);
	return 0;
}


/**
 * @brief Receives bytes as many as possible, without blocking the
 * calling task.
 *
 * rt_mbx_receive_wp receives at most @e msg_size of bytes of message
 * from mailbox mbx then returns immediately.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg points to a buffer provided by the caller.
 *
 * @param msg_size corresponds to the size of the message received.
 *
 * @return On success, the number of received bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to not a valid mailbox.
 */
int rt_mbx_receive_wp(MBX *mbx, void *msg, int msg_size)
{
	unsigned long flags;

	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_RECV_WP, mbx, msg_size, 0);

	flags = rt_global_save_flags_and_cli();
	if (mbx->rcvsem.count && mbx->avbs) {
		mbx->rcvsem.count = 0;
		if (mbx->rcvsem.type > 0) {
			(mbx->rcvsem.owndby = mbx->owndby = RT_CURRENT)->owndres += 2;
		}
		rt_global_restore_flags(flags);
		msg_size = mbxget(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
		rt_sem_signal(&mbx->rcvsem);
	} else {
		rt_global_restore_flags(flags);
	}
	return msg_size;
}


/**
 * @brief Receives a message only if the whole message can be passed
 * without blocking the calling task.
 *
 * rt_mbx_receive_if receives a message from the mailbox @e mbx if the
 * whole message of @e msg_size bytes is available immediately.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg points to a buffer provided by the caller.
 *
 * @param msg_size corresponds to the size of the message received.
 *
 * @return On success, the number of received bytes (0 or @e msg_size)
 * is returned. On failure a negative value is returned as described
 * below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 */
int rt_mbx_receive_if(MBX *mbx, void *msg, int msg_size)
{
	unsigned long flags;

	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_RECV_IF, mbx, msg_size, 0);

	flags = rt_global_save_flags_and_cli();
	if (mbx->rcvsem.count && msg_size <= mbx->avbs) {
		mbx->rcvsem.count = 0;
		if (mbx->rcvsem.type > 0) {
			(mbx->rcvsem.owndby = mbx->owndby = RT_CURRENT)->owndres += 2;
		}
		rt_global_restore_flags(flags);
		mbxget(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
		rt_sem_signal(&mbx->rcvsem);
		return 0;
	}
	rt_global_restore_flags(flags);
	return msg_size;
}


/**
 * @brief Receives a message with timeout.
 *
 * rt_mbx_receive_until receives a message of @e msg_size bytes from
 * the mailbox @e mbx. The caller will be blocked until all bytes of
 * the message arrive, timeout expires or an error occurs.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg points to a buffer provided by the caller.
 *
 * @param msg_size corresponds to the size of the message received.
 *
 * @param time is an absolute value of the timeout.
 *
 * @return On success, the number of received bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 *
 * See also: notes under rt_mbx_received_timed().
 */
int rt_mbx_receive_until(MBX *mbx, void *msg, int msg_size, RTIME time)
{
	CHK_MBX_MAGIC;

        TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_RECV_UNTIL, mbx, msg_size, time);

	if (rt_sem_wait_until(&mbx->rcvsem, time) > 1) {
		return msg_size;
	}
	while (msg_size) {
		if (mbx_wait_until(mbx, &mbx->avbs, time, RT_CURRENT)) {
			rt_sem_signal(&mbx->rcvsem);
			return msg_size;
		}
		msg_size = mbxget(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
	}
	rt_sem_signal(&mbx->rcvsem);
	return 0;
}


/**
 * @brief Receives a message with timeout.
 *
 * rt_mbx_receive_timed receives a message of @e msg_size bytes from
 * the mailbox @e mbx. The caller will be blocked until all bytes of 
 * the message arrive, timeout expires or an error occurs.
 *
 * @param mbx is a pointer to a user allocated mailbox structure.
 *
 * @param msg points to a buffer provided by the caller.
 *
 * @param msg_size corresponds to the size of the message received.
 *
 * @param delay is the timeout value relative to the current time.
 *
 * @return On success, the number of received bytes is returned. On
 * failure a negative value is returned as described below:
 * - @b EINVAL: mbx points to an invalid mailbox.
 *
 * See also: notes under rt_mbx_received_until().
 */
int rt_mbx_receive_timed(MBX *mbx, void *msg, int msg_size, RTIME delay)
{
	return rt_mbx_receive_until(mbx, msg, msg_size, get_time() + delay);
}


int rt_mbx_ovrwr_send(MBX *mbx, void *msg, int msg_size)
{
	unsigned long flags;

	CHK_MBX_MAGIC;

	TRACE_RTAI_MBX(TRACE_RTAI_EV_MBX_OVRWR_SEND, mbx, msg_size, 0);

	flags = rt_global_save_flags_and_cli();
	if (mbx->sndsem.count) {
		mbx->sndsem.count = 0;
		if (mbx->sndsem.type > 0) {
			(mbx->sndsem.owndby = mbx->owndby = RT_CURRENT)->owndres += 2;
		}
		rt_global_restore_flags(flags);
		msg_size = mbxovrwrput(mbx, (char **)(&msg), msg_size);
		mbx_signal(mbx);
		rt_sem_signal(&mbx->sndsem);
	} else {
		rt_global_restore_flags(flags);
	}
	return msg_size;
}

/* ++++++++++++++++++++++++++ NAMED MAIL BOXES ++++++++++++++++++++++++++++++ */

#include <rtai_registry.h>

MBX *_rt_typed_named_mbx_init(unsigned long mbx_name, int size, int qtype)
{
	MBX *mbx;

	if ((mbx = rt_get_adr_cnt(mbx_name))) {
		return mbx;
	}
	if ((mbx = rt_malloc(sizeof(MBX))) && !rt_typed_mbx_init(mbx, size, qtype)) {
		if (rt_register(mbx_name, mbx, IS_MBX, 0)) {
			return mbx;
		}
		rt_mbx_delete(mbx);
	}
	rt_free(mbx);
	return (MBX *)0;
}

int rt_named_mbx_delete(MBX *mbx)
{
	if (!rt_mbx_delete(mbx)) {
		rt_free(mbx);
	}
	return rt_drg_on_adr_cnt(mbx) >= 0;
}

/* +++++++++++++++++++++++++ MAIL BOXES ENTRIES +++++++++++++++++++++++++++++ */

struct rt_native_fun_entry rt_mbx_entries[] = {

	{ { 0, rt_typed_mbx_init }, 	       TYPED_MBX_INIT },
	{ { 0, rt_mbx_delete }, 	       MBX_DELETE },
	{ { 0, _rt_typed_named_mbx_init },     NAMED_MBX_INIT },
	{ { 0, rt_named_mbx_delete },          NAMED_MBX_DELETE },
	{ { UR1(2, 3), rt_mbx_send }, 	       MBX_SEND },
	{ { UR1(2, 3), rt_mbx_send_wp },       MBX_SEND_WP },
	{ { UR1(2, 3), rt_mbx_send_if },       MBX_SEND_IF },
	{ { UR1(2, 3), rt_mbx_send_until },    MBX_SEND_UNTIL },
	{ { UR1(2, 3), rt_mbx_send_timed },    MBX_SEND_TIMED },
	{ { UW1(2, 3), rt_mbx_receive },       MBX_RECEIVE },
	{ { UW1(2, 3), rt_mbx_receive_wp },    MBX_RECEIVE_WP },
	{ { UW1(2, 3), rt_mbx_receive_if },    MBX_RECEIVE_IF },
	{ { UW1(2, 3), rt_mbx_receive_until }, MBX_RECEIVE_UNTIL },
	{ { UW1(2, 3), rt_mbx_receive_timed }, MBX_RECEIVE_TIMED },
	{ { UR1(2, 3), rt_mbx_ovrwr_send },    MBX_OVRWR_SEND },
        { { UW1(2, 3), rt_mbx_evdrp },         MBX_EVDRP },
	{ { 0, 0 },  		      	       000 }
};

extern int set_rt_fun_entries(struct rt_native_fun_entry *entry);
extern void reset_rt_fun_entries(struct rt_native_fun_entry *entry);

int MBX_INIT_MODULE (void)
{
	return set_rt_fun_entries(rt_mbx_entries);
}

void MBX_CLEANUP_MODULE (void)
{
	reset_rt_fun_entries(rt_mbx_entries);
}

/* +++++++++++++++++++++++++++ END MAIL BOXES +++++++++++++++++++++++++++++++ */

/*@}*/
