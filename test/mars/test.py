import sys
sys.path.append('../python')
from histomem import *



def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

#######################################
#
# main
#
#######################################


tg = testgen('lnse14.psi.ch',5555)


if 1:
  for xpos in xrange(512):
    for ypos in xrange(512):
        tg.sans2(xpos,ypos)



if 0:    
 for time in (0,   40000,   80000,   120000,  160000,  200000, 240000):
    tg.amor_ed(1,time,0x6)  # Einzeldetektor 1
    tg.amor_ed(2,time,0x6)  # Einzeldetektor 2
    tg.amor_ed(2,time,0x6)  # Einzeldetektor 2
    tg.amor_ed(3,time,0x6)  # Einzeldetektor 3
    tg.amor_ed(3,time,0x6)  # Einzeldetektor 3
    tg.amor_ed(3,time,0x6)  # Einzeldetektor 3

    
# tg.value(0xffffffff)

#hm = histomem('hm03.psi.ch','spy','007')

# hm.stop()
# hm.configure('conf.xml')

# hm.start()
#sleep(2.0)
#hm.stop()

#data = hm.get()
#write_data(data,'data.txt')

#print data
#print data[0:100]
