#!/usr/bin/env python
from __future__ import division
import sys
sys.path.append('../python')
from histomem import *
import array
import sys

def printf(format,*args): sys.stdout.write(format % args)


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
hm = histomem('lnse14.psi.ch','spy','007')
D = hm.get(bank=2)
# write_data(D,'data.dat')


t = D[0]
printf ("Total                  : %9d\n",t)

v = D[1]
p = 100.0 * v / t
printf ("b0 valid               : %9d (%6.2f %%)\n",v,p)

v = D[2]
p = 100.0 * v / t
printf ("b1 valid               : %9d (%6.2f %%)\n",v,p)

v = D[3]
p = 100.0 * v / t
printf ("b2 valid               : %9d (%6.2f %%)\n",v,p)

v = D[4]
p = 100.0 * v / t
printf ("kv_valid   & kh_invalid: %9d (%6.2f %%)\n",v,p)

v = D[5]
p = 100.0 * v / t
printf ("kv_invalid & kh_valid  : %9d (%6.2f %%)\n",v,p)

v = D[6]
p = 100.0 * v / t
printf ("an_invalid             : %9d (%6.2f %%)\n",v,p)

v = D[7]
p = 100.0 * v / t
printf ("kv_invalid             : %9d (%6.2f %%)\n",v,p)

v = D[8]
p = 100.0 * v / t
printf ("kh_invalid             : %9d (%6.2f %%)\n",v,p)


#     val=D[x*S+y]

#        UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[0], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        // valid
#        if (b0_valid) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[1], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (b1_valid) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[2], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (b2_valid) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[3], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        // Error Counts
#        if ( kv_valid && !kh_valid) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[4], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (!kv_valid &&  kh_valid) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[5], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (!an_valid ) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[6], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (!kv_valid ) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[7], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);
#        if (!kh_valid ) UINT32_INC_CEIL_CNT_OVL(bank2_HistoDataPtr[8], shm_cfg_ptr[CFG_FIL_BIN_OVERFLOWS]);

