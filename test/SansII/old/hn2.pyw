#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
from numpy import *
from time import *
from matplotlib.widgets import RectangleSelector
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons
from matplotlib.widgets import Slider
from nexus import *


DIMX = DIMY = 512



def get_histo():
  if 0:
    global Z0, Z1, Z2
    Z0 = hm.get(bank=0)
    Z0.shape = DIMY, DIMX

    Z1 = hm.get(bank=1)
    Z1.shape = DIMY, DIMX

    Z2 = hm.get(bank=2)
    Z2.shape = DIMY, DIMX

  else:
#     Z0, Z1, Z2 = sans2_sum_b0_b1_b2('30..44') 
#    Z0, Z1, Z2 = sans2_sum_b0_b1_b2('63..66') 
    Z0, Z1, Z2 = sans2_sum_b0_b1_b2('79..95') 
#    Z0, Z1, Z2 = sans2_sum_b0_b1_b2('79..106') 
    Z0.shape = DIMY, DIMX
    Z1.shape = DIMY, DIMX
    Z2.shape = DIMY, DIMX

def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i


def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)


def sans2_sum(rng, bank='AVG', sum=0):
    PATH = 'O:\\psi.ch\\project\\sinqdata\\2007\\sans2\\000\\'
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        print "sans22007n%06d.hdf"%i
        sum += getdata(PATH+"sans22007n%06d.hdf"%i,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum


def getdata_b0_b1_b2(filename):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_front')
    status, b0 = NXgetdata(nf)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_back')
    status, b1 = NXgetdata(nf)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_anode')
    status, b2 = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(b0),array(b1),array(b2)

def sans2_sum_b0_b1_b2(rng):
    PATH = 'O:\\psi.ch\\project\\sinqdata\\2007\\sans2\\000\\'
    s0 = s1 = s2 = 0
    for i in srange(rng):
        print "sans22007n%06d.hdf"%i
        b0,b1,b2 = getdata_b0_b1_b2(PATH+"sans22007n%06d.hdf"%i)
        s0 += b0
        s1 += b1
        s2 += b2
    return s0,s1,s2

def line_select_callback(event1, event2):
    'event1 and event2 are the press and release events'
    x1, y1 = int(round(event1.xdata)), int(round(event1.ydata))
    x2, y2 = int(round(event2.xdata)), int(round(event2.ydata))

    x1, x2 = min(x1,x2), max(x1,x2)+1
    y1, y2 = min(y1,y2), max(y1,y2)+1

    lx = x2-x1
    ly = y2-y1

    #print "(%3.2f, %3.2f) --> (%3.2f, %3.2f)"%(x1,y1,x2,y2)
    #print " The button you used were: ",event1.button, event2.button



#figsrc = figure()
#figzoom = figure()

    range = Z[y1:y2,x1:x2]
    sx = range.sum(0)
    sy = range.sum(1)
#    print sx
#    print sy


#    if event1.button == 1:    # left mouse button 
    if True:            # always


        line_x.set_data(arange(x1,x2),sx)          # update the data
        ax_x.set_xlim(x1,max(x2-1,x1+1))
        ax_x.set_ylim(min(sx),max(sx))

        line_y.set_data(sy,arange(y1,y2))          # update the data
        ax_y.set_ylim(y1,max(y2-1,y1+1))
        ax_y.set_xlim(min(sy),max(sy))

        mainfig.canvas.draw()                         # redraw the canvas

    if event1.button > 1:         # middle or right mouse button
        newfig = figure()
#        newax_x = newfig.add_subplot(111, xlim=(0,1), ylim=(0,1), autoscale_on=False)
        newax_x = newfig.add_subplot(212)
        newax_x.plot(arange(x1,x2),sx)
        newax_x.set_xlim(x1,max(x2-1,x1+1))
        newax_x.set_ylim(min(sx),max(sx))
        xlabel('X Axis (Cathode) --->')
        ylabel('Summed Counts --->')
        
        newax_y = newfig.add_subplot(211)
        newax_y.plot(arange(y1,y2),sy)          # update the data
        newax_y.set_xlim(y1,max(y2-1,y1+1))
        newax_y.set_ylim(min(sy),max(sy))
        xlabel('Y Axis (Anode) --->')
        ylabel('Summed Counts --->')
        
        
        newfig.canvas.draw()
        
        
        show()

hm = histomem('lnse14.psi.ch','spy','007')

ioff()    # turn interactive mode off
#colorbar()
# current_ax=subplot(221)                    # make a new plotingrange







get_histo()
Z = Z0

border = 60
Zmax=(Z[border:-border,border:-border]).max()
Zmax = max(Zmax,1)
print Zmax


mainfig = figure()
current_ax = axes([0.05, 0.3, 0.6, 0.62], axisbg='w')



im = imshow(Z, cmap=cm.jet,origin='lower',vmin=0, vmax=Zmax,aspect = 'auto')
print dir(im)
#im = imshow(Z, cmap=cm.jet,origin='lower')

#range = Z[110:410,140:370]
#im = imshow(range, cmap=cm.jet,origin='lower')

im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

#lineprops = dict(color='white', linestyle='-',linewidth = 2, alpha=0.5)
rectprops = dict(facecolor='yellow', edgecolor = 'white', alpha=0.5, fill=True)
LS = RectangleSelector(current_ax, line_select_callback,
                      drawtype='box',useblit=False,rectprops=rectprops)
                      

sx = Z.sum(0)
sy = Z.sum(1)


ax_x = mainfig.add_axes([0.05, 0.05, 0.6, 0.2], axisbg='w')
line_x, = ax_x.plot(arange(DIMX),sx)
ax_x.set_xlim(0,DIMX)
ax_x.set_ylim(min(sx),max(sx))

ax_y = mainfig.add_axes([0.7, 0.3, 0.28, 0.68], axisbg='w')
line_y, = ax_y.plot(sy,arange(DIMY))
ax_y.set_ylim(0,DIMY)
ax_y.set_xlim(min(sy),max(sy))



class ButtonCallback:
    def __init__(self):
        self.cn = 0
        
    def update(self, event):
        global Z
        get_histo()
        Z = Z0
        im.set_data(Z)
        mainfig.canvas.draw()                         # redraw the canvas

    def scale(self, event):
        global Z
        border = 1
        Zmax=(Z[border:-border,border:-border]).max()
        Zmax = min(max(Zmax,1),2**16-1)
        norm = Normalize(vmin=0, vmax=Zmax)
        im.set_norm(norm)
#        im.update()
#        im.set_data(Z)
        mainfig.canvas.draw()                         # redraw the canvas

    def color(self, event):
        self.cn += 1
        if self.cn > len(cm.cmapnames): self.cn = 0
        im.set_cmap(cm.get_cmap(cm.cmapnames[self.cn]))
#        global Z
#        get_histo()
#        Z = Z0
#        im.set_data(Z)
        mainfig.canvas.draw()                         # redraw the canvas


if 1:
    bcb    = ButtonCallback()
    ax_update = mainfig.add_axes([0.85, 0.05, 0.1, 0.05])
    b_update  = Button(ax_update, 'Update')
    b_update.on_clicked(bcb.update) 

    ax_scale = mainfig.add_axes([0.85, 0.12, 0.1, 0.05])
    b_scale  = Button(ax_scale, 'Scale')
    b_scale.on_clicked(bcb.scale) 

    ax_color = mainfig.add_axes([0.85, 0.19, 0.1, 0.05])
    b_color  = Button(ax_color, 'Color')
    b_color.on_clicked(bcb.color) 


axcolor = 'lightgoldenrodyellow'
rax = mainfig.add_axes([0.7, 0.05, 0.10, 0.10], axisbg=axcolor)
radio = RadioButtons(rax, ('AVG', 'KV', 'KH'))
def bankfunc(label):
    global Z
    banlkdict = {'AVG':Z0, 'KV':Z1, 'KH':Z2}
    Z = banlkdict[label]
    im.set_data(Z)
    mainfig.canvas.draw()                         # redraw the canvas

radio.on_clicked(bankfunc)




axcolor = 'lightgoldenrodyellow'
axamp  = axes([0.05, 0.95, 0.6, 0.03], axisbg=axcolor)

samp = Slider(axamp, '', 0.01, 200.0, valinit=100, valfmt='%1.0f')
def update(val):
    Zmax=Z.max()
    Zmax = max(Zmax,1)
    amp = min(int(Zmax * samp.val / 100.0),2**16-1)
    
    norm = Normalize(vmin=0, vmax=amp)
    im.set_norm(norm)
    mainfig.canvas.draw()                         # redraw the canvas
samp.on_changed(update)


show()
