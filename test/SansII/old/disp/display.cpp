#include "display.h"
#include <qtimer.h>
#include <qlayout.h>
#include <qptrlist.h>

extern "C"
{
  #include "http.h"
}


MyLCD::MyLCD(QWidget *parent, char *label_text, QString tagstr, QGridLayout *grid, int gridrow)
{
        number= new QLCDNumber(parent);
	number->setNumDigits (8);
        number->setSegmentStyle(QLCDNumber::Filled);
//        number->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
//        number->setFrameStyle(QFrame::NoFrame);

	tag = tagstr;

	label = new QLabel(label_text,parent);
        label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        label->setFont(QFont("Times", 18, QFont::Bold))	;
        label->setMinimumWidth(100);
        label->setFrameStyle (QFrame::Panel);


        grid->addWidget(label, gridrow, 0);
        grid->addWidget(number, gridrow, 1);

        grid->setColStretch(1,10);
//      gridLayout->setColumnStretch(1, 10);

//      gridLayout->setColumnMinimumWidth(0,100);
//      setLayout(grid);

 }

void MyLCD::display(QString text)
{
      number->display(text);
}




display::display(QWidget *parent) : QWidget(parent)
{
//	setSegmentStyle(Filled);
	QTimer *timer = new QTimer(this);
        QGridLayout* grid = new QGridLayout( this, 3, 2, 0 );

        list.append( new MyLCD(this,"Acq-Time   [s]","Acq-Time:",grid,0));
        list.append( new MyLCD(this,"Coinc-Time [us]  ","Coinc-Time:",grid,1));
        list.append( new MyLCD(this,"Rate-AN [ev/s]","Rate-AN:",grid,2));
        list.append( new MyLCD(this,"Rate-KV [ev/s]","Rate-KV:",grid,3));
        list.append( new MyLCD(this,"Rate-KH [ev/s]","Rate-KH:",grid,4));
        list.append( new MyLCD(this,"Rate-Valid [ev/s]","Rate-Valid:",grid,5));

	connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
	timer->start(100);

	showTime();

	resize(700, 400);
}


#define ENTRIES 5
#define BUFFSIZE 16

#define AVG_SIZE 10

int rate_an[AVG_SIZE];
int rate_kv[AVG_SIZE];
int rate_kh[AVG_SIZE];
int rate_valid[AVG_SIZE];
int rate_an_ptr;
int rate_kv_ptr;
int rate_kh_ptr;
int rate_valid_ptr;

void init_avg(void)
{
    int i;
	for (i=0; i<AVG_SIZE; i++)
	{
		rate_an[i]=0;
		rate_kv[i]=0;
		rate_kh[i]=0;
		rate_valid[i]=0;
	}
	rate_an_ptr=0;
	rate_kv_ptr=0;
	rate_kh_ptr=0;
	rate_valid_ptr=0;
}



int avg(int* array, int* ptr,int val)
{
	double sum=0.0;
    int i;

    array[*ptr]=val*10;

	for (i=0; i<AVG_SIZE; i++)
	{
		sum += array[i];
	}

	sum = (sum / AVG_SIZE) + 0.5;
	(*ptr)++;
	if (*ptr>=AVG_SIZE) *ptr=0;
	return (int) sum;
}

void display::showTime()
{
	char buff[ENTRIES][BUFFSIZE];
	char *msg;
	int msg_len;
	int status;
	int val;
	int i;
    MyLCD *disp;

	status = do_request();
	if (status>=0)
	{
	  msg = ghttp_get_body(request);
	  msg_len = ghttp_get_body_len(request);


//	  for ( i = 0; i< ENTRIES ; i++)
//      {


    i=0;
    status = getint(msg, msg_len, "Acq-Time:", &val);
    if(status>0)
    {
      snprintf(buff[i],12,"%d",val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);

    i=1;
    status = getint(msg, msg_len, "Coinc-Time:", &val);
    if(status>0)
    {
      snprintf(buff[i],12,"%12.3f",0.05*val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);

    i=2;
    status = getint(msg, msg_len, "Rate-AN:", &val);
    if(status>0)
    {
      val = avg(rate_an, &rate_an_ptr, val);
      snprintf(buff[i],12,"%d",val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);


    i=3;
    status = getint(msg, msg_len, "Rate-KV:", &val);
    if(status>0)
    {
      val = avg(rate_kv, &rate_kv_ptr, val);
      snprintf(buff[i],12,"%d",val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);


    i=4;
    status = getint(msg, msg_len, "Rate-KH:", &val);
    if(status>0)
    {
      val = avg(rate_kh, &rate_kh_ptr, val);
      snprintf(buff[i],12,"%d",val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);


    i=5;
    status = getint(msg, msg_len, "Rate-Valid:", &val);
    if(status>0)
    {
      val = avg(rate_valid, &rate_valid_ptr, val);
      snprintf(buff[i],12,"%d",val);
    }
    else strncpy(buff[i],"Error",BUFFSIZE);




	}
	else
	{
      for ( i = 0; i< ENTRIES ; i++) strncpy(buff[i],"Error",BUFFSIZE);
	}
	close_request();


   i=0;
   for ( disp = list.first(); disp; disp = list.next() )
   {
		  disp->display(buff[i]);
		  i++;
   }


}
