#ifndef HTTP_H
#define HTTP_H

#include <ghttp.h>


 

/* This is the http request object */
extern ghttp_request *request;

void close_request(void);

int do_request(void);

int isnum(char c);

int getint(char* str, int n,const char* key, int *valp);


#endif
