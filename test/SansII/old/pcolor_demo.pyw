#!/usr/bin/env python
from __future__ import division
from pylab import *
import array

S=512
D=array.array('l')
f = open('sans2_2.dat',"rb")
D.fromfile(f,S*S)
f.close()
D.byteswap()

Z = arange(S*S)
Z.shape = S,S

for x in xrange(S):
  for y in xrange(S):
     Z[x][y]=D[y*S+x]
#     Z[x][y]=(x-S/2)**2 + (y-S/2)**2


#c = pcolor(Z)
#c.set_linewidth(0)
Zmax=max(D[1:])

#Zmax=300
im = imshow(Z, cmap=cm.jet,vmin=0, vmax=Zmax)
im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()

    
