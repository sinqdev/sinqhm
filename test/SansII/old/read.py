from nexus import *
from pylab import *
from numpy import *


def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i


def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)


def sans2_sum(rng, bank='AVG', sum=0):
    PATH = 'O:\\psi.ch\\project\\sinqdata\\2007\\sans2\\000\\'
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        print "sans22007n%06d.hdf"%i
        sum += getdata(PATH+"sans22007n%06d.hdf"%i,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum


#sum = sans2_sum('30..44','KH')
#sum = sans2_sum('21..23','KH')
# sum = sans2_sum(16,'KH',sum)
sum = sans2_sum('48..50','AVG')

range = sum[110:410,140:370]

im = imshow(range, cmap=cm.jet,origin='lower')
im.set_interpolation('nearest')
colorbar()
show()
