import sys
sys.path.append('../python')
from histomem import *
import array



hm = histomem('hm03.psi.ch','spy','007')


# hm.stop()




total_t = 0
total_s = 0
for i in xrange(10):
    start_time = time()
    data = hm.get_binary()
    stop_time = time()
    t = stop_time - start_time
    size = len(data)
    rate = size / 1024.0 / 1024.0 / t
    print t,size,"rate=",rate," MByte/s"
    total_t += t
    total_s += size

rate = total_s / 1024.0 / 1024.0 / total_t
print "total:",total_t,total_s,"rate=",rate," MByte/s"
