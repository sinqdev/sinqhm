#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
import array


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
    
LWL_SANS2_DATA     =  0x10000000
LWL_HDR_TYPE_MASK  =  0x1f000000



    
S=512
hm = histomem('lnse14.psi.ch','spy','007')

# hm.stop()
# hm.configure('conf.xml')

# hm.start()
#sleep(2.0)
#hm.stop()

D = hm.getrawdata()



X = arange(512)
Y = arange(512)

for i in xrange(512):
    X[i]=0
    Y[i]=0


for v in D:
  if ((v & LWL_HDR_TYPE_MASK) == LWL_SANS2_DATA):
      xPos = ((v >> 9) & 0x1ff)
      yPos = (v & 0x1ff)
      X[xPos] += 1  
      Y[yPos] += 1
    

print "X Werte"
for i in X:
  print i
  
print "Y Werte"
for i in Y:
  print i
  
    

    

#im = imshow(Z, cmap=cm.jet,vmin=0, vmax=Zmax)
#im.set_interpolation('nearest')

#colorbar()
#show()

    
