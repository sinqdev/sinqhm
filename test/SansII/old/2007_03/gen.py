
def srange(s):
   for e in s.split(','):
     r = e.split('..')
     if len(r) == 2:
       r = map(int,r)
       step = 1 if (r[1] > r[0]) else -1
       for i in xrange(r[0],r[1]+step,step):
         yield i
     else:
       yield int(e)
