import scipy, pylab

from scipy import optimize
from numpy import *

def read_data(data,filename):
    file = open(filename,'r')
    line = file.readline()
    x = 0
    for strval in line.split('\t'):
      data[x] = int(strval)
      x = x + 1
    file.close()

def read_data_52(data,filename):
    file = open(filename,'r')
    y=0
    for i in range(52):
      line = file.readline()
      if (i&1==1):
        x = 0
        for strval in line.split('\t'):
          data[y][x] = int(strval)
          x = x + 1
        y = y + 1
    file.close()
    
    
    
    
def fit(function, guess, x, y):
    p0 = guess(x,y)
    errfunc = lambda p, x, y: function(p,x)-y
    return optimize.leastsq(errfunc, p0, args=(x,y))        


def gauss(p, x):
    # define a gaussian fitting function where
    # p[0] = amplitude
    # p[1] = mean
    # p[2] = sigma
    return p[0]*scipy.exp(-(x-p[1])**2/(2.0*p[2]**2))


def initial_guess(x,y):
    amplitude = max(y)
    mean      = x[scipy.where(y==max(y))[0]][0]
    sigma     = 5
    print 'initial guess:'
    print 'amplitude = ', amplitude
    print 'mean      = ', mean
    print 'sigma     = ', sigma
    return [amplitude, mean, sigma]




d2 = scipy.zeros([26,512])
read_data_52(d2,'kdiff2.txt')




# make x data
kx = scipy.arange(-256,256)
#kx = scipy.arange(512)
print 'kx[256] = ', kx[256]


ky = scipy.zeros(512)
read_data(ky,'kdiff1.txt')

# fit
[amplitude, mean ,sigma], success = fit(gauss, initial_guess, kx, ky)    

hwhm = scipy.sqrt(2*scipy.log(2)) * sigma
fwhm = 2*hwhm

print '\n\nsuccess   = ', success
print 'amplitude = %8.2f'%amplitude
print 'mean      = %8.2f'%mean
print 'sigma     = %8.2f'%sigma
print 'fwhm      = %8.2f'%fwhm



ax = pylab.axes()
pylab.text(0.05, 0.95,'amplitude=%.3f\nmean=%.3f\n\nsigma=%.3f\nfwhm=%.3f'%(amplitude,mean,sigma,fwhm), 
     fontsize = 12, horizontalalignment='left', verticalalignment='top', transform = ax.transAxes)



# visualize the data
pylab.plot(kx, ky, 'k.')


fx = scipy.arange(kx[0],kx[-1],0.1)
corrfit = gauss([amplitude, mean ,sigma], fx)
pylab.plot(fx, corrfit, 'r-')


pylab.plot([mean, mean], [0, amplitude], 'g-')
pylab.plot([mean-hwhm, mean+hwhm], [amplitude/2.0, amplitude/2.0], 'g-')
pylab.plot([mean-hwhm, mean-hwhm], [0, amplitude/2.0], 'g-')
pylab.plot([mean+hwhm, mean+hwhm], [0, amplitude/2.0], 'g-')

pylab.show()