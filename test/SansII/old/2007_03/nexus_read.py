from nexus import *
from pylab import *
from numpy import *

def getdata(filename,bank):
    dataname={'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    status,nf=NXopen(filename,NXACC_READ)
#    status=NXopengroup(nf,"entry1", "NXentry")
#    status=NXopengroup(nf,"SANS-II", "NXinstrument")
#    status=NXopengroup(nf,"detector", "NXdetector")
#    status=NXopendata (nf, dataname[bank])
    status=NXopenpath(nf,'/entry1/SANS-II/detector/'+dataname[bank])
    status,data=NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)



def dsum(bank,start,stop):
  first = 1
  for i in xrange(start,stop+1):
    filename = "sans22007n%06d.hdf"%i
    print filename
    data=getdata(filename,bank)
    if first:
      sum = data
      first = 0
    else:
      sum += data
  return sum


#kv1 =  getdata("sans22007n000021.hdf",'KV')
#kh1 =  getdata("sans22007n000021.hdf",'KH')
#avg1 = getdata("sans22007n000021.hdf",'AVG')
#kv2 =  getdata("sans22007n000022.hdf",'KV')
#kh2 =  getdata("sans22007n000022.hdf",'KH')
#avg2 = getdata("sans22007n000022.hdf",'AVG')
#kv3 =  getdata("sans22007n000023.hdf",'KV')
#kh3 =  getdata("sans22007n000023.hdf",'KH')
#avg3 = getdata("sans22007n000023.hdf",'AVG')




avg = dsum('AVG',9,13)

im = imshow(avg, cmap=cm.jet,origin='lower')
#im = imshow(avg, cmap=cm.jet,vmin=0, vmax=Zmax)
im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()