import scipy, pylab

from scipy import optimize
from numpy import *
from nexus import *



def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i


def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)


def sans2_sum(rng, bank='AVG', sum=0):
    PATH = 'O:\\psi.ch\\project\\sinqdata\\2007\\sans2\\000\\'
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        print "sans22007n%06d.hdf"%i
        sum += getdata(PATH+"sans22007n%06d.hdf"%i,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum



    
    
    
    
def fit(function, guess, x, y):
    p0 = guess(x,y)
    errfunc = lambda p, x, y: function(p,x)-y
    return optimize.leastsq(errfunc, p0, args=(x,y))        


def gauss(p, x):
    # define a gaussian fitting function where
    # p[0] = amplitude
    # p[1] = mean
    # p[2] = sigma
    return p[0]*scipy.exp(-(x-p[1])**2/(2.0*p[2]**2))


def initial_guess(x,y):
    amplitude = max(y)
    mean      = x[scipy.where(y==max(y))[0]][0]
    sigma     = 5
#    print 'initial guess:'
#    print 'amplitude = ', amplitude
#    print 'mean      = ', mean
#    print 'sigma     = ', sigma
    return [amplitude, mean, sigma]


# make x data
kx = scipy.arange(-256,256)




sum = sans2_sum('30..44','KV')
#sum = sans2_sum('21..23','KV')




for i in range(26):
    ky = sum[1+2*i] 



    # fit
    [amplitude, mean ,sigma], success = fit(gauss, initial_guess, kx, ky)    
    hwhm = scipy.sqrt(2*scipy.log(2)) * sigma
    fwhm = 2*hwhm
    counts = scipy.sum(ky)
    #print '\n\nsuccess   = ', success
    print 'Channel %2d: counts = %8d  amplitude = %9.1f  mean = %8.2f  sigma = %8.2f  fwhm = %8.2f'%(i,counts,amplitude,mean,sigma,fwhm)





ky = sum[1] 
[amplitude, mean ,sigma], success = fit(gauss, initial_guess, kx, ky)    
hwhm = scipy.sqrt(2*scipy.log(2)) * sigma
fwhm = 2*hwhm

# visualize the data
pylab.plot(kx, ky, 'k.')

fx = scipy.arange(kx[0],kx[-1],0.1)
corrfit = gauss([amplitude, mean ,sigma], fx)
pylab.plot(fx, corrfit, 'r-')


pylab.plot([mean, mean], [0, amplitude], 'g-')
pylab.plot([mean-hwhm, mean+hwhm], [amplitude/2.0, amplitude/2.0], 'g-')
pylab.plot([mean-hwhm, mean-hwhm], [0, amplitude/2.0], 'g-')
pylab.plot([mean+hwhm, mean+hwhm], [0, amplitude/2.0], 'g-')

ax = pylab.axes()
pylab.text(0.05, 0.95,'amplitude=%.3f\nmean=%.3f\n\nsigma=%.3f\nfwhm=%.3f'%(amplitude,mean,sigma,fwhm), 
     fontsize = 12, horizontalalignment='left', verticalalignment='top', transform = ax.transAxes)

pylab.show()