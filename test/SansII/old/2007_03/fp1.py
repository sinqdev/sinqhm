import scipy, pylab

from scipy import optimize
from numpy import *

def read_data(data,filename):
    file = open(filename,'r')
    line = file.readline()
    x = 0
    for strval in line.split('\t'):
      data[x] = int(strval)
      x = x + 1
    file.close()

def read_data_52(data,filename):
    file = open(filename,'r')
    y=0
    print y
    for i in range(52):
      line = file.readline()
      if (i&1==1):
        x = 0
        for strval in line.split('\t'):
          data[y][x] = int(strval)
          x = x + 1
        y = y + 1
    file.close()
    
    



def pearson7(p, x):
    # define a gaussian fitting function where
    height  =    p[0]
    center  =    p[1]
    hwhm    =    p[2]
    shape   =    p[3]
    return height/(1+((x-center)/hwhm)**2*(2**(1/shape)-1))**shape


def guess_pearson7(x,y):
    height = max(y)
    center = x[scipy.where(y==max(y))[0]][0]
    hwhm   = 5
    shape  = 2
    return [height,center,hwhm,shape]

def info_pearson7(p):
    height  =    p[0]
    center  =    p[1]
    hwhm    =    p[2]
    shape   =    p[3]
    return 'height=%.3f  center=%.3f  hwhm=%.3f  shape=%.3f'%(height,center,hwhm,shape)
    


def gauss(p, x):
    # define a gaussian fitting function where
    # p[0] = amplitude
    # p[1] = mean
    # p[2] = sigma
    return p[0]*scipy.exp(-(x-p[1])**2/(2.0*p[2]**2))    

def guess_gauss(x,y):
    amplitude = max(y)
    mean      = x[scipy.where(y==max(y))[0]][0]
    sigma     = 5
    return [amplitude, mean, sigma]


def info_gauss(p):
    amplitude  = p[0] 
    mean       = p[1] 
    sigma      = p[2] 
    fwhm = 2 * scipy.sqrt(2*scipy.log(2)) * sigma
    return 'amplitude=%.3f  mean=%.3f  sigma=%.3f  fwhm=%.3f'%(amplitude,mean,sigma,fwhm)
    


    
def fit(function, guess, x, y):
    p0 = guess(x,y)
    errfunc = lambda p, x, y: function(p,x)-y
#    return optimize.fmin(errfunc, p0, args=(x,y))        
    return optimize.leastsq(errfunc, p0, args=(x,y))        




if 0:
    fit_func   =  gauss
    guess_func =  guess_gauss
    info       =  info_gauss
else:
    fit_func   =  pearson7
    guess_func =  guess_pearson7
    info       =  info_pearson7


# make x data
kx = scipy.arange(-256,256)


kdiff2 = scipy.zeros([26,512])
read_data_52(kdiff2,'kdiff2.txt')

kdiff3 = scipy.zeros([26,512])
read_data_52(kdiff3,'kdiff3.txt')

kdiff4 = scipy.zeros([26,512])
read_data_52(kdiff4,'kdiff4.txt')


if 1:
  for i in range(26):
    ky = kdiff2[i] + kdiff3[i] + kdiff4[i]

    #ky = scipy.zeros(512)
    #read_data(ky,'kdiff1.txt')

    # fit
    p1, success = fit(fit_func, guess_func, kx, ky)  
    counts = scipy.sum(ky)
    #print '\n\nsuccess   = ', success
    print 'Channel %2d: counts = %8d  '%(i,counts),info(p1)





ky = kdiff2[0] + kdiff3[0] + kdiff4[0]

file = open('c:\\fit.dat','w')
for i in range(512):
    file.write('%d %d\n'%(kx[i],ky[i]))
file.close()



p1, success = fit(fit_func, guess_func, kx, ky)    
print info(p1)
    
    
    
# visualize the data
pylab.plot(kx, ky, 'k.')

fx = scipy.arange(kx[0],kx[-1],0.1)
corrfit = fit_func(p1, fx)
pylab.plot(fx, corrfit, 'r-')

p1, success = fit(gauss, guess_gauss, kx, ky)    
corrfit = gauss(p1, fx)
pylab.plot(fx, corrfit, 'b-')

#pylab.plot([mean, mean], [0, amplitude], 'g-')
#pylab.plot([mean-hwhm, mean+hwhm], [amplitude/2.0, amplitude/2.0], 'g-')
#pylab.plot([mean-hwhm, mean-hwhm], [0, amplitude/2.0], 'g-')
#pylab.plot([mean+hwhm, mean+hwhm], [0, amplitude/2.0], 'g-')

#ax = pylab.axes()
#pylab.text(0.05, 0.95,'amplitude=%.3f\nmean=%.3f\n\nsigma=%.3f\nfwhm=%.3f'%(amplitude,mean,sigma,fwhm), 
#     fontsize = 12, horizontalalignment='left', verticalalignment='top', transform = ax.transAxes)

pylab.show()