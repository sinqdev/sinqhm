#!/usr/bin/env python
from __future__ import division
from pylab import *
import sys
sys.path.append('../python')
from histomem import *
import array
from time import *


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
hm = histomem('lnse14.psi.ch','spy','007')





filename = 'sans2_rate_'+strftime("%Y%m%d_%H%M%S", localtime())
file = open(filename,'w')



while 1:
    dict = hm.status_dict()
    rate_total = int(dict['rate-total'])*5
    rate_valid = int(dict['rate-valid'])*10
    string = '%s %d %d\n'%(strftime("%H:%M:%S", localtime()),rate_total,rate_valid)
    print string
    file.write(string)
    sleep(1)
    