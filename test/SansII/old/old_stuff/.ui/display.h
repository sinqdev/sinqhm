/****************************************************************************
** Form interface generated from reading ui file 'display.ui'
**
** Created: Wed Jun 28 15:36:11 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef FORM1_H
#define FORM1_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QLCDNumber;
class QLabel;

class Form1 : public QDialog
{
    Q_OBJECT

public:
    Form1( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~Form1();

    QLCDNumber* lCDNumber1_3;
    QLCDNumber* lCDNumber1_2;
    QLCDNumber* lCDNumber1;
    QLabel* textLabel1;
    QLabel* textLabel1_2;
    QLabel* textLabel1_3;

protected:
    QGridLayout* Form1Layout;

protected slots:
    virtual void languageChange();

};

#endif // FORM1_H
