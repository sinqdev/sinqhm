/****************************************************************************
** Form implementation generated from reading ui file 'display.ui'
**
** Created: Thu Sep 14 09:34:40 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "display.h"

#include <qvariant.h>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/* 
 *  Constructs a Form1 as a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
Form1::Form1( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "Form1" );
    setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    Form1Layout = new QGridLayout( this, 1, 1, 11, 6, "Form1Layout"); 

    lCDNumber1_3 = new QLCDNumber( this, "lCDNumber1_3" );
    lCDNumber1_3->setPaletteForegroundColor( QColor( 170, 0, 0 ) );
    lCDNumber1_3->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    QPalette pal;
    QColorGroup cg;
    cg.setColor( QColorGroup::Foreground, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 170, 0, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setDisabled( cg );
    lCDNumber1_3->setPalette( pal );
    lCDNumber1_3->setFrameShape( QLCDNumber::NoFrame );
    lCDNumber1_3->setSegmentStyle( QLCDNumber::Filled );

    Form1Layout->addWidget( lCDNumber1_3, 2, 1 );

    lCDNumber1_2 = new QLCDNumber( this, "lCDNumber1_2" );
    lCDNumber1_2->setPaletteForegroundColor( QColor( 0, 170, 0 ) );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setDisabled( cg );
    lCDNumber1_2->setPalette( pal );
    lCDNumber1_2->setFrameShape( QLCDNumber::NoFrame );
    lCDNumber1_2->setSegmentStyle( QLCDNumber::Filled );

    Form1Layout->addWidget( lCDNumber1_2, 1, 1 );

    lCDNumber1 = new QLCDNumber( this, "lCDNumber1" );
    lCDNumber1->setPaletteForegroundColor( QColor( 0, 170, 0 ) );
    lCDNumber1->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, black );
    cg.setColor( QColorGroup::LinkVisited, black );
    pal.setActive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, white );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, white );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setInactive( cg );
    cg.setColor( QColorGroup::Foreground, QColor( 0, 170, 0) );
    cg.setColor( QColorGroup::Button, black );
    cg.setColor( QColorGroup::Light, black );
    cg.setColor( QColorGroup::Midlight, black );
    cg.setColor( QColorGroup::Dark, black );
    cg.setColor( QColorGroup::Mid, black );
    cg.setColor( QColorGroup::Text, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::BrightText, white );
    cg.setColor( QColorGroup::ButtonText, QColor( 128, 128, 128) );
    cg.setColor( QColorGroup::Base, black );
    cg.setColor( QColorGroup::Background, black );
    cg.setColor( QColorGroup::Shadow, black );
    cg.setColor( QColorGroup::Highlight, QColor( 0, 0, 128) );
    cg.setColor( QColorGroup::HighlightedText, white );
    cg.setColor( QColorGroup::Link, QColor( 0, 0, 192) );
    cg.setColor( QColorGroup::LinkVisited, QColor( 128, 0, 128) );
    pal.setDisabled( cg );
    lCDNumber1->setPalette( pal );
    lCDNumber1->setFrameShape( QLCDNumber::NoFrame );
    lCDNumber1->setSegmentStyle( QLCDNumber::Filled );

    Form1Layout->addWidget( lCDNumber1, 0, 1 );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setPaletteForegroundColor( QColor( 0, 170, 0 ) );
    QFont textLabel1_font(  textLabel1->font() );
    textLabel1_font.setPointSize( 14 );
    textLabel1->setFont( textLabel1_font ); 

    Form1Layout->addWidget( textLabel1, 0, 0 );

    textLabel1_2 = new QLabel( this, "textLabel1_2" );
    textLabel1_2->setPaletteForegroundColor( QColor( 0, 170, 0 ) );
    QFont textLabel1_2_font(  textLabel1_2->font() );
    textLabel1_2_font.setPointSize( 14 );
    textLabel1_2->setFont( textLabel1_2_font ); 

    Form1Layout->addWidget( textLabel1_2, 1, 0 );

    textLabel1_3 = new QLabel( this, "textLabel1_3" );
    textLabel1_3->setPaletteForegroundColor( QColor( 0, 170, 0 ) );
    QFont textLabel1_3_font(  textLabel1_3->font() );
    textLabel1_3_font.setPointSize( 14 );
    textLabel1_3->setFont( textLabel1_3_font ); 

    Form1Layout->addWidget( textLabel1_3, 2, 0 );
    languageChange();
    resize( QSize(600, 480).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
Form1::~Form1()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void Form1::languageChange()
{
    setCaption( tr( "Form1" ) );
    textLabel1->setText( tr( "textLabel1" ) );
    textLabel1_2->setText( tr( "textLabel2" ) );
    textLabel1_3->setText( tr( "textLabel3" ) );
}

