#include <qapplication.h>
#include "display.h"

int main( int argc, char* argv[] )
{
  QApplication app( argc, argv );

  Form1 dialog;
  app.setMainWidget( &dialog );
  dialog.show();

  int ret = app.exec();
  return ret;
}
