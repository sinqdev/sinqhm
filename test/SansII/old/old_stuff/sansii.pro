unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
SOURCES += main.cpp
FORMS	= display.ui
TEMPLATE	=app
CONFIG	+= qt warn_on release
LANGUAGE	= C++
