#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
from numpy import *


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
S=512
hm = histomem('lnse14.psi.ch','spy','007')
#hm = histomem('hm03.psi.ch','spy','007')

#hm.stop()
#hm.configure('conf.xml')
#hm.start()
#sleep(2.0)
#hm.stop()
# Z = arange(S*S)

Z = hm.get(bank=2)
Z.shape = S,S

# write_data(Z,'data.dat')



if 0:
 Zmax = 0
 for x in xrange(S):
  for y in xrange(S):
     val=D[x*S+y]
     Z[S-1-y][x]=val
     if (x>border) and (x<(S-border)) and (y>border) and (y<(S-border)):
       if val > Zmax:
         Zmax = val
#     Z[x][y]=(x-S/2)**2 + (y-S/2)**2




#cm.jet cm.gray
border = 60
#range = Z[110:410,140:370]
#Zmax=max(range)
Zmax=(Z[border:-border,border:-border]).max()
Zmax = max(Zmax,1)

im = imshow(Z, cmap=cm.jet,origin='lower',vmin=0, vmax=Zmax)




#range = Z[110:410,140:370]
#im = imshow(range, cmap=cm.jet,origin='lower')


#im = imshow(Z, cmap=cm.jet,origin='lower')

im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()

    
