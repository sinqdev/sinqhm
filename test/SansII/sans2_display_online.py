#!/usr/bin/env python
from __future__ import division
from pylab import *
import sys
sys.path.append('../python')
from histomem import *
from numpy import *
from time import *
from matplotlib.widgets import RectangleSelector
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons
from matplotlib.widgets import Slider
from nexus import *
import os

DIMX = DIMY = 340

#------------------------------------------------------------------------------
# Select what to plot
#------------------------------------------------------------------------------

SINQ_DATA_PATH = 'O:\\psi.ch\\project\\sinqdata'

online    = 1
swap      = 0          # swap x and y axes, if 1 

#------------------------------------------------------------------------------
# old sans format

old_sans_format = 0
old_sans_iterate_dir = 0

#old_sans_path   = 'O:\\psi.ch\\project\\sinqdata\\2002\\sans2\\strunz082002'
#old_sans_path   = r'O:\psi.ch\group\sinqhm\sinqhmrtai\test\SansII\data\old'
old_sans_path   = r'O:\psi.ch\project\sinqdata\2002\sans2\kell'
old_sans_name   = 's0000160.san'
#old_sans_name   = 's0000156.san'


#------------------------------------------------------------------------------
# nexus format

#hdf_range = '148..149'
#hdf_range = '145..146'
#hdf_range = 4995
#year = 2005
year = 2007
#hdf_range = '148,153,163,164,165'
#hdf_range = '195..205'
#hdf_range = '79..95'
hdf_range = '214..218'
#hdf_range =  218

# AgBe
#4.5A 3600 sec hdf 188
#6.4A 2x3600 sec hdf 189 und 191
#10.6A ca.32120 sec hdf 190



histo_name = 'lnse14.psi.ch'



#------------------------------------------------------------------------------

border = 60   # ignore  border for Zmax 

autorange = 0
autoscale = 1

#------------------------------------------------------------------------------
#
# Write Data
#
#------------------------------------------------------------------------------

def write_data(data,filename):
    file = open(filename,'w')
    for x in xrange(int(DIMX)):
      for y in xrange(int(DIMY)):
        file.write('%9d '%data[y,x])
      file.write('\n')
    file.close()


def read_old_sans2_format(filename):
    global DIMY,DIMX
    data=[]
    file = open(filename,'r')
    lines = file.readlines()
    for line in lines:
      if line.find('#') <0 :
        for v in line.split():
          data.append(int(v))
    return array(data)
    
   
    
def old_sans_files(path):
    for f in os.listdir(path):
        if f.endswith('.san'):
            yield(f)
        
if old_sans_format:
  filelist = old_sans_files(old_sans_path)
        
#------------------------------------------------------------------------------
#
# Get Data
#
#------------------------------------------------------------------------------

def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i

#------------------------------------------------------------------------------

def sinq_data_filename(base_path, instrument, num, year=2007):
    return (   '%s\\%d\\%s\\%03d\\%s%dn%06d.hdf'
             % (base_path,year,instrument,int(num//1000),instrument,year,num)
           )

#------------------------------------------------------------------------------

def get_histo(online):
  global DIMX, DIMY
  global Z0, Z1, Z2
  
  if online:
    Z0 = hm.get(bank=0)
    Z1 = hm.get(bank=1)
    Z2 = hm.get(bank=2)
  else:
    if old_sans_format:
      if old_sans_iterate_dir:
        filename = old_sans_path + '\\' + filelist.next()
      else:
        filename = old_sans_path + '\\' + old_sans_name
      print filename
      Z0 = Z1 = Z2 = read_old_sans2_format(filename)
    else:
      Z0, Z1, Z2 = sans2_sum_b0_b1_b2(hdf_range,year) 
    


  DIMX = DIMY = sqrt(Z0.size)
  Z0.shape = DIMY, DIMX
  Z1.shape = DIMY, DIMX
  Z2.shape = DIMY, DIMX
  
  if swap:
      Z0 = Z0.swapaxes(0,1)
      Z1 = Z1.swapaxes(0,1)
      Z2 = Z2.swapaxes(0,1)

#------------------------------------------------------------------------------

def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)


#------------------------------------------------------------------------------

def sans2_sum(rng, bank='AVG', sum=0,year=2007):
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        filename = sinq_data_filename(SINQ_DATA_PATH, 'sans2', i, year)
        print filename
        sum += getdata(filename,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum


#------------------------------------------------------------------------------

def getdata_b0_b1_b2(filename):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_front')
    status, b0 = NXgetdata(nf)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_back')
    status, b1 = NXgetdata(nf)
    status = NXopenpath(nf,'/entry1/SANS-II/detector/counts_anode')
    status, b2 = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(b0),array(b1),array(b2)


#------------------------------------------------------------------------------

def sans2_sum_b0_b1_b2(rng,year=2007):
    s0 = s1 = s2 = 0
    if year >= 2007:
        for i in srange(rng):
            filename = sinq_data_filename(SINQ_DATA_PATH, 'sans2', i, year)
            print filename
            b0,b1,b2 = getdata_b0_b1_b2(filename)
            s0 += b0
            s1 += b1
            s2 += b2
        return s0,s1,s2
    else:
        for i in srange(rng):
            filename = sinq_data_filename(SINQ_DATA_PATH, 'sans2', i, year)
            print filename
            b0 = getdata(filename,'/entry1/data1/counts')
            s0 += b0
        return s0,s0,s0    


#------------------------------------------------------------------------------
#
#  GUI routines
#
#------------------------------------------------------------------------------
    
def line_select_callback(event1, event2):
    global g_x1, g_x2, g_y1, g_y2
    
    'event1 and event2 are the press and release events'
    x1, y1 = int(round(event1.xdata)), int(round(event1.ydata))
    x2, y2 = int(round(event2.xdata)), int(round(event2.ydata))

    g_x1, g_x2 = min(x1,x2), max(x1,x2)+1
    g_y1, g_y2 = min(y1,y2), max(y1,y2)+1

    #print "(%3.2f, %3.2f) --> (%3.2f, %3.2f)"%(x1,y1,x2,y2)
    #print " The button you used were: ",event1.button, event2.button
    update_cut(g_x1,g_x2,g_y1,g_y2,event1.button)
    mainfig.canvas.draw()                         # redraw the canvas    


#------------------------------------------------------------------------------

def update_cut(x1,x2,y1,y2,button):

    range = Z[y1:y2,x1:x2]
    sx = range.sum(0)
    sy = range.sum(1)


#    if button == 1:    # left mouse button 
    if True:            # always

        line_x.set_data(arange(x1,x2),sx)          # update the data
        ax_x.set_xlim(x1,max(x2-1,x1+1))
        ax_x.set_ylim(min(sx),max(sx))

        line_y.set_data(sy,arange(y1,y2))          # update the data
        ax_y.set_ylim(y1,max(y2-1,y1+1))
        ax_y.set_xlim(min(sy),max(sy))


    if button > 1:         # middle or right mouse button
        newfig = figure()
#        newax_x = newfig.add_subplot(111, xlim=(0,1), ylim=(0,1), autoscale_on=False)
        newax_x = newfig.add_subplot(212)
        newax_x.plot(arange(x1,x2),sx)
        newax_x.set_xlim(x1,max(x2-1,x1+1))
        newax_x.set_ylim(min(sx),max(sx))
        xlabel('X Axis (Anode) --->')
        ylabel('Summed Counts --->')
        
        newax_y = newfig.add_subplot(211)
        newax_y.plot(arange(y1,y2),sy)          # update the data
        newax_y.set_xlim(y1,max(y2-1,y1+1))
        newax_y.set_ylim(min(sy),max(sy))
        xlabel('Y Axis (Cathode) --->')
        ylabel('Summed Counts --->')
       
        show()


#------------------------------------------------------------------------------

class ButtonCallback:
    def __init__(self):
        self.cn = 0
        
    def update(self, event):
        global Z,hdf_range,old_sans_num
        if type(hdf_range)==int:
          hdf_range += 1
        get_histo(online)
        Z = [Z0,Z1,Z2][g_bank]
        if autoscale: imscale()
        redraw(range_change=1)
        

    def scale(self, event):
        imscale()
        mainfig.canvas.draw()                         # redraw the canvas

    def color(self, event):
        self.cn += 1
        if self.cn > len(cm.cmapnames): self.cn = 0
        im.set_cmap(cm.get_cmap(cm.cmapnames[self.cn]))
        mainfig.canvas.draw()                         # redraw the canvas

#------------------------------------------------------------------------------

def imscale():
    global g_max
    g_max = (Z[border:-border,border:-border]).max()
    g_max = min(max(g_max,1),2**16-1)
    im_norm()


def im_norm():
    lmax = g_max * g_maxfactor
    lmax = min(max(lmax,1),2**16-1)

    norm = Normalize(vmin=0, vmax=lmax)
    im.set_norm(norm)
    mainfig.canvas.draw() 

#------------------------------------------------------------------------------

def bankfunc(label):
    global Z,g_bank
    bankdict = {'AVG':0, 'KV':1, 'KH':2}
    g_bank = bankdict[label]
    Z = [Z0,Z1,Z2][g_bank]
    redraw(range_change=0)
    

#------------------------------------------------------------------------------

def slider_update(val):
    global g_maxfactor
    g_maxfactor  = samp.val / 100.0
    im_norm()

#------------------------------------------------------------------------------

def redraw(range_change=0):
    global g_button,g_x1,g_x2,g_y1,g_y2

    im.set_data(Z)

    if range_change and autorange:
      g_x1 = 0
      g_x2 = DIMX
      g_y1 = 0
      g_y2 = DIMY

    update_cut(g_x1,g_x2,g_y1,g_y2,g_button)
 
    g_button = 1
    mainfig.canvas.draw()                         # redraw the canvas


#------------------------------------------------------------------------------
#
#  main
#
#------------------------------------------------------------------------------

#------------------
# get initial data
#------------------

if online:
    hm = histomem(histo_name,'spy','007')

get_histo(online)

write_data(Z0,'Sans2_214_to_218.txt')


g_x1 = 0
g_x2 = DIMX
g_y1 = 0
g_y2 = DIMY
g_button = 1
g_bank = 0
g_maxfactor = 1.0

Z = Z0

g_max=(Z[border:-border,border:-border]).max()
g_max = max(g_max,1)
#print Zmax

#--------------------
# 2D - plot
#--------------------

ioff()    # turn interactive mode off

mainfig = figure()
current_ax = axes([0.05, 0.3, 0.6, 0.62], axisbg='w')

im = imshow(Z, cmap=cm.jet,origin='lower',vmin=0, vmax=g_max,aspect = 'auto')
#colorbar()
#im = imshow(Z, cmap=cm.jet,origin='lower')

#range = Z[110:410,140:370]
#im = imshow(range, cmap=cm.jet,origin='lower')

im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36
                      

sx = Z.sum(0)
sy = Z.sum(1)


ax_x = mainfig.add_axes([0.05, 0.05, 0.6, 0.2], axisbg='w')
line_x, = ax_x.plot(arange(DIMX),sx)
ax_x.set_xlim(0,DIMX)
ax_x.set_ylim(min(sx),max(sx))

ax_y = mainfig.add_axes([0.7, 0.3, 0.28, 0.68], axisbg='w')
line_y, = ax_y.plot(sy,arange(DIMY))
ax_y.set_ylim(0,DIMY)
ax_y.set_xlim(min(sy),max(sy))


#----------------
# Range Selector
#----------------

rectprops = dict(facecolor='yellow', edgecolor = 'white', alpha=0.5, fill=True)
LS = RectangleSelector(current_ax, line_select_callback,
                      drawtype='box',useblit=False,rectprops=rectprops)


#----------
# buttons
#----------

bcb    = ButtonCallback()
ax_update = mainfig.add_axes([0.85, 0.05, 0.1, 0.05])
b_update  = Button(ax_update, 'Update')
b_update.on_clicked(bcb.update) 

ax_scale = mainfig.add_axes([0.85, 0.12, 0.1, 0.05])
b_scale  = Button(ax_scale, 'Scale')
b_scale.on_clicked(bcb.scale) 

ax_color = mainfig.add_axes([0.85, 0.19, 0.1, 0.05])
b_color  = Button(ax_color, 'Color')
b_color.on_clicked(bcb.color) 

#----------
# buttons
#----------

axcolor = 'lightgoldenrodyellow'
rax     = mainfig.add_axes([0.7, 0.05, 0.10, 0.10], axisbg=axcolor)
radio   = RadioButtons(rax, ('AVG', 'KV', 'KH'))
radio.on_clicked(bankfunc)

#------------------------
# slider for Zmax factor
#------------------------

axcolor = 'lightgoldenrodyellow'
axamp   = axes([0.05, 0.95, 0.6, 0.03], axisbg=axcolor)
samp    = Slider(axamp, '', 0.01, 200.0, valinit=100, valfmt='%1.0f')
samp.on_changed(slider_update)


# Enter main loop of GUI library
show()

#------------------------------------------------------------------------------
