#!/usr/bin/env python
from __future__ import division
from pylab import *
import sys
sys.path.append('../python')
#from histomem import *
import array


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

    
def write_data_n(data,n,filename):
    file = open(filename,'w')
    for y in xrange(n):
      for x in xrange(n):
        file.write('%d'%data[x][y])
        if (x==(n-1)):
            file.write('\n')
        else:
            file.write('\t')
    file.close()
    
    
def read_data(data,filename):
    file = open(filename,'r')
    y = 0
    for line in file.readlines():
      x = 0
      for strval in line.split('\t'):
        data[x][y] = int(strval)
        x = x + 1
      y = y + 1
    file.close()
    
    
    
S=512
# hm = histomem('lnse14.psi.ch','spy','007')
#hm = histomem('hm03.psi.ch','spy','007')

# hm.stop()
# hm.configure('conf.xml')

# hm.start()
#sleep(2.0)
#hm.stop()

#D = hm.get(bank=0)
#write_data(D,'data.dat')

Z = arange(S*S)
Z.shape = S,S

SUM = arange(S*S)
SUM.shape = S,S

read_data(SUM ,'sum.txt')
#SUM = Z
#read_data(Z,'s10.txt')
#SUM = SUM + Z
#read_data(Z,'s11.txt')
#SUM = SUM + Z
#read_data(Z,'s12.txt')
#SUM = SUM + Z



#write_data_n(SUM,S,'sum.txt')


Zmax = 0
border = 1




#c = pcolor(Z)
#c.set_linewidth(0)
#Zmax=max(D[1:])

# cm.jet cm.gray
im = imshow(SUM, cmap=cm.jet)
# im = imshow(Z, cmap=cm.jet,vmin=0, vmax=Zmax)
im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()

    
