import scipy, pylab

from scipy import optimize
from numpy import *
from nexus import *
import sys
sys.path.append('../python')
from histomem import *

#------------------------------------------------------------------------------
# parameter 
#------------------------------------------------------------------------------

SINQ_DATA_PATH = 'O:\\psi.ch\\project\\sinqdata'

online    = 0           # 0 : read hdf files
                        # 1 : read from histomem
                        
hdf_range = 223
#hdf_range = '69..106'
# '21..23'
# '30..44'
# '48..50'
# '52..54'   # connector A,B je CVC getauscht
# '55..61'   # connector A,B je CVC getauscht, K1 und K2 getauscht

histo_name = 'lnse14.psi.ch'

pearson = 0  #  1 : use pearson
             #  0 : use gauss

#------------------------------------------------------------------------------

def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i

#------------------------------------------------------------------------------

def sinq_data_filename(base_path, instrument, num, year=2007):
    return (   '%s\\%d\\%s\\%03d\\%s%dn%06d.hdf'
             % (base_path,year,instrument,int(num//1000),instrument,year,num)
           )

#------------------------------------------------------------------------------


def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)

#------------------------------------------------------------------------------

def sans2_sum(rng, bank='AVG', sum=0,year=2007):
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        filename = sinq_data_filename(SINQ_DATA_PATH, 'sans2', i, year)
        print filename
        sum += getdata(filename,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum

#------------------------------------------------------------------------------

def lorentz(p, x):
    # p[0] = amplitude
    # p[1] = mean
    # p[2] = sigma
    return p[0]*(p[2]/(scipy.pi)/((x-p[1])**2 + (p[2]**2)))
    
#------------------------------------------------------------------------------

def pearson7(p, x):
    # define a gaussian fitting function where
    height  =    p[0]
    center  =    p[1]
    hwhm    =    p[2]
    shape   =    p[3]
    return height/(1+((x-center)/hwhm)**2*(2**(1/shape)-1))**shape

#------------------------------------------------------------------------------

def guess_pearson7(x,y):
    height = max(y)
    center = x[scipy.where(y==max(y))[0]][0]
    hwhm   = 5
    shape  = 2
    return [height,center,hwhm,shape]

#------------------------------------------------------------------------------

def info_pearson7(p):
    height  =    p[0]
    center  =    p[1]
    hwhm    =    p[2]
    shape   =    p[3]
    return 'height=%10.1f  center=%6.3f  hwhm=%6.3f  shape=%6.3f'%(height,center,hwhm,shape)

#------------------------------------------------------------------------------

def gauss(p, x):
    # define a gaussian fitting function where
    # p[0] = amplitude
    # p[1] = mean
    # p[2] = sigma
    return p[0]*scipy.exp(-(x-p[1])**2/(2.0*p[2]**2))    

#------------------------------------------------------------------------------

def guess_gauss(x,y):
    amplitude = max(y)
    mean      = x[scipy.where(y==max(y))[0]][0]
    sigma     = 5
    return [amplitude, mean, sigma]

#------------------------------------------------------------------------------

def info_gauss(p):
    amplitude  = p[0] 
    mean       = p[1] 
    sigma      = p[2] 
    fwhm = 2 * scipy.sqrt(2*scipy.log(2)) * sigma
    return 'amplitude=%10.1f  mean=%6.3f  sigma=%6.3f  fwhm=%6.3f'%(amplitude,mean,sigma,fwhm)
    
#------------------------------------------------------------------------------

    
def fit(function, guess, x, y):
    p0 = guess(x,y)
    errfunc = lambda p, x, y: function(p,x)-y
#    return optimize.fmin(errfunc, p0, args=(x,y))        
    return optimize.leastsq(errfunc, p0, args=(x,y))        

#------------------------------------------------------------------------------



if pearson:
    fit_func   =  pearson7
    guess_func =  guess_pearson7
    info       =  info_pearson7
else:
    fit_func   =  gauss
    guess_func =  guess_gauss
    info       =  info_gauss



# make x data
kx = scipy.arange(-256,256)



if online:
    hm = histomem(histo_name,'spy','007')
    sum = array(hm.get(bank=1))
    sum.shape=(512,512)
else:
    sum = sans2_sum(hdf_range,'KV')




for i in range(26):
    ky = sum[1+2*i] 
    # fit
    p1, success = fit(fit_func, guess_func, kx, ky)  
    counts = scipy.sum(ky)
    #print '\n\nsuccess   = ', success
    print 'Channel %2d: counts = %8d  '%(i,counts),info(p1)



# visualize the data
ky = sum[1] 
pylab.plot(kx, ky, 'k.')


fx = scipy.arange(kx[0],kx[-1],0.1)
p1, success = fit(fit_func, guess_func, kx, ky)    
corrfit = fit_func(p1, fx)
pylab.plot(fx, corrfit, 'r-')

if (fit_func == gauss):
    [amplitude, mean ,sigma] = p1
    hwhm = scipy.sqrt(2*scipy.log(2)) * sigma
    fwhm = 2*hwhm
    pylab.plot([mean, mean], [0, amplitude], 'g-')
    pylab.plot([mean-hwhm, mean+hwhm], [amplitude/2.0, amplitude/2.0], 'g-')
    pylab.plot([mean-hwhm, mean-hwhm], [0, amplitude/2.0], 'g-')
    pylab.plot([mean+hwhm, mean+hwhm], [0, amplitude/2.0], 'g-')

ax = pylab.axes()
pylab.text(0.05, 0.95,info(p1), 
           fontsize = 12, horizontalalignment='left', verticalalignment='top', transform = ax.transAxes)

pylab.show()
