#!/usr/bin/env python
from __future__ import division
import sys
sys.path.append('../python')
from histomem import *
from nexus import *
from pylab import *
from numpy import *



#------------------------------------------------------------------------------
# parameter 
#------------------------------------------------------------------------------

SINQ_DATA_PATH = 'O:\\psi.ch\\project\\sinqdata'


online    = 0           # 0 : read hdf files
                        # 1 : read from histomem

hdf_range = 223
#hdf_range = '168..169'
#hdf_range = '145..146'

histo_name = 'lnse14.psi.ch'


#------------------------------------------------------------------------------

def printf(format,*args): sys.stdout.write(format % args)

#------------------------------------------------------------------------------

def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i

#------------------------------------------------------------------------------

def sinq_data_filename(base_path, instrument, num, year=2007):
    return (   '%s\\%d\\%s\\%03d\\%s%dn%06d.hdf'
             % (base_path,year,instrument,int(num//1000),instrument,year,num)
           )

#------------------------------------------------------------------------------

def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)

#------------------------------------------------------------------------------

def sans2_sum(rng, bank='AVG', sum=0, year=2007):
    dataname = {'AVG':'counts_front','KV':'counts_back','KH':'counts_anode'}
    for i in srange(rng):
        filename = sinq_data_filename(SINQ_DATA_PATH, 'sans2', i, year)
        print filename
        sum += getdata(filename,'/entry1/SANS-II/detector/'+dataname[bank])
    return sum

#------------------------------------------------------------------------------


if online:
    hm = histomem(histo_name,'spy','007')
    D = hm.get(bank=2)
else:
    D = sans2_sum(hdf_range,'KH')

D.shape = (D.size,)


t = D[0]
printf ("Total                  : %9d\n",t)
t = max (t,1) # avoid division by zero !

v = D[1]
p = 100.0 * v / t
printf ("b0 valid               : %9d (%6.2f %%)\n",v,p)

v = D[2]
p = 100.0 * v / t
printf ("b1 valid               : %9d (%6.2f %%)\n",v,p)

v = D[3]
p = 100.0 * v / t
printf ("b2 valid               : %9d (%6.2f %%)\n",v,p)

v = D[4]
p = 100.0 * v / t
printf ("kv_valid   & kh_invalid: %9d (%6.2f %%)\n",v,p)

v = D[5]
p = 100.0 * v / t
printf ("kv_invalid & kh_valid  : %9d (%6.2f %%)\n",v,p)

v = D[6]
p = 100.0 * v / t
printf ("an_invalid             : %9d (%6.2f %%)\n",v,p)

v = D[7]
p = 100.0 * v / t
printf ("kv_invalid             : %9d (%6.2f %%)\n",v,p)

v = D[8]
p = 100.0 * v / t
printf ("kh_invalid             : %9d (%6.2f %%)\n",v,p)

v = D[9]
p = 100.0 * v / t
printf ("AN == 0                : %9d (%6.2f %%)\n",v,p)

v = D[10]
p = 100.0 * v / t
printf ("AN == 0x1ff            : %9d (%6.2f %%)\n",v,p)

kve = D[11]
p = 100.0 * kve / t
printf ("KV Event               : %9d (%6.2f %%)\n",kve,p)

v = D[12]
p = 100.0 * v / max(kve,1)
printf ("KV == 0                : %9d (%6.2f %% of KV events)\n",v,p)

v = D[13]
p = 100.0 * v / max(kve,1)
printf ("KV == 0x1ff            : %9d (%6.2f %% of KV events)\n",v,p)

khe = D[14]
p = 100.0 * khe / t
printf ("KH Event               : %9d (%6.2f %%)\n",khe,p)

v = D[15]
p = 100.0 * v / max(khe,1)
printf ("KH == 0                : %9d (%6.2f %% of KH events)\n",v,p)

v = D[16]
p = 100.0 * v / max(khe,1)
printf ("KH == 0x1ff            : %9d (%6.2f %% of KH events)\n",v,p)

