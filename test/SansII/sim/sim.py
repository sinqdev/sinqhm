#!/usr/bin/env python
from __future__ import division
from pylab import *
from numpy import *


def load_data(filename):
    file = open(filename,'r')
    a=[]
    for i in file.readlines():
#      print i,len(i)
#      print int(i)
      a.append(int(i))
    return array(a)



def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i
          
          

NUMW   =  149
MIDDLE = (NUMW-1)//2

wire_charge_ratio = zeros(NUMW)
RR = 3300.0
RM = 68.0     # Anode
# RM = 47.0    #  Kathode
RG = 2*RR + (NUMW-1) * RM

for wire in xrange(NUMW):
  RA = RR + wire * RM
  wire_charge_ratio[wire] = (RG - RA)/RG
    
print 'wire_charge_ratio:' , wire_charge_ratio[0],wire_charge_ratio[74],wire_charge_ratio[148]
    
    
wirelen = zeros(NUMW)
    
r = 300
for wire in xrange(NUMW):
  dist = abs(MIDDLE-wire)*4
  wirelen[wire]  = 2 * sqrt(r**2 - dist**2)
    
print 'wlen:' , wirelen[0],wirelen[74],wirelen[148]

QBINS = 512
HEIGHT = 100
wires = range(NUMW)    
        
        
spec_dat = load_data('data.txt')
l = int( len(spec_dat) *0.6)

CBIN = l//QBINS + 1
spec_h = zeros(QBINS)

for i in xrange(l):
    spec_h[i//CBIN] += spec_dat[i]

hmax = max(spec_h[QBINS//10:])
div = hmax//HEIGHT

for i in xrange(len(spec_h)):
  spec_h[i] = min(spec_h[i],2*hmax)

spec = spec_h // div


# spec = ones(QBINS)




a = zeros(QBINS)
b = zeros(QBINS)


RNG = 74
#WIRE = MIDDLE
WIRE = 74

for q in xrange(len(spec)):
  print q
  counts = int(spec[q])
  for c in xrange(counts):
    for w in xrange(WIRE-RNG, WIRE+RNG+1):
      fa = wire_charge_ratio[w]
      fb = 1.0 - fa
#      a[int(q*fa)] += 1
#      b[int(q*fb)] += 1
      a[int(q*fa)] += wirelen[w]
      b[int(q*fb)] += wirelen[w]


#a[0] = 0
#b[0] = 0


plot((spec_h*max(a))//(2*hmax))
plot(a)
plot(b)
show()
    