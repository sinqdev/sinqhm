#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
from numpy import *

hm = histomem('lnse14.psi.ch','spy','007')

Z = hm.get(bank=1)
Z.shape = 512,512


border = 60
Zmax=(Z[border:-border,border:-border]).max()
Zmax = max(Zmax,1)

im = imshow(Z, cmap=cm.jet,origin='lower',vmin=0, vmax=Zmax)

#im = imshow(Z, cmap=cm.jet,origin='lower')

#range = Z[110:410,140:370]
#im = imshow(range, cmap=cm.jet,origin='lower')

im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()
