#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import struct

import sys
sys.path.append('../python')
#from histomem import *

def printf(format,*args): sys.stdout.write(format % args)

__author__ = "Marc 'BlackJack' Rintsch"
__version__ = '0.1.0'
__date__ = '$Date: 2006-04-25 11:06:18 +0200 (Tue, 25 Apr 2006) $'
__revision__ = '$Rev: 849 $'


class BMPError(Exception):
    """Error for problems with BMP file."""
    pass


class BMP(object):

    HEADER_FMT = '< 2s i 4x i iii hh ii'
    HEADER_SIZE = struct.calcsize(HEADER_FMT)
   
    def __init__(self, filename):
        """Create a `BMP` object from the content of `fileobj`.
       
        Raises `BMPError` if the BMP in `fileobj` is not an uncompressed
        24 bits per pixel BMP.
        """

        self.fileobj = open(filename, 'rb')

        
        (self.signature,
         self.file_size,
         self.data_offset,
         self.info_size,
         self.width,
         self.height,
         self.planes,
         self.bit_count,
         self.compression,
         self.data_size
        ) = struct.unpack(self.HEADER_FMT, self.fileobj.read(self.HEADER_SIZE))
       
        if self.signature != 'BM':
            raise BMPError('not a BMP')
        if self.planes != 1:
            raise BMPError('image has more than one plane')
        if self.bit_count != 24:
            raise BMPError('not a 24 bpp image')
        if self.compression != 0:
            raise BMPError('compressed image')
       
        self.remaining_header = self.fileobj.read(  self.data_offset
                                             - self.HEADER_SIZE)
        self.data = self.fileobj.read()
       
        self.bytes_per_row = None
        self._calculate_bytes_per_row()
        self.fileobj.close()


    def _calculate_bytes_per_row(self):
        """Calculate the bytes per row.
       
        Row data in BMP images is padded to 32 bit boundaries.
        """
        self.bytes_per_row = ((self.width * self.bit_count - 1) // 32) * 4 + 4

    def get_size(self):
        return (self.width,self.height)
   

    def get_rgb(self,x,y):
#        offset = (self.height-y-1) * self.bytes_per_row
        offset = (self.height-y-1) * self.bytes_per_row
        row = self.data[offset:offset+self.bytes_per_row]
        return map(ord, row[x*3:x*3+3])

    def get_grey(self,x,y):
        red, green, blue = self.get_rgb(x,y)
        return  red+green+blue

   
    def dump(self,tg=0):
        """Convert image to 16 bits per pixel."""
        assert self.bit_count in (24, 16)
        if self.bit_count == 16:
            return
        #
        # Convert image data.
        #
        for x in xrange(self.width):
          for y in xrange(self.width):
              red, green, blue = self.get_rgb(x,y)
              if (red+green+blue) < 500 :
                printf("*")
#                tg.sans2(x,y)
              else:
                printf(" ")
          printf("\n")



def main():
#    tg = testgen('hm01.psi.ch',5555)
  bmp = BMP('test512.dat')
#    bmp.dump(tg)
  bmp.dump()
  if 0:    
    size_x, size_y = bmp.get_size()
    for y in xrange(size_y):
        for x in xrange(size_x):
            g = bmp.get_grey(x,y)
            if g < 500 :
                printf("*")
            else:
                printf(" ")
        printf("\n")
        
        
if __name__ == '__main__':
    main()