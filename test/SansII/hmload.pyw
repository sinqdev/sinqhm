#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
import array


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
S=512
# hm = histomem('lnse14.psi.ch','spy','007')
hm = histomem('hm03.psi.ch','spy','007')

# hm.stop()
# hm.configure('conf.xml')

# hm.start()
#sleep(2.0)
#hm.stop()

D = hm.get(bank=0)
write_data(D,'data.dat')

Z = arange(S*S)
Z.shape = S,S

Zmax = 0
border = 1

for x in xrange(S):
  for y in xrange(S):
     val=D[x*S+y]
     Z[S-1-y][x]=val
     if (x>border) and (x<(S-border)) and (y>border) and (y<(S-border)):
       if val > Zmax:
         Zmax = val
#     Z[x][y]=(x-S/2)**2 + (y-S/2)**2


#c = pcolor(Z)
#c.set_linewidth(0)
#Zmax=max(D[1:])

# cm.jet cm.gray
im = imshow(Z, cmap=cm.jet,vmin=0, vmax=Zmax)
im.set_interpolation('nearest')
# bicubic bilinear blackman100 blackman256 blackman64 nearest sinc144 sinc256 sinc64 spline16 spline36

colorbar()
show()

    
