import sys
sys.path.append('../python')
from histomem import *


def test_1():
    tg=testgen('lnse12.psi.ch',5555)
    for tube in xrange(74):
        print "tube ",tube
        for pos in xrange(512):
             tg.event(tube,pos)


def test_2():
    tg=testgen('lnse12.psi.ch',5555)
    for tube in xrange(74):
        print "tube ",tube
        for pos in xrange(512):
            for i in xrange(tube+1):
                tg.event(tube,pos)


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

#######################################
#
# main
#
#######################################

while 1:
  test_2()
