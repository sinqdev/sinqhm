
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>



int lwl_fifo = -1;

char lwl_fifo_pipe_name[256] = "/tmp/lwlfifo";

#define LWL_SIM_BUFFER_SIZE 1024

char lwl_sim_buffer[LWL_SIM_BUFFER_SIZE+1];
int lwl_sim_ppos = -1;
int lwl_sim_spos = -1;
int lwl_sim_bytes_read = 0;
int lwl_sim_comment = 0;
int lwl_sim_status  = 0;

int use_port = 0;
int sockfd =-1 ;


/******************************************************************************/

void error(char *msg)
{
    perror(msg);
    exit(1);
}

/******************************************************************************/

void sock_init(int portno)
{
     struct sockaddr_in serv_addr;
     int rv;

     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) error("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     rv = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
     if ( rv < 0) error("ERROR on binding");

     listen(sockfd,5);
}

/******************************************************************************/

int sock_accept(void)
{
  struct sockaddr_in cli_addr;
  int client_len, fd;

  client_len = sizeof(cli_addr);
  fd = accept(sockfd, (struct sockaddr *) &cli_addr, &client_len);
  if (fd < 0)  error("ERROR on accept");

  return fd;
}

/******************************************************************************/

int SetSocketTimeout(int connectSocket, int milliseconds)
{
   struct timeval tv;

    tv.tv_sec = milliseconds / 1000 ;
    tv.tv_usec = ( milliseconds % 1000) * 1000  ;

   return setsockopt (connectSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof tv);
}

unsigned int bla(void)
{
int bytes_read;
  lwl_fifo = sock_accept();

  while(1)
  {
     bytes_read = read(lwl_fifo,lwl_sim_buffer,LWL_SIM_BUFFER_SIZE);
//       bytes_read = recv(lwl_fifo, lwl_sim_buffer, LWL_SIM_BUFFER_SIZE, 0 );

      if (bytes_read>0) 
      {
        lwl_sim_buffer[bytes_read]=0;
        printf("%s",lwl_sim_buffer);
      }
    usleep(100000);
  } 

}




/******************************************************************************/

static void terminateSignal(int signal)
{
  printf("\n\nvme_lwl: Task terminated.\n\n");

  if (lwl_fifo>2) close(lwl_fifo);
  lwl_fifo = -1;

  exit(0);
}

/******************************************************************************/


int main(int argc, char *argv[])
{
  int port, i;
  unsigned int base = 0x1a00;
  unsigned int use_pipe = 0;


      port = 5555;
      use_port = 1;






  signal(SIGTERM,terminateSignal);
  signal(SIGINT,terminateSignal);


    printf("VMETgen Listen on port %d\n",port);
    sock_init(port);

  bla();

  return 0;

}
/*******************************************************************/


