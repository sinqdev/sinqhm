import urllib2
import struct
import array
import socket
import sys
from time import *

class Sock(socket.socket):
    def __init__(self,host,port):
        socket.socket.__init__(self,socket.AF_INET, socket.SOCK_STREAM)
        self.orig_send,self.send = self.send, self.send_all
        self.connect((host, port))

    def send_all(self,data):
        to_send = len(data)
        has_been_sent = 0;
        while (has_been_sent<to_send):
          has_been_sent += self.orig_send(data[has_been_sent:])
        return has_been_sent


def event(tube,pos):
  header  = 0x80000000
  header |= ( ((tube%10)+1) << 24)
  data = ( 1 << ( (tube//10) + 8 )) & 0xfe00
  data |= (pos & 0x1ff)
  string = " 0x%08x 0x%04x\n"%(header,data)
#  print string,
  testgen.send(string)

def test_1(histoname):
  global testgen
  testgen  = Sock(histoname, 5555)
  for tube in xrange(74):
    print "tube ",tube
    for pos in xrange(512):
  #    for i in xrange(tube+1):
        event(tube,pos)
  testgen.close()


def test_2(histoname):
  global testgen
  testgen  = Sock(histoname, 5555)
  for tube in xrange(74):
    print "tube ",tube
    for pos in xrange(512):
      for i in xrange(tube+1):
        event(tube,pos)
  testgen.close()


def init(histoname,user,passwd):
  pwmgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
  auth_handler = urllib2.HTTPBasicAuthHandler(pwmgr)
  auth_handler.add_password(None,histoname, user, passwd)
  opener = urllib2.build_opener(auth_handler)
  urllib2.install_opener(opener)

def get_histo(histoname):
  result = urllib2.urlopen('http://'+histoname+'/admin/readhmdata.egi')
  bin = result.read()
  a = array.array("L")
  for i in xrange(len(bin)//4):
    a.append(socket.ntohl(struct.unpack("L",bin[i*4:i*4+4])[0]))
  result.close()
  return a

def daq_start(histoname):
  result = urllib2.urlopen('http://'+histoname+'/admin/startdaq.egi')
  status = result.read()
  result.close()
  if status[0:15] != 'OK: DAQ Started':
    raise "ERROR starting DAQ:"+status

def daq_stop(histoname):
  result = urllib2.urlopen('http://'+histoname+'/admin/stopdaq.egi')
  status = result.read()
  result.close()
  if status[0:15] != 'OK: DAQ Stopped':
    raise "ERROR stopping DAQ:"+status

def configure(histoname,filename):
  file = open(filename)
  data = file.read()
  file.close()
  result = urllib2.urlopen('http://'+histoname+'/admin/configure.egi',data)
  status = result.read()
  result.close()
  if status[0:44] != 'OK: Histogram Memory successfully configured':
    raise "ERROR configuring DAQ:"+status


def write_data(data,filename):
  file = open(filename,'w')
  for i in xrange(len(data)):
    if (i>0 and i%512==0):
      file.write('\n')
    file.write('%6d '%data[i])
  file.close()



################################################
# main 
################################################

HM = 'lnse12.psi.ch'

init(HM,'spy','007')

daq_stop(HM)

configure(HM,'conf.xml')
daq_start(HM)
test_2(HM)
sleep(2.0)
daq_stop(HM)

data = get_histo(HM)
write_data(data,'data.txt')
print data
#print data[0:100]

