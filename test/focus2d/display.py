#!/usr/bin/env python

import sys,re
from PyQt4 import QtCore, QtGui
import sys
sys.path.append('../python')
from histomem import *


class MyLCD(QtGui.QWidget):
    def __init__(self, label, parent = None):
        QtGui.QWidget.__init__(self, parent)
        
        self.number = QtGui.QLCDNumber()
        self.number.setNumDigits (8)
        self.number.setSegmentStyle(QtGui.QLCDNumber.Filled)
        self.number.setFrameStyle(QtGui.QFrame.NoFrame)

        self.label = QtGui.QLabel(label,self)
        self.label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.label.setFont(QtGui.QFont("Times", 18, QtGui.QFont.Bold))

        gridLayout = QtGui.QGridLayout()
        gridLayout.addWidget(self.label, 0, 0)
        gridLayout.addWidget(self.number, 0, 1)
        gridLayout.setColumnStretch(1, 10)
        gridLayout.setColumnMinimumWidth(0,100)
        self.setLayout(gridLayout)        
        
    def display(self,text):
        self.number.display(text)

class MyWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.time = 0

#       quit = QtGui.QPushButton("Quit")
#       quit.setFont(QtGui.QFont("Times", 18, QtGui.QFont.Bold))
#       self.connect(quit, QtCore.SIGNAL("clicked()"),
#                    QtGui.qApp, QtCore.SLOT("quit()"))

        self.disp1 = MyLCD("L1",self)
        self.disp2 = MyLCD("L2dsadsa",self)
        self.disp3 = MyLCD("L3dsa",self)

        
        gridLayout = QtGui.QGridLayout()
#        gridLayout.addWidget(quit, 0, 0)
        gridLayout.addWidget(self.disp1, 1, 0)
        gridLayout.addWidget(self.disp2, 2, 0)
        gridLayout.addWidget(self.disp3, 3, 0)

        self.setLayout(gridLayout)

        timer = QtCore.QTimer(self)
        self.connect(timer, QtCore.SIGNAL("timeout()"), self.timerFunc)
        timer.start(20)

    def timerFunc(self):

        
        ts = hm.textstatus()
        m = re.search(r"LWL-Status:\s*(\d+)",ts)
        if (m):
          text=m.group(1)
        else:
          text='----'
        
        self.disp1.display(text)        

        self.time = self.time + 1
        text = "%d"%self.time        
        self.disp2.display(text)        

        text='----'
        self.disp3.display(text)        


if __name__ == "__main__":

    hm = histomem('hm03.psi.ch','spy','007')
    app = QtGui.QApplication(sys.argv)
    clock = MyWidget()
    clock.resize(600,500)
    clock.show()
    sys.exit(app.exec_())
    






