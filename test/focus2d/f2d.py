# Echo server program
import socket
import sys
# from msvcrt import *
from time import *


class Sock(socket.socket):
    def __init__(self,host,port):
        socket.socket.__init__(self,socket.AF_INET, socket.SOCK_STREAM)
        self.orig_send,self.send = self.send, self.send_all
        self.connect((host, port))

    def send_all(self,data):
        to_send = len(data)
        has_been_sent = 0;
        while (has_been_sent<to_send):
          has_been_sent += self.orig_send(data[has_been_sent:])
        return has_been_sent


class NB_Sock(socket.socket):
    def __init__(self,host,port):
        socket.socket.__init__(self,socket.AF_INET, socket.SOCK_STREAM)
        self.orig_send,self.send = self.send, self.send_nb
        self.orig_recv,self.recv = self.recv, self.recv_nb
        self.connect((host, port))
        self.setblocking(0)           # set to non blocking mode

    def send_nb(self,data):
        try:
            self.orig_send(data)
            data_send=1;
        except: 
            data_send=0;

        return data_send

    def recv_nb(self,bufflen=1024):
        try:
            data = self.orig_recv(bufflen)
            data_avail=1;
        except: 
            data =""
            data_avail=0;

        return data_avail,data


def event(tube,pos):
  header  = 0x80000000
  header |= ( ((tube%10)+1) << 24)
  data = ( 1 << ( (tube//10) + 8 )) & 0xfe00
  data |= (pos & 0x1ff)
  string = " 0x%08x 0x%04x\n"%(header,data)
#  print string,
  testgen.send(string)


testgen  = Sock('lnse12', 5555)

for tube in xrange(74):
  print "tube ",tube
  for pos in xrange(512):
#    for i in xrange(tube+1):
      event(tube,pos)
         

testgen.close()
