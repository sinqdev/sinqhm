#!/usr/bin/python
import sys
sys.path.append('../python')
from histomem import *

###############################################################################

def test_hrpt():
  
    hm.stop()
    hm.configure('hrpt.xml')
    hm.start()
  
    tg = testgen('hm01.psi.ch',6666)
    for frame in xrange(3):
      for addr in xrange(1600):
         count = addr + 1
         errorbit = 1
         tg.hrpt(addr | 0x2000, count | 0xffff, frame, errorbit)
#      tg.hrpt(1599, 0x1111, frame, 1)
      
      tg.hrpt(1600, 0x4321, frame, 1)
      tg.hrpt(1601, 0x8765, frame, 1)
    


###############################################################################

def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

###############################################################################

def print_data(data):
    for i in xrange(len(data)):
        if (i>0 and i%10==0):
            print 
#        print ('%8d '%data[i]),
        print ('%8x '%i32toul(data[i])),
    print

###############################################################################

def print_raw(data):
    for d in data:
        if ( d & 0xffff0000L):
          print ('\n0x%08x '%d),
        else :
          print ('0x%04x '%d),
    print

###############################################################################

#######################################
#
# main
#
#######################################


hm = histomem('hm04.psi.ch','spy','007')
hm.storerawdata(1)



test_hrpt()

sleep(1.5)
hm.stop()

data = hm.get()
print len(data)
#write_data(data,'data.txt')
print_data(data)

print "-------------------------------------------"
data = hm.getrawdata()
print len(data)
#write_data(data,'data.txt')
#print_raw(data)
#print data
#print data[0:100]

