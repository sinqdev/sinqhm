from nexus import *
from pylab import *
from numpy import *


def srange(s):
    if type(s) == str:
       for e in s.split(','):
         r = e.split('..')
         if len(r) == 2:
           r = map(int,r)
           step = 1 if (r[1] > r[0]) else -1
           for i in xrange(r[0],r[1]+step,step):
             yield i
         else:
           yield int(e)
    else: 
      if type(s) == int:
        yield(s)
      else:
        for i in s:
          yield i


def getdata(filename, entry):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,entry)
    status, data = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(data)

def get_hrpt_data(filename):
    status, nf = NXopen(filename,NXACC_READ)
    status = NXopenpath(nf,'/entry1/data1/counts')
    status, dy = NXgetdata(nf)
    status = NXopenpath(nf,'/entry1/data1/two_theta')
    status, dx = NXgetdata(nf)
    NXclosedata(nf)
    NXclosegroup(nf)
    NXclose(nf)
    return array(dx),array(dy)

#O:\psi.ch\project\sinqdata\2006\hrpt\013\hrpt2006n013028.hdf
#hrpt2006n018814.hdf

theta,data = get_hrpt_data('hrpt2006n018814.hdf')


#plot(theta,data)
plot(data)

show()
