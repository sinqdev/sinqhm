from array import *
from time import *
#import struct
import urllib2
import socket
import sys


def printf(format,*args): sys.stdout.write(format % args)

def itoi32(val):
  val &= 0xffffffffL
  if val >= 2147483648:
    return int(val-4294967296)
  else:
    return int(val)

def i32toul(val):
  val &= 0xffffffffL
  if val <0 :
    return (val+4294967296)
  else:
    return val
    
def status_cmp(sts,str):
    return (sts[0:len(str)] == str)



class Sock(socket.socket):
    def __init__(self,host,port):
        socket.socket.__init__(self,socket.AF_INET, socket.SOCK_STREAM)
        self.orig_send,self.send = self.send, self.send_all
        self.connect((host, port))

    def send_all(self,data):
        to_send = len(data)
        has_been_sent = 0;
        while (has_been_sent<to_send):
          has_been_sent += self.orig_send(data[has_been_sent:])
        return has_been_sent

class histomem:
    def __init__(self,histoname,user="spy",passwd="007"):
        self.hm_name = histoname
        pwmgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        auth_handler = urllib2.HTTPBasicAuthHandler(pwmgr)
        auth_handler.add_password(None,self.hm_name, user, passwd)
        opener = urllib2.build_opener(auth_handler)
        urllib2.install_opener(opener)

    def start(self):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/startdaq.egi')
        status = request.read()
        request.close()
        if not status_cmp(status,'OK: DAQ Started'):
            raise "ERROR starting DAQ:"+status

    def stop(self):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/stopdaq.egi')
        status = request.read()
        request.close()
        if not status_cmp(status,'OK: DAQ Stopped'):
            raise "ERROR stopping DAQ:"+status

    def do_error(self):
        try:
          request = urllib2.urlopen('http://'+self.hm_name+'/error/unknown.html')
          status = request.read()
          request.close()
        except:
          print "error"
          
    def configure(self,filename):
        file = open(filename)
        data = file.read()
        file.close()
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/configure.egi',data)
        status = request.read()
        request.close()
        if not status_cmp(status,'OK: Histogram Memory successfully configured'):
            raise "ERROR configuring DAQ:"+status

    def textstatus(self):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/textstatus.egi')
        status = request.read()
        request.close()
        return status

    def status_dict(self):
        dict = {}
        text=self.textstatus()
        for row in text.split('\n'):
          fields=row.split(':')
          if len(fields)==2:
            dict[fields[0].strip().lower()]=fields[1].strip()
        return dict

    def storerawdata(self,val):
    	if val>0:
    	    vstr='on'
    	else:
    	    vstr='off'
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/rawapply.egi?cmd=1&storeraw='+vstr)
        status = request.read()
        request.close()
        if not status_cmp(status,'OK: rawdata = '+vstr):
            raise "ERROR storerawdata:"+status

    def get(self,bank=0):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/readhmdata.egi?bank='+str(bank))
        bin = request.read()
        request.close()
        a = array("i",bin)
        a.byteswap()
#        a = array("I")
#        for i in xrange(len(bin)//4):
#           a.append(struct.unpack(">I",bin[i*4:i*4+4])[0])
        return a

    def get_binary(self,bank=0):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/readhmdata.egi?bank='+str(bank))
        bin = request.read()
        request.close()
        return bin
          
    def getrawdata(self):
        request = urllib2.urlopen('http://'+self.hm_name+'/admin/readrawdata.egi')
        bin = request.read()
        request.close()
        if status_cmp(bin,'ERROR:'):
            raise "ERROR getrawdata:"+bin
        a = array("I",bin)
        a.byteswap()
#        a = array("I")
#        for i in xrange(len(bin)//4):
#            a.append(itoi32(struct.unpack(">I",bin[i*4:i*4+4])[0]))
        return a


class testgen:
    def __init__(self,tgname,port):
        self.socket = Sock(tgname, port)

    def __del__(self):
        self.socket.close()

    def value(self, val):
        string = " 0x%08x\n" % i32toul(val)
        self.socket.send(string)
        
        
    def focus_2d(self, tube, pos, timestamp=0, sync=0x2):
        header  = 0x80000000L | ( timestamp & 0x0fffff)
        header |= ( ((tube%10)+1) << 24)
        header |= ( (sync & 0xf) << 20)        
        data = ( 1 << ( (tube//10) + 8 )) & 0xfe00
        data |= (pos & 0x1ff)
        string = " 0x%08x 0x%04x\n"%(i32toul(header),data)
        self.socket.send(string)


    def amor_psd(self, x, y, timestamp=0, sync=0x6):
        header  = 0x32000000 | (timestamp & 0xfffff)
        header |= ( (sync & 0xf) << 20)        
        data_x = x & 0xffff
        data_y = y & 0xffff
        string = " 0x%08x 0x%04x 0x%04x\n"%(i32toul(header), data_x, data_y)
        self.socket.send(string)


    def amor_ed(self, ed, timestamp=0, sync=0x6):
        header  = 0x20000000 | ((ed % 10) << 24)
        header |= (timestamp & 0xfffff)
        header |= ( (sync & 0xf) << 20)        
        data_ed = ed & 0xf
        string = " 0x%08x 0x%04x\n"%(i32toul(header),data_ed)
        self.socket.send(string)
       
        
    def sans2(self, x, y, sync=0x6):
        header  = 0x12000000 | (x & 0x1ff)
        header |= ( (sync & 0xf) << 20)        
        data_kv = 0x8000 | (y & 0x01ff) 
        data_kh = 0x8000 | (y & 0x01ff)
        string = " 0x%08x 0x%04x 0x%04x\n"%(i32toul(header), data_kv, data_kh)
#        header  = 0x10000000 | ( (y & 0x1ff) << 9) | (x & 0x1ff)
#        header |= ( (sync & 0xf) << 20)        
#        string = " 0x%08x\n"%(i32toul(header))
        self.socket.send(string)     


    def sans_gummi(self, timestamp=0, m1=0, m2=0, m3=0):
        header  = 0x14000000 | (timestamp & 0xfffff)
        data_1 = (m1>>16) & 0xffff
        data_2 =  m1      & 0xffff
        data_3 = (m2>>16) & 0xffff
        data_4 =  m2      & 0xffff
        data_5 = (m3>>16) & 0xffff
        data_6 =  m3      & 0xffff
        string = " 0x%08x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x\n"%(i32toul(header), data_1, data_2, data_3, data_4, data_5, data_6)
        self.socket.send(string)


    def sans_tof(self, n, timestamp=0, sync=0x0):
        header  = 0x01000000 
        header |= (timestamp & 0xfffff)
        header |= ( (sync & 0xf) << 20)        
        data = n & 0xffff
        string = " 0x%08x 0x%04x\n"%(i32toul(header),data)
        self.socket.send(string)


    def dig(self, n, sync=0x0):
        header  = 0x11000000 
        header |= ( (sync & 0xf) << 20)        
        data = n & 0xffff
        string = " 0x%08x 0x%04x\n"%(i32toul(header),data)
        self.socket.send(string)


    def hrpt(self, addr, count, frame, error = 1, sync=0x0):
        header  = 0x13000000 
        header |= ( (sync & 0xf) << 20)        
        data_1 = ((addr<<1) | (error&0x01)) & 0xffff
        data_2 =  count & 0xffff
        data_3 =  frame & 0xffff
        string = " 0x%08x 0x%04x 0x%04x 0x%04x\n"%(i32toul(header),data_1 ,data_2 ,data_3)
        self.socket.send(string)

        