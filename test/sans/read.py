import sys
sys.path.append('../python')
from histomem import *

###############################################################################

def test_dig():
  
    hm.stop()
    hm.configure('sans_dig.xml')
    hm.start()
  
    tg = testgen('hm01.psi.ch',5555)
    for bin in xrange(16384):
      for i in xrange(bin%10 + 1):
        tg.dig(bin)
    
    
def test_tof():
  
    hm.stop()
    hm.configure('sans_tof.xml')
    hm.start()
  
    tg = testgen('hm01.psi.ch',5555)
    
    for timestamp in xrange(10):
      tg.sans_tof(0,10)

#    tg.sans_gummi(1, 0x1, 0x123456, 0xaffeba)     
#    tg.sans_gummi(1, 0x1, 0xfffff, 0xfffff)
#    for timestamp in xrange(3):
#    for pos in xrange(512):
#    for i in xrange(tube+1):
#    sans_gummi(self, timestamp=0, m1=0, m2=0, m3=0):
    for timestamp in xrange(1,4):    
        tg.sans_gummi(timestamp, 0xaaaa00, 0, 0)
    for timestamp in xrange(2,5):
        tg.sans_gummi(timestamp, 0, 0xbbbb00, 0)
    for timestamp in xrange(3,6):
        tg.sans_gummi(timestamp, 0, 0, 0xcccc00)
    
    for i in xrange(0x21):  
      for timestamp in xrange(0,10):
          tg.sans_gummi(timestamp, 1, 1, 1)

    for j in xrange(0x13):  
      for i in xrange(0x13):  
        for timestamp in xrange(0,10):
          tg.sans_tof( 128*128+j, timestamp)

###############################################################################

def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

###############################################################################

def print_data(data):
    for i in xrange(len(data)):
        if (i>0 and i%10==0):
            print 
#        print ('%8d '%data[i]),
        print ('%8x '%data[i]),
    print

###############################################################################

def print_raw(data):
    for d in data:
        if ( d & 0xffff0000L):
          print ('\n0x%08x '%d),
        else :
          print ('0x%04x '%d),
    print

###############################################################################

#######################################
#
# main
#
#######################################


hm = histomem('sanshm.psi.ch','spy','007')
hm.storerawdata(1)


#test_dig()

#test_tof()

#sleep(0.5)
#hm.stop()

data = hm.get()
print len(data)
#write_data(data,'data.txt')
print_data(data)

##print "-------------------------------------------"
#data = hm.getrawdata()
#print len(data)
#write_data(data,'data.txt')
#print_raw(data)
#print data
#print data[0:100]

