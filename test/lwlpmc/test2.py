#!/usr/bin/python
import sys
sys.path.append('../python')
from histomem import *
import array


warncount=0    

def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

#######################################

def test(send_data):
    global warncount
    hm.start()

    for i in send_data:
        tg.value(i)

    sleep(1.0)
    hm.stop()

    rec_data = hm.getrawdata()
    errcount=0
    if len(send_data) != len(rec_data):
        print 'WARNING: send %d values, received %d'%(len(send_data),len(rec_data))
        sleep(1.0)
        warncount +=1
        
    l = min(len(send_data),len(rec_data))

    for i in xrange(l):
        if i32toul(send_data[i]) != i32toul(rec_data[i]):
          errcount += 1
          if errcount < 10:
            print 'Error: [%d] send: 0x%08x  received: 0x%08x' % (i,(i32toul(send_data[i])),(i32toul(rec_data[i])))
#            print 'Error: [%d] send: 0x%08x  received: 0x%08x' % (i,((send_data[i])),((rec_data[i])))


    return errcount




#######################################
#
# main
#
#######################################

def test_pattern():
  send_data = array.array("l")
  for j in xrange(100):
    send_data.append(0x10000000)
    for i in xrange(256):
      send_data.append(i)

    for i in xrange(32):
      send_data.append(itoi32(~(1l << i)))
      send_data.append(itoi32(1 << i))
  
      send_data.append(itoi32(0x55555555))
      send_data.append(itoi32(0xaaaaaaaa))
      send_data.append(itoi32(0xffffffff))
  return send_data

#define REG_FIFO_STATUS   5

tg = testgen('hm01.psi.ch',6666)
hm = histomem(sys.argv[1],'spy','007')

send_data = test_pattern()

hm.stop()
hm.storerawdata(1)

for i in send_data:
    tg.value(i)
sleep(1)

turns = 0
passed = 0
failed = 0

while 1:
    turns += 1
    status = test(send_data)
    if status >0:
        failed += 1
    else:
        passed += 1
    print 'turn:%6d   passed:%6d  failed: %6d  warnings %6d'%(turns,passed,failed,warncount)    
    sys.stdout.flush()
