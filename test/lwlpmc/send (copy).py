import sys
sys.path.append('../python')

from histomem import *
import array


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()

#######################################

def test(send_data):

    for i in send_data:
        tg.value(i)

    return 0




#######################################
#
# main
#
#######################################

def test_pattern():
  send_data = array.array("l")
  for j in xrange(100):
    send_data.append(0x10000000)
    for i in xrange(256):
      send_data.append(i)

    for i in xrange(32):
      send_data.append(i32toul(~(1 << i)))
      send_data.append(i32toul(1 << i))
  
      send_data.append(i32toul(0x55555555))
      send_data.append(i32toul(0xaaaaaaaa))
      send_data.append(i32toul(0xffffffff))
  return send_data



tg = testgen('hm01.psi.ch',5555)
send_data = test_pattern()



turns = 0
passed = 0
failed = 0

while 1:
    turns += 1
    status = test(send_data)
    print 'send:%6d  '%(turns)    
#    sleep(0.5)
    sys.stdout.flush()
