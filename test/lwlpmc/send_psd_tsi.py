#!/usr/bin/python

import sys
sys.path.append('../python')
from histomem import *
import array

#######################################

def test(send_data):

    for i in send_data:
        tg.value(i)

    return 0

#######################################

def psd_tsi_data(timestamp=0):
  send_data = array.array("l")
  send_data.append(i32toul(0x0E000000 + timestamp))
  send_data.append(i32toul(0x0000))  #
  send_data.append(i32toul(0x0064)) #  hits
  send_data.append(i32toul(0x0001))  #  flashes
  return send_data

#######################################
#
# main
#
#######################################

tg = testgen('hm01.psi.ch',5555)
send_data = psd_tsi_data()

turns = 0
passed = 0
failed = 0

while 1:
    turns += 1
    status = test(send_data)
    print 'send:%6d  '%(turns)    
    sleep(0.1)
    sys.stdout.flush()
