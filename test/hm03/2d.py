#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import struct


import sys
sys.path.append('../python')
from histomem import *

def printf(format,*args): sys.stdout.write(format % args)

__author__ = "Marc 'BlackJack' Rintsch"
__version__ = '0.1.0'
__date__ = '$Date: 2006-04-25 11:06:18 +0200 (Tue, 25 Apr 2006) $'
__revision__ = '$Rev: 849 $'


class BMPError(Exception):
    """Error for problems with BMP file."""
    pass


class BMP(object):

    HEADER_FMT = '< 2s i 4x i iii hh ii'
    HEADER_SIZE = struct.calcsize(HEADER_FMT)
   
    def __init__(self, fileobj):
        """Create a `BMP` object from the content of `fileobj`.
       
        Raises `BMPError` if the BMP in `fileobj` is not an uncompressed
        24 bits per pixel BMP.
        """
        (self.signature,
         self.file_size,
         self.data_offset,
         self.info_size,
         self.width,
         self.height,
         self.planes,
         self.bit_count,
         self.compression,
         self.data_size
        ) = struct.unpack(self.HEADER_FMT, fileobj.read(self.HEADER_SIZE))
       
        if self.signature != 'BM':
            raise BMPError('not a BMP')
        if self.planes != 1:
            raise BMPError('image has more than one plane')
        if self.bit_count != 24:
            raise BMPError('not a 24 bpp image')
        if self.compression != 0:
            raise BMPError('compressed image')
       
        self.remaining_header = fileobj.read(  self.data_offset
                                             - self.HEADER_SIZE)
        self.data = fileobj.read()
       
        self.bytes_per_row = None
        self._calculate_bytes_per_row()

    def _calculate_bytes_per_row(self):
        """Calculate the bytes per row.
       
        Row data in BMP images is padded to 32 bit boundaries.
        """
        self.bytes_per_row = ((self.width * self.bit_count - 1) // 32) * 4 + 4
   

   
    def dump(self,tg):
        """Convert image to 16 bits per pixel."""
        assert self.bit_count in (24, 16)
        if self.bit_count == 16:
            return
        #
        # Convert image data.
        #
        y=0;
        x=0;
#        for rowdata in self.iter_rows():
        for i in xrange(self.height-1,-1,-1):
            offset = i * self.bytes_per_row
            rowdata = self.data[offset:offset+self.bytes_per_row]
        
            x=0
#            for red, green, blue in self._iter_pixels(rowdata):
            for i in xrange(self.width):
                red, green, blue = map(ord, rowdata[i*3:i*3+3])
            
                if (red+green+blue) < 500 :
                  tg.sans2(x,y)
#                  printf("*")
#                else:
#                  printf(" ")

                x+=1
            y+=1
#            printf("\n")





def main():
    tg = testgen('lnse14.psi.ch',5555)
    
    bmp_file = open('test512.dat', 'rb')
    bmp = BMP(bmp_file)
    bmp_file.close()
    bmp.dump(tg)



        
        
if __name__ == '__main__':
    main()