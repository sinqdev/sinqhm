#!/usr/bin/env python
from __future__ import division
from pylab import *
from histomem import *
import array


def write_data(data,filename):
    file = open(filename,'w')
    for i in xrange(len(data)):
        if (i>0 and i%512==0):
            file.write('\n')
        file.write('%6d '%data[i])
    file.close()
    
    
    
hm = histomem('hm03.psi.ch','spy','007')
S=160
Z = arange(S)

#hm.stop()
#hm.configure('tof.xml')
#hm.start()
#write_data(D,'data.dat')

D = hm.get()
for x in xrange(S):
     Z[x]=D[x]

plot(Z)
show()
    
