SinqHM
==================

This is the SINQ histogram memory code. The main part is in the 
develop directory. The source codes for the tools upon which SinqHM 
is built is also included.

What is needed in addition to build SinqHM for the raeltime linux running 
on the MEN-A12 baords in the SINQ hall are two additional archives which 
live in the archivpsi.psi.ch archival system. 

One is /eop/elinos with the linux realtime system.

The other is LINUX_RTAI_V2 which is the linux instantiation used for 
the MEN A12. This actually has to be unpacked in the root of this 
directory hierarchy. 

But this is ony needed when rebuilding the whole realtime system. Which is 
very unlikely as it is churning away quite nicely since 10 years.
  