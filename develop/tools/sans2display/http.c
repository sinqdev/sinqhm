
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <ghttp.h>




/* This is the http request object */
ghttp_request *request = NULL;

int init_request(void)
{
  int status;

  if (request) ghttp_request_destroy(request);

  /* Allocate a new empty request object */
  request = ghttp_request_new();
  if(!request) return -1;

  /* Set the URI for the request object */
  status = ghttp_set_sync(request, ghttp_sync);
  if (status<0)  return -1;

  status = ghttp_set_uri(request, "http://lnse14/admin/textstatus.egi");
  if (status<0)  return -1;

  status = ghttp_set_authinfo(request,"spy", "007");
  if (status<0)  return -1;

  ghttp_set_header(request,"connection","keep-alive");

  return 0;
}

void close_request(void)
{
  ghttp_request_destroy(request);
  request=0;
}



int do_request(void)
{
  int try=2;
  int ok=0;
  int status;


  do
  {
        try--;
	if(!request)
	{
          if (init_request()<0)
	  {
	        printf("ERROR: init_request\n");
	        return -1;
	  }
	}
	else
	{
	        ghttp_clean(request);
	}

        status = ghttp_prepare(request);

	if (status>=0)
	{
		/* Process the request */
		status = ghttp_process(request);
		if (status>=0) ok=1;
	}

	if (!ok) close_request();
  }
  while(try && !ok);

  if (!ok) return -1;

  return 0;

}

int isnum(char c)
{
  return (((c>='0') && (c<='9')) || (c=='+') || (c=='-') );
}

int isspace(char c)
{
  return ((c==' ') || (c=='\t'));
}
#define MAX_DST 20




int getint(char* str, int n, const char* key, int *valp)
{
  int l,conv,dst;
  int i=0;
  int found=0;
  char dst_str[MAX_DST+1];

  l = strlen(key);

  if (l<=0) return 0;
  while((i<=(n-l)) && !found)
  {
    if (strncasecmp(&str[i], key,l) == 0)
    {
      found = 1;
    }
    else
    {
      i++;
    }
  }

  if (found)
  {
    // locate at end of key
    i+=l;

    // skip spaces
    while( (i<n) && isspace(str[i])) i++;

    dst=0;
    while( (i<n) && (dst<MAX_DST) && isnum(str[i]))
    {
      dst_str[dst]=str[i];
      dst++;
      i++;
    }
    dst_str[dst]=0;
    conv = sscanf(dst_str,"%d",valp);
    if (conv>0) return 1;

  }

  return 0;
}


#if 0
int main(void)
{
        int i;
	int status;
	int val;
	char *msg;
	int msg_len;

	for(i=0;i<13;i++)
	{
		status = do_request();
		if (status>=0)
		{
		  msg = ghttp_get_body(request);
		  msg_len = ghttp_get_body_len(request);
//		  fwrite(msg,msg_len , 1, stdout);
		  status = getint(msg, msg_len, "LWL-Status:", &val);
		  if(status>0)  printf("VAL: %d\n",val); else  printf("Error\n");
		}
		else
		{
		  printf("Error\n");
		}
	}

	close_request();

}
#endif
