#ifndef DIGITALCLOCK_H
#define DIGITALCLOCK_H

//#include <QLCDNumber>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qptrlist.h>
#include <qlayout.h>
#include <qgrid.h>





void init_avg(void);


class MyLCD
{

public:
        MyLCD(QWidget *parent, char *label_text, QString tagstr, QGridLayout *grid, int gridrow);
        QLCDNumber *number;
	QLabel     *label;
        QString    tag;
	void display(QString text);
private:
};


class display : public QWidget
{
	Q_OBJECT

public:
	display(QWidget *parent = 0);
        QPtrList<MyLCD> list;

private slots:
	void showTime();
};





#endif
