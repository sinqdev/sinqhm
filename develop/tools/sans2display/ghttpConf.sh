#
# Configuration file for using the ghttp library in GNOME applications.
#
GHTTP_LIBDIR="-L/afs/psi.ch/project/sinq/sl-linux/stow/libghttp-1.0.9/lib"
GHTTP_LIBS="-lghttp"
GHTTP_INCLUDEDIR="-I/afs/psi.ch/project/sinq/sl-linux/stow/libghttp-1.0.9/include"
MODULE_VERSION="ghttp-1.0.9"

