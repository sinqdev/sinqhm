/****************************************************************************
** display meta object code from reading C++ file 'display.h'
**
** Created: Fri Mar 2 13:57:57 2007
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.1.2   edited Feb 24 09:29 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "display.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.1.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *display::className() const
{
    return "display";
}

QMetaObject *display::metaObj = 0;
static QMetaObjectCleanUp cleanUp_display( "display", &display::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString display::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "display", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString display::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "display", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* display::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUMethod slot_0 = {"showTime", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "showTime()", &slot_0, QMetaData::Private }
    };
    metaObj = QMetaObject::new_metaobject(
	"display", parentObject,
	slot_tbl, 1,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_display.setMetaObject( metaObj );
    return metaObj;
}

void* display::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "display" ) )
	return this;
    return QWidget::qt_cast( clname );
}

bool display::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: showTime(); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool display::qt_emit( int _id, QUObject* _o )
{
    return QWidget::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool display::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool display::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
