/*
 *	File selection menu
 *	Copyright
 *		(C) 1992 Joseph H. Allen
 *
 *	This file is part of JOE (Joe's Own Editor)
 */
#ifndef _JOE_TAB_H
#define _JOE_TAB_H 1

#include "config.h"
#include "types.h"

int cmplt PARAMS((BW *bw));

#endif
