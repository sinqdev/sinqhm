/*
 *	Math
 *	Copyright
 *		(C) 1992 Joseph H. Allen
 *
 *	This file is part of JOE (Joe's Own Editor)
 */
#ifndef _JOE_UMATH_H
#define _JOE_UMATH_H 1

#include "config.h"
#include "types.h"

extern unsigned char *merr;
double calc(BW *bw, unsigned char *s);
int umath(BW *bw);

extern B *mathhist;

#endif
