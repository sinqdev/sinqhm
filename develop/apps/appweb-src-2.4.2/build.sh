export PATH=/opt/elinos-4.0/bin:/opt/elinos-4.0/cdk/ppc/60x/glibc-2.3.4/bin:$PATH

export AR=ppc_60x-ar
export AS=ppc_60x-as
export CC=ppc_60x-gcc
export LD=ppc_60x-gcc
export GDB=ppc_60x-gdb
export STRIP=ppc_60x-strip

#export CXX=ppc_60x-g++
#export LDXX=ppc_60x-g++
export CXX=
export LDXX=

#export BUILD_CC=gcc
#export BUILD_LD=ld
#export BUILD_MT=mt
export BUILD_CC=
export BUILD_LD=
export BUILD_MT=

export BLD_FEATURE_STATIC_LINK_LIBC=1

./configure --host=powerpc-linux --type=RELEASE --disable-shared --enable-static --disable-stdc++ --disable-multi-thread  --disable-samples    


# some config options, to see all use ./configure --help
#
#  --type=BUILD             Set the build type (DEBUG|RELEASE)
#  --enable-multi-thread    Build AppWeb multi-threaded.
#  --disable-multi-thread   Build AppWeb single threaded.
#
#  --enable-keep-alive      Build with HTTP Keep-Alive support.
#  --disable-keep-alive     Do not include HTTP Keep-Alive handling.
#
#  --enable-samples         Build the samples
#  --disable-samples        Do not build the samples
#
#  --enable-shared          Build an appweb shared library and program. [default]
#  --disable-shared         Do not build an appweb shared library and program.
#
#  --enable-shared-libc     Link with the shared versions of libc.
#  --disable-shared-libc    Link with the static versions of libc.
#


make

cp lib/powerpc-unknown-linux/libappwebStatic.a  ../../lib/ppc/libappWebStatic-2.4.2.a
rm -f ../../include/appWeb-2.4.2/appWeb.h
rm -f ../../include/appWeb-2.4.2/buildConfig.h
find . -name "*.h" -exec cp {} ../../include/appWeb-2.4.2 \;
ln -s  ../../include/appWeb-2.4.2/appweb.h ../../include/appWeb-2.4.2/appWeb.h

