#	
#	config.sh -- Build configuration file.
#	
#	WARNING: DO NOT EDIT. This file is generated by configure.
#	
#	If you wish to modify the defaults, then edit conf/config.defaults.* and
#	then run "configure --reset".
#	
################################################################################

#
#	Key Product Settigns
#
BLD_PRODUCT="appWeb"
BLD_NAME="Mbedthis AppWeb"
BLD_VERSION="2.0.4"
BLD_NUMBER="1"
BLD_TYPE="RELEASE"
BLD_DEFAULTS="normal"
BLD_PACKAGES="matrixssl openssl php5"
BLD_APPWEB_CONFIG="normal.conf"
BLD_APPWEB=1

#
#	Other Product Settings
#
BLD_COMPANY="Mbedthis"
BLD_DEBUG=0
BLD_DIRS="bootstrap include obj bin mpr ejs esp http doc appWeb appWebSamples images"
BLD_HTTP_PORT=7777
BLD_LIB_VERSION="1.0.0"
BLD_SSL_PORT=4443
BLD_CLEAN_INSTALL="0"
BLD_LICENSE="gpl"
BLD_COMMERCIAL="0"

#
#	Host and Build System Settings.
#
BLD_HOST_SYSTEM="powerpc-unknown-linux-gnu"
BLD_BUILD_SYSTEM="i686-pc-linux-gnu"

#
# 	Host System Settings 
#
BLD_HOST_OS="LINUX"
BLD_HOST_CPU_ARCH=MPR_CPU_PPC
BLD_HOST_CPU="powerpc"
BLD_HOST_UNIX=1

#
# 	Build System Settings for Build Tools
#
BLD_BUILD_OS="LINUX"
BLD_BUILD_CPU_ARCH=MPR_CPU_IX86
BLD_BUILD_CPU=i686
BLD_BUILD_UNIX=1

#
#	System and Installation Directories
#
BLD_ROOT_PREFIX="/"
BLD_PREFIX="/etc/appWeb"
BLD_DOC_PREFIX="/usr/share/appWeb-2.0.4"
BLD_INC_PREFIX="/usr/include/appWeb"
BLD_LIB_PREFIX="/usr/lib"
BLD_SBIN_PREFIX="/usr/sbin"
BLD_SRC_PREFIX="/usr/src/appWeb-2.0.4"
BLD_WEB_PREFIX="/var/appWeb/web"

#
#	Feature Selection
#
BLD_FEATURE_ACCESS_LOG=1
BLD_FEATURE_ADMIN_MODULE=1
BLD_FEATURE_ASPNET_MODULE=0
BLD_FEATURE_ASSERT=1
BLD_FEATURE_AUTH_MODULE=1
BLD_FEATURE_C_API_MODULE=1
BLD_FEATURE_C_API_CLIENT=0
BLD_FEATURE_CGI_MODULE=1
BLD_FEATURE_COMPAT_MODULE=0
BLD_FEATURE_CONFIG_PARSE=1
BLD_FEATURE_CONFIG_SAVE=1
BLD_FEATURE_COOKIE=1
BLD_FEATURE_COPY_MODULE=1
BLD_FEATURE_DIGEST=1
BLD_FEATURE_DLL=1
BLD_FEATURE_EGI_MODULE=1
BLD_FEATURE_EJS=1
BLD_FEATURE_ESP_MODULE=1
BLD_FEATURE_ESP3_MODULE=
BLD_FEATURE_EVAL_PERIOD=30
BLD_FEATURE_FLOATING_POINT=1
BLD_FEATURE_IF_MODIFIED=1
BLD_FEATURE_INT64=1
BLD_FEATURE_KEEP_ALIVE=1
BLD_FEATURE_LEGACY_API=1
BLD_FEATURE_LIB_STDCPP=1
BLD_FEATURE_LICENSE=0
BLD_FEATURE_LOG=1
BLD_FEATURE_MULTITHREAD=1
BLD_FEATURE_MALLOC=0
BLD_FEATURE_MALLOC_STATS=0
BLD_FEATURE_MALLOC_LEAK=0
BLD_FEATURE_MALLOC_HOOK=0
BLD_FEATURE_NUM_TYPE=int
BLD_FEATURE_NUM_TYPE_ID=MPR_TYPE_INT
BLD_FEATURE_ROMFS=0
BLD_FEATURE_RUN_AS_SERVICE=1
BLD_FEATURE_SAFE_STRINGS=0
BLD_FEATURE_SAMPLES=0
BLD_FEATURE_SESSION=1
BLD_FEATURE_SHARED=1
BLD_FEATURE_SQUEEZE=0
BLD_FEATURE_SSL_MODULE=0
BLD_FEATURE_STATIC=1
BLD_FEATURE_STATIC_LINK_LIBC=0
BLD_FEATURE_TEST=0
BLD_FEATURE_UPLOAD_MODULE=1
BLD_FEATURE_XDB_MODULE=0

#
#	Builtin Modules
#
BLD_FEATURE_ADMIN_MODULE_BUILTIN=0
BLD_FEATURE_ASPNET_MODULE_BUILTIN=0
BLD_FEATURE_AUTH_MODULE_BUILTIN=0
BLD_FEATURE_C_API_MODULE_BUILTIN=0
BLD_FEATURE_CGI_MODULE_BUILTIN=0
BLD_FEATURE_COMPAT_MODULE_BUILTIN=0
BLD_FEATURE_COPY_MODULE_BUILTIN=0
BLD_FEATURE_EGI_MODULE_BUILTIN=0
BLD_FEATURE_ESP_MODULE_BUILTIN=0
BLD_FEATURE_ESP3_MODULE_BUILTIN=
BLD_FEATURE_SSL_MODULE_BUILTIN=0
BLD_FEATURE_UPLOAD_MODULE_BUILTIN=0
BLD_FEATURE_XDB_MODULE_BUILTIN=0

#
#	Loadable Modules
#
BLD_FEATURE_ADMIN_MODULE_LOADABLE=1
BLD_FEATURE_ASPNET_MODULE_LOADABLE=0
BLD_FEATURE_AUTH_MODULE_LOADABLE=1
BLD_FEATURE_C_API_MODULE_LOADABLE=1
BLD_FEATURE_CGI_MODULE_LOADABLE=1
BLD_FEATURE_COMPAT_MODULE_LOADABLE=0
BLD_FEATURE_COPY_MODULE_LOADABLE=1
BLD_FEATURE_EGI_MODULE_LOADABLE=1
BLD_FEATURE_ESP_MODULE_LOADABLE=1
BLD_FEATURE_ESP3_MODULE_LOADABLE=
BLD_FEATURE_SSL_MODULE_LOADABLE=0
BLD_FEATURE_UPLOAD_MODULE_LOADABLE=1
BLD_FEATURE_XDB_MODULE_LOADABLE=0

################################################################################
#
#	Programs for building tools to run on the build system (native)
#
BLD_AR_FOR_BUILD="ar"
BLD_CC_FOR_BUILD="cc"
BLD_CSC_FOR_BUILD=""
BLD_JAVAC_FOR_BUILD=""
BLD_LD_FOR_BUILD="ld"
BLD_RANLIB_FOR_BUILD=""
BLD_NM_FOR_BUILD="nm"

#
#	Native build flags 
#
BLD_CFLAGS_FOR_BUILD=""
BLD_IFLAGS_FOR_BUILD=""
BLD_LDFLAGS_FOR_BUILD=""

#
#	Native file extensions
#
BLD_ARCHIVE_FOR_BUILD=".a"
BLD_EXE_FOR_BUILD=""
BLD_OBJ_FOR_BUILD=".o"
BLD_PIOBJ_FOR_BUILD=".lo"
BLD_CLASS_FOR_BUILD=".class"
BLD_SHLIB_FOR_BUILD=""
BLD_SHOBJ_FOR_BUILD=".so"

#
#	Build programs for the host
#
BLD_AR_FOR_HOST="ppc_60x-ar"
BLD_CC_FOR_HOST="ppc_60x-gcc"
BLD_CSC_FOR_HOST="csc"
BLD_JAVAC_FOR_HOST="javac"
BLD_LD_FOR_HOST="ppc_60x-ld"
BLD_RANLIB_FOR_HOST="ppc_60x-ranlib"
BLD_NM_FOR_HOST="ppc_60x-ranlib"

#
#	Build flags for the host
#
BLD_CFLAGS_FOR_HOST=""
BLD_IFLAGS_FOR_HOST=""
BLD_LDFLAGS_FOR_HOST=""

#
#	File extensions for the host
#
BLD_ARCHIVE_FOR_HOST=".a"
BLD_EXE_FOR_HOST=""
BLD_OBJ_FOR_HOST=".o"
BLD_PIOBJ_FOR_HOST=".lo"
BLD_CLASS_FOR_HOST=".class"
BLD_SHLIB_FOR_HOST=""
BLD_SHOBJ_FOR_HOST=".so"

if [ "${BLD_NATIVE}" = 1 ]
then
	#
	#	Build programs for building tools to run natively on the build system
	#
	BLD_AR="ar"
	BLD_CC="cc"
	BLD_CSC=""
	BLD_JAVAC=""
	BLD_LD="ld"
	BLD_RANLIB=""
	BLD_NM="nm"

	#
	#	Build flags 
	#
	BLD_CFLAGS=""
	BLD_IFLAGS=""
	BLD_LDFLAGS=""

	#
	#	File extensions
	#
	BLD_ARCHIVE=.a
	BLD_EXE=
	BLD_OBJ=.o
	BLD_PIOBJ=.lo
	BLD_CLASS=.class
	BLD_SHLIB=
	BLD_SHOBJ=.so
	BLD_LIB=.so

else
	#
	#	Build programs on the host system
	#
	BLD_AR="ppc_60x-ar"
	BLD_CC="ppc_60x-gcc"
	BLD_CSC="csc"
	BLD_JAVAC="javac"
	BLD_LD="ppc_60x-ld"
	BLD_RANLIB="ppc_60x-ranlib"
	BLD_NM="ppc_60x-ranlib"

	#
	#	Build flags for the host
	#
	BLD_CFLAGS=""
	BLD_IFLAGS=""
	BLD_LDFLAGS=""

	#
	#	File extensions
	#
	BLD_ARCHIVE=.a
	BLD_EXE=
	BLD_OBJ=.o
	BLD_PIOBJ=.lo
	BLD_CLASS=.class
	BLD_SHLIB=
	BLD_SHOBJ=.so
	BLD_LIB=.so
fi


#
#	Ensure we run our build tools by preference
#
export PATH="${BLD_TOP}/bin:${BLD_TOP}/bin/${BLD_TYPE}:${PATH}"

BLD_TOOLS_DIR="${BLD_TOP}/bin"
BLD_BIN_DIR="${BLD_TOP}/bin"
BLD_INC_DIR="/usr/include/${BLD_PRODUCT}"
BLD_EXP_OBJ_DIR="${BLD_TOP}/obj"

#
#	MATRIXSSL
#
BLD_FEATURE_MATRIXSSL_MODULE=0
BLD_FEATURE_MATRIXSSL_MODULE_LOADABLE=0
BLD_FEATURE_MATRIXSSL_MODULE_BUILTIN=0
BLD_MATRIXSSL_DIR=''
BLD_MATRIXSSL_LIBS=''
BLD_MATRIXSSL_IFLAGS=''
BLD_MATRIXSSL_CFLAGS=''
BLD_MATRIXSSL_LDFLAGS=''

#
#	OPENSSL
#
BLD_FEATURE_OPENSSL_MODULE=0
BLD_FEATURE_OPENSSL_MODULE_LOADABLE=0
BLD_FEATURE_OPENSSL_MODULE_BUILTIN=0
BLD_OPENSSL_DIR=''
BLD_OPENSSL_LIBS=''
BLD_OPENSSL_IFLAGS=''
BLD_OPENSSL_CFLAGS=''
BLD_OPENSSL_LDFLAGS=''

#
#	PHP5
#
BLD_FEATURE_PHP5_MODULE=0
BLD_FEATURE_PHP5_MODULE_LOADABLE=0
BLD_FEATURE_PHP5_MODULE_BUILTIN=0
BLD_PHP5_DIR=''
BLD_PHP5_LIBS=''
BLD_PHP5_IFLAGS=''
BLD_PHP5_CFLAGS=''
BLD_PHP5_LDFLAGS=''

