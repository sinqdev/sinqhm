#
#	Makefile -- Top level Makefile. 
#
#	Copyright (c) Mbedthis Software LLC, 2003-2005. All Rights Reserved.
#
#	Standard Make targets supported are:
#	
#		make 						# Does a "make compile"
#		make clean					# Removes generated objects
#		make compile				# Compiles the source
#		make depend					# Generates the make dependencies
#		make test 					# Runs unit tests
#		make package				# Creates an installable package
#		make projects				# Creates Visual Studio project files
#
#	Additional targets for this makefile:
#
#		make build					# Do a clean build

#	Installation targets. Use "make ROOT_DIR=myDir" to do a custom local
#	install:
#		make install				# Call install-binary
#		make install-release		# Install release files (README.TXT etc)
#		make install-binary			# Install binary files
#		make install-dev			# Install development libraries and headers
#		make install-doc			# Install documentation
#		make install-samples		# Install samples
#		make install-src			# Install source code
#		make install-all			# Install everything except source code
#
#	To remove, use make uninstall-ITEM, where ITEM is a component above.

BLD_TOP		:= .
BUILD_CHECK	:= buildCheck
FIRST		:= first

#
#	Conditionally read in the Make rules and templates. The config.make file
#	will not exist if configure has not been run.
#

ifeq ($(shell [ -f config.make ] && echo found),found)

include		config.make

PRE_DIRS	= $(BLD_DIRS)

include		make.rules

#
#	Read the per O/S make rules. Copy from the master in conf if required.
#
Junk	:= ($(shell [ ! -f make.os \
		-o conf/make.os.$(BLD_HOST_OS) -nt make.os ] && \
		echo "  cp conf/make.os.$(BLD_HOST_OS) make.os" ; \
		cp conf/make.os.$(BLD_HOST_OS) make.os ; \
		chmod 644 make.os))
ifeq ($(BLD_NATIVE),1)
	include $(BLD_TOP)/conf/make.os.$(BLD_BUILD_OS)
else
	include $(BLD_TOP)/make.os
endif

else
	#
	#	Configure has not yet been run
	#
all clean compile depend package test projects: 
	@if [ "$(UCLINUX_BUILD_USER)" = 1 ] ; \
	then \
		rm -f conf/config.defaults ; \
		BLD_PRODUCT=appWeb ; \
		echo "    ln -s $$BLD_PRODUCT/uclinux.defaults conf/config.defaults" ;\
		ln -s $$BLD_PRODUCT/uclinux.defaults conf/config.defaults ; \
		if [ ! -f conf/config.cache -o ../../.config -nt config.make ] ; \
		then \
			if [ "$$CONFIG_USER_APPWEB_DYNAMIC" = "y" ] ; \
			then \
				SW="$$SW --enable-modules \
					--with-auth=loadable \
					--with-cgi=loadable \
					--with-copy=loadable \
					--with-esp=loadable \
					--with-copy=loadable" ; \
				moduleType=loadable ; \
			else \
				SW="$$SW --disable-modules" ; \
				moduleType=builtin ; \
			fi ; \
			if [ "$$CONFIG_USER_APPWEB_MULTITHREAD" = "y" ] ; \
			then \
				SW="$$SW --enable-multi-thread" ; \
			else SW="$$SW --disable-multi-thread" ; \
			fi ; \
			if [ "$$CONFIG_USER_APPWEB_SSL" = "y" ] ; \
			then \
				SW="$$SW --with-openssl=$$moduleType" ; \
				SW="$$SW --with-openssl-dir=../../lib/libssl" ; \
			elif [ "$$CONFIG_USER_APPWEB_MATRIXSSL" = "y" ] ; \
			then \
				SW="$$SW --with-matrixssl=$$moduleType" ; \
				SW="$$SW --with-matrixssl-dir=../../lib/matrixssl" ; \
				SW="$$SW --with-matrixssl-iflags=-../../lib/libmatrixssl" ; \
			else SW="$$SW --without-ssl" ; \
			fi ; \
			if [ "$$CONFIG_USER_APPWEB_CGI" = "y" ] ; \
			then SW="$$SW --with-cgi=$$moduleType" ; \
			else SW="$$SW --without-cgi" ; \
			fi ; \
			echo "    ./configure $$SW " ; \
			./configure $$SW; \
			echo "  $(MAKE) -S $(MAKEF)" ; \
			$(MAKE) -S $(MAKEF) ; \
		fi ; \
	else \
		echo "Must run configure first" ; \
		exit 2 ; \
	fi
endif

compileFirst: make.os mpr/make.dep

#
#	Check for the existing of the dynamic dependancy files (make.dep)
#
.PRECIOUS	: mpr/make.dep

mpr/make.dep: \
		$(BLD_TOOLS_DIR)/genDepend$(BLD_EXE_FOR_BUILD) \
		$(BLD_TOOLS_DIR)/bldout$(BLD_EXE_FOR_BUILD) config.make
	@echo -e "\n  # Making dynamic makefile dependancies\n"
	@echo "  bin/makedep"
	@bin/makedep
	@echo "  $(MAKE) depend"
	@$(MAKE) --no-print-directory depend

#
#	Build the dependancy generation program
#
$(BLD_TOOLS_DIR)/genDepend$(BLD_EXE_FOR_BUILD):
	@echo "  cd bootstrap/genDepend"
	@cd bootstrap/genDepend ; \
	echo "  $(MAKE)" ; \
	$(MAKE) --no-print-directory

#
#	Build the build output formatting program
#
$(BLD_TOOLS_DIR)/bldout$(BLD_EXE_FOR_BUILD):
	@echo "  cd bootstrap/bldout"
	@cd bootstrap/bldout ; \
	echo "  $(MAKE)" ; \
	$(MAKE) --no-print-directory

#
#	Copy the per O/S make rules in place. We use ./make.os
#
make.os: conf/make.os.${BLD_HOST_OS}
	@rm -f make.os
	@echo "    cp conf/make.os.${BLD_HOST_OS} make.os"
	@cp conf/make.os.${BLD_HOST_OS} make.os
	@chmod 644 make.os

config.sh:

config.h:

config.make:

#
#	uClinux target to package the build into the ROM file system
#
romfs:
	@if [ "$(ROMFSDIR)" = "" ] ; \
	then \
		echo "ROMFSDIR is not defined" ; \
		exit 255 ; \
	fi
	@ROOT_DIR="$(ROMFSDIR)" SKIP_PERMS=1 TASK=Install \
		$(BLD_PRODUCT)/package/makeInstall binary

#
#	Installation targets
#
install: install-binary

#
#	Installs core AppWeb binary files
#
install-binary:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall binary

#
#	Installs license, readme and script files required for a packaged release.
#
install-release:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall release

#
#	Installs development libraries, code headers and test web files
#
install-dev:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall dev

#
#	Installs the product documentation
#
install-doc:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall doc

#
#	Installs the product samples
#
install-samples:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall samples

#
#	Installs the product source code including a copy of the doc and samples
#
install-src:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall src

#
#	Install everything except the source
#
install-all:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Install \
	$(BLD_PRODUCT)/package/makeInstall "binary dev release dev doc samples"

uninstall: uninstall-binary

uninstall-binary:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall binary

uninstall-release:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall release

uninstall-dev:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall dev

uninstall-doc:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall doc

uninstall-samples:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall samples

uninstall-src:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall src

uninstall-all:
	@ROOT_DIR="$(ROOT_DIR)" TRACE=$(TRACE) TASK=Remove \
	$(BLD_PRODUCT)/package/makeInstall "binary dev release dev doc samples"

#
#	Standard alias for test
#
check:
	echo "    $(MAKE) -S $(MAKEF) test" ; \
	$(MAKE) --no-print-directory -S $(MAKEF) test; \

#
#	Target for a new build. Used internally by Mbedthis.
#
build: 
	@(  \
		[ -x bin/incBuild -a -f mbedthis ] && bin/incBuild ; \
		. config.sh ; \
		echo -e "\n  #\n  # Building $(BLD_PRODUCT) $(BLD_VERSION)-$${BLD_NUMBER}, ${BLD_TYPE} Build\n  #" ; \
		echo -e "  $(MAKE) -S compile test\n" ; \
		$(MAKE) --no-print-directory -S compile test; \
		echo -e "\n  #\n  # Build Complete. Built $(BLD_PRODUCT) $(BLD_VERSION)-$${BLD_NUMBER}, ${BLD_TYPE} Build" ; \
		echo -e "  # Build log saved in make.log\n  #" ; \
	) 2>&1 | tee make.log
	@echo ""

#
#	Crude check if AppWeb has been removed
#
verifyClean:
	@echo /etc/$(BLD_PRODUCT)* /usr/lib/$(BLD_PRODUCT)* \
		/sbin/$(BLD_PRODUCT)* /usr/sbin/$(BLD_PRODUCT)* \
		/usr/src/$(BLD_PRODUCT)* /usr/share/$(BLD_PRODUCT)* \
		/var/$(BLD_PRODUCT)*

clobberExtra:
	@rm -f conf/config.cache conf/config.args config.make config.sh config.h 
	@find . -name Debug | egrep -v './bin/Debug|./obj/Debug' | \
		xargs -i rm -fr {}
	@find . -name Release | egrep -v './bin/Release|./obj/Release' | \
		xargs -i rm -fr {}

#
#	Patch copyright strings
#
patch:
	@copyrightDir=$(BLD_TOP)/copyrights/$(BLD_LICENSE) ; \
	( \
		find . -name '*.c' -print ; \
		find . -name '*.cpp' -print ; \
		find . -name '*.h' -print  ; \
	) | $(BLD_BIN_DIR)/incPatch -v -l $$copyrightDir

#
#	Linux gdb has trouble debugging dynamically loadable DLLs
#
configure-static-linux:
	./configure --reset --type=DEBUG --disable-modules --enable-static 

configure-debug-uClibc:
	DIR=/usr/local ; \
	export CC=i386-linux-uclibc-gcc ; \
	GCC_DIR=`$$CC -print-libgcc-file-name 2>&1 | sed -e 's/\/libgcc.a//'` ; \
	AR=i386-linux-uclibc-ar \
	LD=i386-linux-uclibc-ld \
	RANLIB=i386-linux-uclibc-ranlib \
	CFLAGS= \
	IFLAGS="-I$$DIR/i386-linux-uclibc/include -I$$GCC_DIR/include" \
	LDFLAGS="\
		-L$$GCC_DIR/lib \
		-L$$DIR/i386-linux-uclibc/lib \
		-Wl,--dynamic-linker -Wl,$$DIR/i386-linux/lib/ld-uClibc.so.0" \
	./configure  --reset --host=i386-pc-linux --type=DEBUG \
		--without-php5 --disable-modules

#
#	CFLAGS="-nostdinc -nostdinc++"
#	LDFLAGS"=-nostdlib --dynamic-linker $DIR/lib/ld-uClibc.so.0 \
#		--rpath-link $DIR/lib $DIR/lib/crt0.o"

configure-debug-vxworks:
	./configure --product=appWeb --host=pentium-wrs-vxworks \
		--without-matrixssl --type=DEBUG --defaults=vxdev

configure-debug-linux:
	./configure --product=appWeb --host=i386-pc-linux-gnu --defaults=dev

configure-debug-win:
	./configure --product=appWeb --host=i386-pc-cygwin --defaults=dev

# --without-cgi

configure-release-linux:
	./configure --product=appWeb --host=i386-pc-linux-gnu --defaults=release

configure-release-win:
	./configure --product=appWeb --host=i386-pc-cygwin --defaults=release

configure-release-vxworks:
	./configure --product=appWeb --host=pentium-wrs-vxworks \
		--without-matrixssl --type=DEBUG --defaults=vxworks

commercial-win:
	./configure --product=appWeb --host=i386-pc-cygwin \
		--defaults=commercialRelease

commercial-linux:
	./configure --product=appWeb --host=i386-pc-linux-gnu \
		--defaults=commercialRelease


noop:
	@true

## Local variables:
## tab-width: 4
## End:
## vim: sw=4 ts=4
