/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n,rv;



     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) error("ERROR opening socket");

     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = 7777;

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     rv = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
     if ( rv < 0) error("ERROR on binding");

     listen(sockfd,5);





     while(1)
     {
       clilen = sizeof(cli_addr);
       newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

       if (newsockfd < 0)  error("ERROR on accept");
       bzero(buffer,256);

       do
       {
         n = read(newsockfd,buffer,255);
         if (n < 0) error("ERROR reading from socket");
         buffer[n]=0;
         printf("Here is the message: %s\n",buffer);
       } while(n>0);

			 close(newsockfd);

     }


     return 0; 
}
