#include <fcntl.h>      /* Needed only for _O_RDWR definition */


#ifdef WIN32
#include <stdlib.h>
#include <stdio.h>
#include <io.h>
#include <winsock2.h>
#else
#include <unistd.h>
#include <sys/socket.h>
#endif

unsigned int buffer[1000];

int main(int argc, char *argv[])
{
  int fh,i;
  int nbytes = 1000 * 4, bytesread;
  unsigned int val;

   /* Open file for input: */
  if( (fh = open( argv[1], O_RDONLY )) == -1 )
  {
    perror( "open failed on input file: %s", argv[1]);
    exit( 1 );
  }

   /* Read in input: */
   do
   {
     bytesread = read( fh, buffer, nbytes ) ;

     for(i=0;i<(bytesread/4);i++)
     {
       val = ntohl(buffer[i]);
       if (val & 0xffff0000) 
       {
         printf("\n0x%08x", val);
       }
       else
       {
         printf(" 0x%04x", val);
       }
     }

     if( (bytesread%4) != 0)
     {
       printf("\n############### WARNING: wrong byte count !!!\n", val);
     }

   }
   while (bytesread>0);

   printf( "\n");

   close( fh );
}


