:
# Start ELinOS session
#
# Automatically created on Wed May  6 18:46:38 CEST 2009

# --- Configuration Settings ------------------------------------------
ELINOS_BOARD="mena12"
export ELINOS_BOARD
ELINOS_CPU="ppc"
export ELINOS_CPU
ELINOS_ARCH="60x"
export ELINOS_ARCH
ELINOS_LIBC="glibc-2.3.4"
export ELINOS_LIBC
ELINOS_BOARD_LINUX_ARCH=""
export ELINOS_BOARD_LINUX_ARCH
ELINOS_DOSNAME="realtime"
export ELINOS_DOSNAME
ELINOS_BOOT_STRAT="uboot_multi"
export ELINOS_BOOT_STRAT
ELINOS_KERNELPATH="men824x-ali/linux-2.4.31"
export ELINOS_KERNELPATH
MKIMAGE_LOAD_ADDRESS="00000000"
export MKIMAGE_LOAD_ADDRESS
MKIMAGE_ENTRY_ADDRESS="00000000"
export MKIMAGE_ENTRY_ADDRESS
ELINOS_HIDE_CONFIG=""
export ELINOS_HIDE_CONFIG
ELINOS_MKEFS_NPTL_NONE="y"
export ELINOS_MKEFS_NPTL_NONE
ELINOS_MKEFS_NPTL_BOTH=""
export ELINOS_MKEFS_NPTL_BOTH
ELINOS_MKEFS_NPTL_ONLY=""
export ELINOS_MKEFS_NPTL_ONLY
ELINOS_MAKE_VERBOSE=""
export ELINOS_MAKE_VERBOSE
ELINOS_MKEFS_AUTO_STRIP="y"
export ELINOS_MKEFS_AUTO_STRIP
ELINOS_MKEFS_AUTO_INCLUDE="y"
export ELINOS_MKEFS_AUTO_INCLUDE
ELINOS_MKEFS_ADD_SEARCH_PATH=""
export ELINOS_MKEFS_ADD_SEARCH_PATH
ELINOS_MKEFS_REPORT_INODES=""
export ELINOS_MKEFS_REPORT_INODES
ELINOS_MKEFS_VERBOSITY_NONE=""
export ELINOS_MKEFS_VERBOSITY_NONE
ELINOS_MKEFS_VERBOSITY_NORMAL="y"
export ELINOS_MKEFS_VERBOSITY_NORMAL
ELINOS_MKEFS_VERBOSITY_MORE=""
export ELINOS_MKEFS_VERBOSITY_MORE
ELINOS_MKEFS_VERBOSITY_MOST=""
export ELINOS_MKEFS_VERBOSITY_MOST
ELINOS_MKEFS_ADD_PARAMETERS=""
export ELINOS_MKEFS_ADD_PARAMETERS
ELINOS_VALIDATION_ALWAYS="y"
export ELINOS_VALIDATION_ALWAYS
ELINOS_VALIDATION_FILE_SYSTEM="y"
export ELINOS_VALIDATION_FILE_SYSTEM
ELINOS_VALIDATION_KERNEL="y"
export ELINOS_VALIDATION_KERNEL
ELINOS_VALIDATION_ELINOS="y"
export ELINOS_VALIDATION_ELINOS
ELINOS_VALIDATION_CROSS="y"
export ELINOS_VALIDATION_CROSS
ELINOS_VALIDATION_SKIP_FS=""
export ELINOS_VALIDATION_SKIP_FS
ELINOS_VALIDATION_IGNORE_WARNS="y"
export ELINOS_VALIDATION_IGNORE_WARNS
ELINOS_PREFIX="/opt/elinos-4.0"
export ELINOS_PREFIX
ELINOS_PROJECT="/afs/psi.ch/group/sinqhm/sinqhm_linux_rtai/LINUX_RTAI_V2"
export ELINOS_PROJECT
ELINOS_CDK="/opt/elinos-4.0/cdk/ppc/60x/glibc-2.3.4"
export ELINOS_CDK
ELINOS_BIN_PREFIX="ppc_60x"
export ELINOS_BIN_PREFIX
ELINOS_TARGET="powerpc-linux"
export ELINOS_TARGET
ELINOS_TARGET_FILES="/opt/elinos-4.0/target/ppc/60x"
export ELINOS_TARGET_FILES
ELINOS_VERSION="4.0"
export ELINOS_VERSION
LINUX_ARCH="ppc"
export LINUX_ARCH
LIST_BOOTSTRATS="uboot uboot_multi"
export LIST_BOOTSTRATS
AS="ppc_60x-as"
export AS
AR="ppc_60x-ar"
export AR
NM="ppc_60x-nm"
export NM
LD="ppc_60x-ld"
export LD
RANLIB="ppc_60x-ranlib"
export RANLIB
STRIPTOOL="ppc_60x-strip"
export STRIPTOOL
CC="ppc_60x-gcc"
export CC
CXX="ppc_60x-g++"
export CXX
GDB="ppc_60x-gdb"
export GDB
unset ELINOS_FEATURE_BASE
# --- Configuration Settings End --------------------------------------

echo "STARTING ELINOS SESSION"
echo "======================="
echo 

# addpath {env-variable} {new-path-element}
addpath()
{
    eval "_X=\"\${$1:-}\""
    if [ -n "$_X" ]; then
        if [ -z "`echo $_X | grep $2`" ]; then
            eval "$1=\"$2:$_X\""
        fi
    else
        eval "$1=\"$2\""
    fi
}

# removepath {env-variable} {old-path-element}
removepath()
{
    eval "_X=\"\${$1:-}\""
    eval "$1=\"`echo \"$_X\" | sed \"s,:$2,,\"`\""
}


# remove ${ELINOS_PREFIX}/bin from PATH if it is not the first entry.
removepath "PATH" "/opt/elinos-4.0/bin"
addpath "PATH" "${ELINOS_CDK}/bin"
addpath "PATH" "${ELINOS_PREFIX}/bin"
addpath "INFOPATH" "${ELINOS_PREFIX}/info:${ELINOS_CDK}/info"
# if possible use manpath to preset MANPATH first
manpath=`which manpath 2>/dev/null`
if [ -n "$manpath" -a -x "$manpath" ]; then
    if [ -z "${MANPATH:-}" ]; then
        MANPATH="`$manpath 2>/dev/null`"
    fi
fi
# export MANPATH in any case.
export MANPATH
addpath "MANPATH" "${ELINOS_PREFIX}/${ELINOS_ARCH}-target/${ELINOS_LIBC}/default/man"
addpath "MANPATH" "${ELINOS_CDK}/${ELINOS_BIN_PREFIX}/man"
addpath "MANPATH" "${ELINOS_CDK}/man"
addpath "MANPATH" "${ELINOS_PREFIX}/man"
unset manpath
unset addpath

echo "Setting up CDK ${ELINOS_CPU}_${ELINOS_ARCH} for $ELINOS_LIBC"
echo ""
echo "\$ELINOS_PREFIX     = $ELINOS_PREFIX"
echo "\$ELINOS_BOARD      = $ELINOS_BOARD"
echo "\$ELINOS_BIN_PREFIX = $ELINOS_BIN_PREFIX"
echo "\$ELINOS_BOOT_STRAT = $ELINOS_BOOT_STRAT"
echo "\$ELINOS_KERNELPATH = $ELINOS_KERNELPATH"
echo "\$ELINOS_PROJECT    = $ELINOS_PROJECT"
echo "\$ELINOS_DOSNAME    = $ELINOS_DOSNAME"
echo "\$CC                = $CC"
echo "\$CXX               = $CXX"
echo "\$AS                = $AS"
echo "\$GDB               = $GDB"

alias ddd='ddd --debugger $GDB'

echo
